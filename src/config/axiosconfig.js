import axios from 'axios';

axios.defaults.baseURL = 'https://classibazaar.com.au/';
axios.interceptors.request.use(config=> {

    let token = JSON.parse(localStorage.getItem('token'));
    if (token) {
        config.headers.Authorization = `Bearer ${token.access_token}`;
    }
    return config;
}, (error)=>{
    console.log(error);
    return Promise.reject(error);
});