import React from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import './CustomSelectBox.css';

function CustomSelectBox({options, mainOption, selectWrapperClass}) {
    return (
        <div className="home-explore-area-custom-select-wrapper home-explore-area-custom-select-wrapper-all">
            <div className="area-custom-selected">
                <span className="area-custom-selected-text">All States</span>
                <div className="area-custom-arrow">
                    <FontAwesomeIcon icon="chevron-down" className="area-custom-arrow-icon" />
                </div>
            </div>
            <div className="area-custom-select-options">
                <div className="area-option area-option-selected">All States</div>
                {
                options && options.map(item=>(
                    <div className="area-option"> {item} </div>
                ))
                }
            </div>
    </div>
    )
}


export default CustomSelectBox
