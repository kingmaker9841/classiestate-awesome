import React, {useState} from 'react';
import './CustomCheckBox.css';

function CustomCheckBox({labelName, clicked, checked}) {
    let [isClicked, setIsClicked] = useState(false);
    const onClickHandler = ()=>{
        setIsClicked(!isClicked);
        clicked(!isClicked);
    }
    return (
        <label className="custom-checkbox-container" >
            {labelName}
            <input type="checkbox" onClick={()=> onClickHandler()} defaultChecked={checked} />
            <span className="custom-checkbox-span"></span>
        </label>
    )
}

export default CustomCheckBox
