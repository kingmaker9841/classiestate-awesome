import React from 'react'

export function changePrice(price){
    price = price.toString();
    if (price.length === 4){
        return price.slice(0,1) + ',' + price.slice(1);
    }else if (price.length === 5){
        return price.slice(0,2) + ',' + price.slice(2);
    }else if (price.length === 6){
       return price.slice(0,3) + ',' + price.slice(3)
    }else if (price.length === 7){
        return price.slice(0,1) + "," + price.slice(1,3) + "," + price.slice(4);
    }else if (price.length === 8) {
        return price.slice(0,2) + ',' + price.slice(2, 3) + ',' + price.slice(5);
    }else {
        return price;
    }

}
function CustomPrice() {
    return (
        <div>
            
        </div>
    )
}

export default CustomPrice
