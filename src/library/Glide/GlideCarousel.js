import React, {useEffect} from 'react';
import Glide from '@glidejs/glide';
import '@glidejs/glide/dist/css/glide.core.min.css';
import bannerBg1 from "../../assets/images/banner/1.jpg";
import bannerBg2 from "../../assets/images/banner/2.jpg";
import bannerBg3 from "../../assets/images/banner/3.jpg";
import './GlideCarousel.css';

function GlideCarousel() {
    useEffect(()=>{
        const glide = new Glide('#glide', {
            gap: 0, autoplay: 5000, animationDuration:1000
        });
        glide.mount();
    }, [])
    return (
        <div className="banner-wrapper">
            <div className="glide__slides glide__controls glide" id="glide">
                <div className="glide__track" data-glide-el="track">
                    <ul className="glide__slides">
                        <li className="glide__slide">
                            <img src={bannerBg1} alt="Banner 1" />
                        </li>
                        <li className="glide__slide">
                            <img src={bannerBg2} alt="Banner 2" />
                        </li>
                        <li className="glide__slide">
                            <img src={bannerBg3} alt="Banner 3" />
                        </li>
                    </ul>
                </div>
                <div className="glide__bullets" data-glide-el="controls[nav]">
                    <button className="glide__bullet bullet-button" data-glide-dir="=0"></button>
                    <button className="glide__bullet bullet-button" data-glide-dir="=1"></button>
                    <button className="glide__bullet bullet-button" data-glide-dir="=2"></button>
                </div>
            </div>
        </div>
    )
}

export default GlideCarousel
