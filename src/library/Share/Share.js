import React, {useEffect} from 'react'
import './Share.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

function Share({text}) {

    return (
        <>
        {/* <button type="button" className="btn btn-primary mx-auto" data-toggle="modal" data-target="#shareModal"> {text} </button>  */}

            <div className="modal fade" id="shareModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content col-12">
                        <div className="modal-header">
                            <h5 className="modal-title">Share</h5> <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        </div>
                        <div className="modal-body">
                            <div className="icon-container1 d-flex">
                                <div className="smd"> <FontAwesomeIcon icon="twitter" className=" img-thumbnail faba" style={{color:"#4c6ef5",backgroundColor: "aliceblue"}} />
                                    <p>Twitter</p>
                                </div>
                                <div className="smd"> <FontAwesomeIcon icon="facebook-square" className="img-thumbnail faba " style={{color: "#3b5998",backgroundColor: "#eceff5"}}/>
                                    <p>Facebook</p>
                                </div>
                                <div className="smd"> <FontAwesomeIcon icon="reddit-alien" className="img-thumbnail faba" style={{color: "#FF5700" , backgroundColor: "#fdd9ce"}} />
                                    <p>Reddit</p>
                                </div>
                            </div>
                            <div className="icon-container2 d-flex">
                                <div className="smd"> <FontAwesomeIcon icon="whatsapp" className="img-thumbnail faba " style={{color: "#25D366",backgroundColor: "#cef5dc"}} />
                                    <p>Whatsapp</p>
                                </div>
                                <div className="smd"> <FontAwesomeIcon icon="facebook-messenger" className="img-thumbnail faba " style={{color: "#3b5998", backgroundColor: "#eceff5"}} />
                                    <p>Messenger</p>
                                </div>
    
                            </div>
                        </div>
                        <div className="modal-footer"> <label style={{fontWeight: "600"}}>Page Link <span className="message"></span></label><br />
                            <div className="row"> <input className="col-10 ur" type="url" placeholder="https://www.arcardio.app/acodyseyy" id="myInput" aria-describedby="inputGroup-sizing-default" style={{height: "40px"}} /> <button className="cpy">Clone</button> </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Share
