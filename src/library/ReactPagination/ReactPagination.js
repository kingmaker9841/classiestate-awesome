import React, {useState} from 'react';
import ReactPaginate from 'react-paginate';
import './ReactPagination.css';

function ReactPagination({data}) {
    let [currentPage, setCurrentPage] = useState(0);
    const perPage = 2;
    const offset = currentPage * perPage;
    const currentPageData = data.slice(offset, offset + perPage);
    const pageCount = Math.ceil(data.length / perPage);
    const handlePageClick = ({selected: selectedPage})=> {
        setCurrentPage(selectedPage);
    }
    return (
        <div>
            <ReactPaginate 
            previousLabel = {'<<'}
            nextLabel={'>>'}
            pageCount={pageCount}
            onPageChange={handlePageClick}
            containerClassName={'react-pagination-container'}
            previousLinkClassName={'react-link-class'}
            nextLinkClassName={'react-link-class'}
            disabledClassName={'react-link-class--active'}
            activeClassName={'react-link-class--disabled'}
            />
        </div>
    )
}

export default ReactPagination
