import React from 'react';
import './ProfileNav.css';
import {NavLink} from 'react-router-dom'

function ProfileNav() {
    return (
        <div className="profile-navigation-container">
            <div className="profile-nav-links">
                <div className="profile-navigation-listing">
                    <NavLink className="profile-nav" activeClassName="active-profile-nav" to={{
                        pathname: '/profile/listing'
                    }}>
                        Listing
                    </NavLink>
                </div>
                <div className="profile-navigation-favorite">
                    <NavLink className="profile-nav" activeClassName="active-profile-nav" to={{
                        pathname: '/profile/favorite'
                    }}>
                        Favorite
                    </NavLink>
                </div>
                <div className="profile-navigation-listing">
                    <NavLink className="profile-nav" activeClassName="active-profile-nav" to={{
                        pathname: '/profile/contact'
                    }}>
                        Contact
                    </NavLink>
                </div>
            </div>
        </div>
    )
}

export default ProfileNav
