import React, {useEffect, useState} from 'react'
import './ProfileContact.css'
import Profile from '../Profile';
import axios from 'axios';
import ClipLoader from 'react-spinners/ClipLoader';
import Footer from '../../Footer/Footer';

function ProfileContact() {
    let [userEmail, setUserEmail] = useState('');
    let [userAddress, setUserAddress] = useState('');
    let [userNumber, setUserNumber] = useState('');
    let [numberVerified, setNumberVerified] = useState('');
    let [message, setMessage] = useState('');
    let [email, setEmail] = useState('');
    let [number,setNumber] = useState('');
    let [fname, setFname] = useState('');
    let [lname, setLname] = useState('');
    let [contactSuccess, setContactSuccess] = useState('');
    let [contactError, setContactError] = useState('');
    let [loading, setLoading] = useState(false);

    useEffect(()=>{
        axios.get('/api/v2/profile')
        .then(res=>{
            console.log(res.data);
            if (res.data){
                setFname(res.data.first_name);
                setLname(res.data.last_name);
                setUserEmail(res.data.user_email);
                setUserAddress(res.data.location);
                setUserNumber(res.data.contact_number);
                setNumberVerified(res.data.mobile_verified ? '': 'Not Verified');
            }
        })
        .catch(err=>{
            console.log(err);
        })
    }, []);
    const onMessageChange = e=> setMessage(e.target.value)
    const onEmailChange = e=> setEmail(e.target.value)
    const onNumberChange = e=> setNumber(e.target.value)

    const contactSubmitHandler = e=>{
        setLoading(true);
        e.preventDefault();
        console.log(message, email, number);
        axios.post('/api/deal/contact', {
            first_name: fname, last_name: lname, email: email,
            msg: message, contact_from: 'ClassiEstate'
       })
       .then(res=>{
           console.log(res);
           console.log(res.data);
           setContactError('');
           setContactSuccess(res.data? (res.data.message ? res.data.message: '') : '')
           setLoading(false);
           window.scrollTo(0,500);
        })
       .catch(err=>{
           console.log(err);
           console.log(err.response ? err.response : '');
           setContactSuccess('');
           setContactError(err.response.data.errors.email ? err.response.data.errors.email: 'Validation Failed');
           setLoading(false);
           window.scrollTo(0,500);
       })
       
    }
    return (
        <>
            <Profile />
            {
                contactSuccess 
                ?(<div className="alert alert-success mt-5 ml-4" role="alert"> {contactSuccess} </div> )
                :null 
            }
            {
                contactError ? (<div className="alert alert-danger mt-5 ml-4" role="alert">{contactError}</div> ) 
                : null
            }
            <div className="profile-form-email-group">
                <div className="profile-form-group">
                    <h3><strong>Send Message</strong></h3>

                    <form className="profile-form" onSubmit={e=> contactSubmitHandler(e)}>
                        <div className="form-group">
                            <label htmlFor="profile-message" ><strong>Your message</strong></label>
                            <textarea id="profile-message" type="textarea" className="form-control" rows="8" onChange={(e)=> onMessageChange(e)} />
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label htmlFor="profile-form-email" ><strong>Your email</strong></label>
                                <input id="profile-form-email" type="email" className="form-control" onChange={(e)=> onEmailChange(e)} />
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="profile-form-contact"><strong>Your contact number</strong></label>
                                <input id="profile-form-contact" type="number" className="form-control" onChange={(e)=> onNumberChange(e)} />
                            </div>
                        </div>
                        <div className="form-check">
                            <input id="profile-save-email" type="checkbox" className="form-check-input" />
                            <label htmlFor="profile-save-email" className="form-check-label"><strong>
                                Save my email in the browser for the next time I contact.    
                            </strong></label>
                        </div>
                        {
                            loading ? 
                            (<button type="sumbit" className="btn btn-success profile-form-submit">
                                <ClipLoader loading={loading} color={"#f4f4f4"} />
                            </button>)
                            :
                            (<button type="sumbit" className="btn btn-success profile-form-submit">Submit</button>)
                        }
                        
                    </form>
                </div>
                <div className="profile-email-phone-group">
                    <div className="profile-phone-group">
                        <p><strong>Phone No</strong></p>
                        <p> {userNumber ? `${userNumber} + ${numberVerified}` : (<em>Not Specified</em>)} </p>
                    </div>
                    <div className="profile-email-group">
                        <p><strong>Email</strong></p>
                        <p> {userEmail ? userEmail: (<em>Not Specified</em>)} </p>
                    </div>
                    <div className="profile-address-group">
                        <p><strong>Address</strong></p>
                        <p> {userAddress? userAddress : (<em>Not Specified</em>)} </p>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default ProfileContact
