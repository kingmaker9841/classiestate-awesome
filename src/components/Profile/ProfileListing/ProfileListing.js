import React from 'react'
import './ProfileListing.css'
import Profile from '../Profile';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import Manual1 from '../../../assets/images/manual1.jpg'
import Manual2 from '../../../assets/images/manual2.jpg'
import Manual3 from '../../../assets/images/manual3.jpg'
import Manual4 from '../../../assets/images/manual4.jpg'
import Footer from '../../Footer/Footer';
// import '../../HomePageAds/HomePageAds.css'

function ProfileListing() {
    return (
        <>
        <Profile />
        <div className="homepagead-grid-wrapper mt-5 profile-listing-grid">
        {/* Manual Grids Start ************************************** */}
        <div className="homead-main-grid" >
                <div className="homead-main-grid-imgcontent">

                    {/* Specifying fallback Image Icon*/ }
                    <img src={Manual1} alt="manual1.jpg" className="homead-main-grid-image" />
                
                </div>

                {/* Ad address, title, posted_at, price */}
                <div className="homead-other-content pl-3 pt-2">
                    <p className="text-muted"> 
                       Test Address 1
                    </p>
                    <p><strong> 
                        Test Title 1
                    </strong></p>
                    <p> 22 hours ago </p>
                    
                    
                    <Link className="link linkhome" to={{
                        pathname: "/posts/2030",
                        state: {
                            fromHome: true
                        }
                    }} >
                    <p className='homead-price-details'>
                        $&nbsp;15000
                        
                    </p>
                    <p className="homead-view-details">
                        <strong>
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;View Details
                        </strong>
                    </p>
                    
                    </Link>
                </div>


            </div>
            <div className="homead-main-grid" >
                <div className="homead-main-grid-imgcontent">

                    {/* Specifying fallback Image Icon*/ }
                    <img src={Manual2} alt="manual2.jpg" className="homead-main-grid-image" />
                
                </div>

                {/* Ad address, title, posted_at, price */}
                <div className="homead-other-content pl-3 pt-2">
                    <p className="text-muted"> 
                       Test Address 2
                    </p>
                    <p><strong> 
                        Test Title 2
                    </strong></p>
                    <p> 2 hours ago </p>
                    
                    
                    <Link className="link linkhome" to={{
                        pathname: "/posts/2030",
                        state: {
                            fromHome: true
                        }
                    }} >
                    <p className='homead-price-details'>
                        $&nbsp;50000
                        
                    </p>
                    <p className="homead-view-details">
                        <strong>
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;View Details
                        </strong>
                    </p>
                    
                    </Link>
                </div>


            </div>
            <div className="homead-main-grid" >
                <div className="homead-main-grid-imgcontent">

                    {/* Specifying fallback Image Icon*/ }
                    <img src={Manual3} alt="manual1.jpg" className="homead-main-grid-image" />
                
                </div>

                {/* Ad address, title, posted_at, price */}
                <div className="homead-other-content pl-3 pt-2">
                    <p className="text-muted"> 
                       Test Address 3
                    </p>
                    <p><strong> 
                        Test Title 3
                    </strong></p>
                    <p> 15 hours ago </p>
                    
                    
                    <Link className="link linkhome" to={{
                        pathname: "/posts/2030",
                        state: {
                            fromHome: true
                        }
                    }} >
                    <p className='homead-price-details'>
                        $&nbsp;100000
                        
                    </p>
                    <p className="homead-view-details">
                        <strong>
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;View Details
                        </strong>
                    </p>
                    
                    </Link>
                </div>


            </div>
            <div className="homead-main-grid" >
                <div className="homead-main-grid-imgcontent">

                    {/* Specifying fallback Image Icon*/ }
                    <img src={Manual4} alt="manual1.jpg" className="homead-main-grid-image" />
                
                </div>

                {/* Ad address, title, posted_at, price */}
                <div className="homead-other-content pl-3 pt-2">
                    <p className="text-muted"> 
                       Test Address 4
                    </p>
                    <p><strong> 
                        Test Title 4
                    </strong></p>
                    <p> 5 hours ago </p>
                    
                    
                    <Link className="link linkhome" to={{
                        pathname: "/posts/2030",
                        state: {
                            fromHome: true
                        }
                    }} >
                    <p className='homead-price-details'>
                        $&nbsp;120000
                        
                    </p>
                    <p className="homead-view-details">
                        <strong>
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;View Details
                        </strong>
                    </p>
                    
                    </Link>
                </div>


            </div>
            <div className="homead-main-grid" >
                <div className="homead-main-grid-imgcontent">

                    {/* Specifying fallback Image Icon*/ }
                    <img src={Manual1} alt="manual1.jpg" className="homead-main-grid-image" />
                
                </div>

                {/* Ad address, title, posted_at, price */}
                <div className="homead-other-content pl-3 pt-2">
                    <p className="text-muted"> 
                       Test Address 5
                    </p>
                    <p><strong> 
                        Test Title 5
                    </strong></p>
                    <p> 1 hour ago </p>
                    
                    
                    <Link className="link linkhome" to={{
                        pathname: "/posts/2030",
                        state: {
                            fromHome: true
                        }
                    }} >
                    <p className='homead-price-details'>
                        $&nbsp;122500
                        
                    </p>
                    <p className="homead-view-details">
                        <strong>
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;View Details
                        </strong>
                    </p>
                    
                    </Link>
                </div>


            </div>
            <div className="homead-main-grid" >
                <div className="homead-main-grid-imgcontent">

                    {/* Specifying fallback Image Icon*/ }
                    <img src={Manual1} alt="manual1.jpg" className="homead-main-grid-image" />
                
                </div>

                {/* Ad address, title, posted_at, price */}
                <div className="homead-other-content pl-3 pt-2">
                    <p className="text-muted"> 
                       Test Address 6
                    </p>
                    <p><strong> 
                        Test Title 6
                    </strong></p>
                    <p> 1 hour ago </p>
                    
                    
                    <Link className="link linkhome" to={{
                        pathname: "/posts/2030",
                        state: {
                            fromHome: true
                        }
                    }} >
                    <p className='homead-price-details'>
                        $&nbsp;2500
                        
                    </p>
                    <p className="homead-view-details">
                        <strong>
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;View Details
                        </strong>
                    </p>
                    
                    </Link>
                </div>


            </div>

            {/* Manual Grids End ************************************** */}
            </div>
            <Footer />
        </>
    )
}

export default ProfileListing
