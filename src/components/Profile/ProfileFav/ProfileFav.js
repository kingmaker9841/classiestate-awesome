import React, {useEffect, useState} from 'react'
import './ProfileFav.css';
import Profile from '../Profile';
import axios from 'axios';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Link} from 'react-router-dom';
import Footer from '../../Footer/Footer'

function ProfileFav() {
    let [favPosts, setFavPosts] = useState([{}]);
    let [isLoggedIn, setIsLoggedIn] = useState(false);
    let [savedFav, setSavedFav] = useState('');

    useEffect(()=>{
        let favs = JSON.parse(localStorage.getItem('classi_favorite'));
        let num = '';
        let j=0;
        var arr = [];
        if (favs){
            while (j<favs.length){
                if (!isNaN(favs[j])){
                    while(!isNaN(favs[j])){
                        num += favs[j];
                        j++;
                    }
                    arr.push(num);
                    console.log(arr);
                    num = '';
                    continue;
                }
                j++;
            }
        }
        let isActive = true;
        const getData = (arr)=>{
            arr.map(async(item)=>{
                const res = await axios.get(`/api/v2/${item}`)
                if (res){
                console.log(res.data);
            }
            })
            
        }
        getData(arr);
        // let brr = [{}];
        // arr.map(item=>{
        //     axios.get(`api/v2/ads_detail/${item}`)
        //     .then(res=>{
        //         console.log(res.data);
        //         brr.push(res.data);
        //     })
        //     .catch(err=>{
        //         console.log(err);
        //     })
        // })
        // console.log(brr);
        // brr && setFavPosts(brr);
        if (localStorage.getItem('token')){
            setIsLoggedIn(true);
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            setSavedFav(cF);
        }
        return ()=>{
            isActive = false;
        }
    }, []);


    const favoriteClickHandler = (e, id)=>{
        if (isLoggedIn){
            if (savedFav){
                if (savedFav.includes(id)){
                    e.target.classList.toggle('homead-icon-saveshare');
                    //Now Update savedFav
                    setSavedFav(prev=> prev.replace(id, ''));
                }else {
                    e.target.classList.toggle('homead-icon-saveshare-active');
                }
            }else {
                e.target.classList.toggle('homead-icon-saveshare-active');
            }
            
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            if (cF){
                if (String(cF).includes(id)){
                    cF = String(cF).replace(id, '')
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }else {
                    cF = String(cF) + ',' + id;
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }  
            }else {
                localStorage.setItem('classi_favorite', JSON.stringify(id));
            }
            
        }
    }

    return (
        <>
            <Profile />
            <div className="homepagead-grid-wrapper">
            {
                favPosts.length > 0 ?( 
                favPosts.map((item,index)=>(
                    <div key={index} className="homead-main-grid" >
                            <div className="homead-main-grid-imgcontent">

                                <div className="homead-img-hover">
                                        <FontAwesomeIcon icon="heart" data-toggle={isLoggedIn ? null : 'modal'} data-target="#favorite-modal" onClick={(e)=> favoriteClickHandler(e, item.post_id)} 
                                            className={
                                                savedFav 
                                                ? (savedFav.includes(item.post_id) 
                                                ? "homead-icon-saveshare-active"
                                                : "homead-icon-saveshare") 
                                                : "homead-icon-saveshare"
                                            } 
                                        /> 
                                </div>

                                {/* Specifying fallback Image Icon*/ }
                                <img src={item.default_image} alt={item.post_id} className="homead-main-grid-image" />
                            
                            </div>

                           {/* Ad address, title, posted_at, price */}
                           <div className="homead-other-content pl-3 pt-2">
                                <p className="text-muted"> 
                                    {
                                    item.ad_address && item.ad_address.length > 25 ? 
                                    item.ad_address && item.ad_address.slice(0,25) + "...":
                                    item.ad_address
                                    } 
                                </p>
                                <p><strong> 
                                    {
                                    item.title && item.title.length> 30 ? 
                                    item.title && item.title.slice(0,28) + "..." :
                                    item.title
                                    } 
                                </strong></p>
                                <p> {item.posted_at} </p>
                                
                                
                                <Link className="link linkhome" to={{
                                    pathname: "/posts/" + item.post_id,
                                    state: {
                                        fromHome: true
                                    }
                                }} >
                                <p className='homead-price-details'>
                                    $&nbsp;{item.price }
                                    
                                </p>
                                <p className="homead-view-details">
                                    <strong>
                                        <FontAwesomeIcon icon="eye" />
                                        &nbsp;View Details
                                    </strong>
                                </p>
                                
                                </Link>
                           </div>


                        </div>


                     
                ))
                                   
                ): null
            }
            </div>
            <Footer />
        </>
    )
}

export default ProfileFav
