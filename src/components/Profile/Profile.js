import React ,{useState, useEffect} from 'react'
import './Profile.css';
import Navigation from '../Navigation/Navigation';
import useDocTitle from '../../customHooks/useDocTitle';
import axios from 'axios';
import SyncLoader from 'react-spinners/SyncLoader';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import ProfileNav from './ProfileNav';

function Profile() {

    //For loading
    let [loading, setLoading] = useState(false);
    //For Doc Title
    let [name, setName] = useState('');
    useDocTitle(name ? `${name} -Profile` : 'Profile');

    //For image and thumbnail 
    let [image, setImage] = useState('');
    let [thumbnail, setThumbnail] = useState('');

    //Bio
    let [personalInfo, setPersonalInfo] = useState('');


    useEffect(()=>{
        setLoading(true);
        let isActive= true;
        axios.get('/api/v2/profile')
        .then(res=>{
            if (isActive){
                console.log(res);
                console.log(res.data);
                setName(`${res.data.first_name} ${res.data.last_name}`);

                if (res.data.image) {
                    setImage(res.data.image)
                }else if (res.data.image_url){
                    setImage(res.data.image_url)
                }
                if (res.data.thumbnail) {
                    setThumbnail(res.data.thumbnail)
                }else if (res.data.thumbnail_url){
                    setThumbnail(res.data.thumbnail_url)
                }
                setPersonalInfo(res.data.personal_info);
            }            
        })
        .catch(err=>{
            if (isActive) {
                console.log(err);
                console.log(err.response);
            }
        })
        setLoading(false);
        return ()=>{
            isActive = false;
        }
    }, [])

    return (
        <div className="profile-container">
            <Navigation />
            <div className="mb-5 d-flex justify-content-center">
                <SyncLoader size={10} color={'var(--primary-color)'} loading={loading} />
            </div>
            <div className="profile-image-bio-container">
                <div className="profile-image-div">
                    <img src={image} className="profile-image" alt="Profile.jpg" />
                </div>
                <div className="profile-thumbnail-div">
                    <img src={thumbnail} className="profile-thumbnail" alt="Thumbnail.jpg" />
                </div>
            </div>
            <div className="profile-name-social">
                <div className="profile-name">
                    <h3><strong>{name}</strong></h3>
                </div>
                <div className="profile-social">
                    <div className="profile-social-fb">
                        <FontAwesomeIcon icon={["fab", "facebook"]} className="profile-facebook-icon" />
                    </div>
                    <div className="profile-social-twitter">
                        <FontAwesomeIcon icon={["fab", "twitter"]} className="profile-twitter-icon" />
                    </div>
                    <div className="profile-social-insta">
                        <FontAwesomeIcon icon={["fab", "instagram"]} className="profile-instagram-icon" />
                    </div>
                </div>
            </div>
            <div className="profile-bio">
                {personalInfo ? personalInfo
                : (
                    <p className="text-justify">Personal Info Space</p>
                )}
            </div>
            <div className="profile-divider"></div>
            <ProfileNav />
        </div>
    )
}

export default Profile
