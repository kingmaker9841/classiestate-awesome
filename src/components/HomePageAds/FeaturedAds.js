import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import SyncLoader from 'react-spinners/SyncLoader';
import './FeaturedAds.css';

function FeaturedAds() {

    let [posts, setPosts] = useState([]);
    let [loading, setLoading] = useState(false);

    
    let [isLoggedIn, setIsLoggedIn] = useState(false);

    //Saved Favorite Ads in Local Storage
    let [savedFav, setSavedFav] = useState('');

    useEffect(()=>{

        let isActive = true;

        setLoading(true)
        axios.get('/api/v2/featured_ads?api_from=classiEstate')
        .then(response=>{
            if (isActive){
                console.log(response);
                console.log(response.data);
                setPosts(response.data.posts);
                setLoading(false);
            }

        })
        .catch(err=>{
            if (isActive){
                console.log(err);
                console.log(err.response);
                setLoading(false);
            }

        })
        if (localStorage.getItem('token')){
            setIsLoggedIn(true);
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            setSavedFav(cF);
        }
        return ()=>{
            isActive = false;
        }
    }, [])

    const favoriteClickHandler = (e, id)=>{
        if (isLoggedIn){
            if (savedFav){
                if (savedFav.includes(id)){
                    e.target.classList.toggle('homead-icon-saveshare');
                    //Now Update savedFav
                    setSavedFav(prev=> prev.replace(id, ''));
                }else {
                    e.target.classList.toggle('homead-icon-saveshare-active');
                }
            }else {
                e.target.classList.toggle('homead-icon-saveshare-active');
            }
            
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            if (cF){
                if (String(cF).includes(id)){
                    cF = String(cF).replace(id, '')
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }else {
                    cF = String(cF) + ',' + id;
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }  
            }else {
                localStorage.setItem('classi_favorite', JSON.stringify(id));
            }
            
        }
    }

    return (
        <>
        {posts && (

        <>
        <div className="home-featured">
            <p className="home-featured-p">Featured Ads</p>
            <p className="btn btn-light btn-outline-dark">Show All</p>
        </div>
        <div className="homead-grid-container">
            <div className="homead-syncloader d-flex justify-content-center">
                <SyncLoader size={10} color={'var(--primary-color)'} loading={loading} />
            </div>

            <div className="homepagead-grid-wrapper"> 
                {
                    posts && 
                    posts.map((item,index)=>(

                        <div key={index} className="homead-main-grid" >
                            <div className="homead-main-grid-imgcontent">

                                <div className="homead-img-hover">
                                        <FontAwesomeIcon icon="heart" data-toggle={isLoggedIn ? null : 'modal'} data-target="#favorite-modal" onClick={(e)=> favoriteClickHandler(e, item.post_id)} className="homead-icon-saveshare" 
                                         className={
                                            savedFav 
                                            ? (savedFav.includes(item.post_id) 
                                            ? "homead-icon-saveshare-active"
                                            : "homead-icon-saveshare") 
                                            : "homead-icon-saveshare"
                                        } 
                                        /> 
                                </div>

                                {/* Specifying fallback Image Icon*/ }
                                <img src={item.default_image} alt={item.post_id} className="homead-main-grid-image" />
                            
                            </div>

                           {/* Ad address, title, posted_at, price */}
                           <div className="homead-other-content pl-3 pt-2">
                                <p className="text-muted"> 
                                    {
                                    item.ad_address.length > 25 ? 
                                    item.ad_address.slice(0,25) + "...":
                                    item.ad_address
                                    } 
                                </p>
                                <p><strong> 
                                    {
                                    item.title.length> 30 ? 
                                    item.title.slice(0,28) + "..." :
                                    item.title
                                    } 
                                </strong></p>
                                <p> {item.posted_at} </p>
                                
                                
                                <Link className="link linkhome" to={{
                                    pathname: "/posts/" + item.post_id,
                                    state: {
                                        fromHome: true
                                    }
                                }} >
                                <p className='homead-price-details'>
                                    {item.price }
                                    
                                </p>
                                <p className="homead-view-details">
                                    <strong>
                                        <FontAwesomeIcon icon="eye" />
                                        &nbsp;View Details
                                    </strong>
                                </p>
                                
                                </Link>
                           </div>


                        </div>


                        
                    ))
                }
            </div>
        </div>
        <div className="modal fade" data-backdrop="" id="favorite-modal" tabIndex="-1" role="dialog">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title text-success">Not Logged In?</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p><strong>Please Login to continue</strong></p>
                        </div>
                        <div className="modal-footer favorite-modal-footer">
                            <p>Want to login?<Link to="/login"><span className="text-danger">&nbsp;Login</span></Link> </p>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                           
                        </div>
                    </div>
                </div>
            </div>
        </>
        )}
        </>
    )
}

export default FeaturedAds;
