import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import './HomePageAds.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import SyncLoader from 'react-spinners/SyncLoader';
import Manual1 from '../../assets/images/manual1.jpg'
import Manual2 from '../../assets/images/manual2.jpg'
import Manual3 from '../../assets/images/manual3.jpg'
import Manual4 from '../../assets/images/manual4.jpg'

function HomePageAds() {

    let [posts, setPosts] = useState([]);
    let [loading, setLoading] = useState(false);

    let [isLoggedIn, setIsLoggedIn] = useState(false);

    //Saved Favorite Ads in Local Storage
    let [savedFav, setSavedFav] = useState('');

    useEffect(()=>{

        let isActive = true;

        setLoading(true)
        axios.get('/api/v2/ads_list?slug=real-estate&api_from=classiEstate')
        .then(response=>{
            if (isActive){
                console.log(response);
                console.log(response.data);
                setPosts(response.data.posts);
                setLoading(false);
            }

        })
        .catch(err=>{
            if (isActive){
                console.log(err);
                console.log(err.response);
                setLoading(false);
            }

        })

        if (localStorage.getItem('token')){
            setIsLoggedIn(true);
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            setSavedFav(cF);
        }
        return ()=>{
            isActive = false;
        }
    }, [])

    const favoriteClickHandler = (e, id)=>{
        if (isLoggedIn){
            if (savedFav){
                if (savedFav.includes(id)){
                    e.target.classList.toggle('homead-icon-saveshare');
                    //Now Update savedFav
                    setSavedFav(prev=> prev.replace(id, ''));
                }else {
                    e.target.classList.toggle('homead-icon-saveshare-active');
                }
            }else {
                e.target.classList.toggle('homead-icon-saveshare-active');
            }
            
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            if (cF){
                if (String(cF).includes(id)){
                    cF = String(cF).replace(id, '')
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }else {
                    cF = String(cF) + ',' + id;
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }  
            }else {
                localStorage.setItem('classi_favorite', JSON.stringify(id));
            }
            
        }
    }

    return (
        <div className="homead-grid-container">
            <div className="homead-syncloader mb-5 d-flex justify-content-center">
                <SyncLoader size={10} color={'var(--primary-color)'} loading={loading} />
            </div>

            <div className="homepagead-grid-wrapper"> 

            {/* Manual Grids Start ************************************** */}
            <div className="homead-main-grid" >
                <div className="homead-main-grid-imgcontent">

                    <div className="homead-img-hover">
                            <FontAwesomeIcon icon="heart" data-toggle={isLoggedIn ? null : 'modal'} data-target="#favorite-modal" onClick={(e)=> favoriteClickHandler(e, 1020)} 
                            className={
                                        savedFav 
                                        ? (savedFav.includes("1020") 
                                        ? "homead-icon-saveshare-active"
                                        : "homead-icon-saveshare") 
                                        : "homead-icon-saveshare"
                                    } 
                    /> 
                    </div>

                    {/* Specifying fallback Image Icon*/ }
                    <img src={Manual1} alt="manual1.jpg" className="homead-main-grid-image" />
                
                </div>

                {/* Ad address, title, posted_at, price */}
                <div className="homead-other-content pl-3 pt-2">
                    <p className="text-muted"> 
                       Test Address 1
                    </p>
                    <p><strong> 
                        Test Title 1
                    </strong></p>
                    <p> 22 hours ago </p>
                    
                    
                    <Link className="link linkhome" to={{
                        pathname: "/posts/2030",
                        state: {
                            fromHome: true
                        }
                    }} >
                    <p className='homead-price-details'>
                        $&nbsp;15000
                        
                    </p>
                    <p className="homead-view-details">
                        <strong>
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;View Details
                        </strong>
                    </p>
                    
                    </Link>
                </div>


            </div>
            <div className="homead-main-grid" >
                <div className="homead-main-grid-imgcontent">

                    <div className="homead-img-hover">
                            <FontAwesomeIcon icon="heart" data-toggle={isLoggedIn ? null : 'modal'} data-target="#favorite-modal" onClick={(e)=> favoriteClickHandler(e, 1022)} 
                                className={
                                savedFav 
                                ? (savedFav.includes("1022") 
                                ? "homead-icon-saveshare-active"
                                : "homead-icon-saveshare") 
                                : "homead-icon-saveshare"
                            } 
                             /> 
                    </div>

                    {/* Specifying fallback Image Icon*/ }
                    <img src={Manual2} alt="manual2.jpg" className="homead-main-grid-image" />
                
                </div>

                {/* Ad address, title, posted_at, price */}
                <div className="homead-other-content pl-3 pt-2">
                    <p className="text-muted"> 
                       Test Address 2
                    </p>
                    <p><strong> 
                        Test Title 2
                    </strong></p>
                    <p> 2 hours ago </p>
                    
                    
                    <Link className="link linkhome" to={{
                        pathname: "/posts/2030",
                        state: {
                            fromHome: true
                        }
                    }} >
                    <p className='homead-price-details'>
                        $&nbsp;50000
                        
                    </p>
                    <p className="homead-view-details">
                        <strong>
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;View Details
                        </strong>
                    </p>
                    
                    </Link>
                </div>


            </div>
            <div className="homead-main-grid" >
                <div className="homead-main-grid-imgcontent">

                    <div className="homead-img-hover">
                            <FontAwesomeIcon icon="heart" data-toggle={isLoggedIn ? null : 'modal'} data-target="#favorite-modal" onClick={(e)=> favoriteClickHandler(e, 1023)} 
                            className={
                                savedFav 
                                ? (savedFav.includes("1023") 
                                ? "homead-icon-saveshare-active"
                                : "homead-icon-saveshare") 
                                : "homead-icon-saveshare"
                            } 
                            /> 
                    </div>

                    {/* Specifying fallback Image Icon*/ }
                    <img src={Manual3} alt="manual1.jpg" className="homead-main-grid-image" />
                
                </div>

                {/* Ad address, title, posted_at, price */}
                <div className="homead-other-content pl-3 pt-2">
                    <p className="text-muted"> 
                       Test Address 3
                    </p>
                    <p><strong> 
                        Test Title 3
                    </strong></p>
                    <p> 15 hours ago </p>
                    
                    
                    <Link className="link linkhome" to={{
                        pathname: "/posts/2030",
                        state: {
                            fromHome: true
                        }
                    }} >
                    <p className='homead-price-details'>
                        $&nbsp;100000
                        
                    </p>
                    <p className="homead-view-details">
                        <strong>
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;View Details
                        </strong>
                    </p>
                    
                    </Link>
                </div>


            </div>
            <div className="homead-main-grid" >
                <div className="homead-main-grid-imgcontent">

                    <div className="homead-img-hover">
                            <FontAwesomeIcon icon="heart" data-toggle={isLoggedIn ? null : 'modal'} data-target="#favorite-modal" onClick={(e)=> favoriteClickHandler(e, 1024)} 
                            className={
                                savedFav 
                                ? (savedFav.includes("1024") 
                                ? "homead-icon-saveshare-active"
                                : "homead-icon-saveshare") 
                                : "homead-icon-saveshare"
                            } 
                            /> 
                    </div>

                    {/* Specifying fallback Image Icon*/ }
                    <img src={Manual4} alt="manual1.jpg" className="homead-main-grid-image" />
                
                </div>

                {/* Ad address, title, posted_at, price */}
                <div className="homead-other-content pl-3 pt-2">
                    <p className="text-muted"> 
                       Test Address 4
                    </p>
                    <p><strong> 
                        Test Title 4
                    </strong></p>
                    <p> 5 hours ago </p>
                    
                    
                    <Link className="link linkhome" to={{
                        pathname: "/posts/2030",
                        state: {
                            fromHome: true
                        }
                    }} >
                    <p className='homead-price-details'>
                        $&nbsp;120000
                        
                    </p>
                    <p className="homead-view-details">
                        <strong>
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;View Details
                        </strong>
                    </p>
                    
                    </Link>
                </div>


            </div>

            {/* Manual Grids End ************************************** */}
                {
                    posts && 
                    posts.map((item,index)=>(

                        <div key={index} className="homead-main-grid" >
                            <div className="homead-main-grid-imgcontent">

                                <div className="homead-img-hover">
                                        <FontAwesomeIcon icon="heart" data-toggle={isLoggedIn ? null : 'modal'} data-target="#favorite-modal" onClick={(e)=> favoriteClickHandler(e, item.post_id)} 
                                            className={
                                                savedFav 
                                                ? (savedFav.includes(item.post_id) 
                                                ? "homead-icon-saveshare-active"
                                                : "homead-icon-saveshare") 
                                                : "homead-icon-saveshare"
                                            } 
                                        /> 
                                </div>

                                {/* Specifying fallback Image Icon*/ }
                                <img src={item.default_image} alt={item.post_id} className="homead-main-grid-image" />
                            
                            </div>

                           {/* Ad address, title, posted_at, price */}
                           <div className="homead-other-content pl-3 pt-2">
                                <p className="text-muted"> 
                                    {
                                    item.ad_address.length > 25 ? 
                                    item.ad_address.slice(0,25) + "...":
                                    item.ad_address
                                    } 
                                </p>
                                <p><strong> 
                                    {
                                    item.title.length> 30 ? 
                                    item.title.slice(0,28) + "..." :
                                    item.title
                                    } 
                                </strong></p>
                                <p> {item.posted_at} </p>
                                
                                
                                <Link className="link linkhome" to={{
                                    pathname: "/posts/" + item.post_id,
                                    state: {
                                        fromHome: true
                                    }
                                }} >
                                <p className='homead-price-details'>
                                    $&nbsp;{item.price }
                                    
                                </p>
                                <p className="homead-view-details">
                                    <strong>
                                        <FontAwesomeIcon icon="eye" />
                                        &nbsp;View Details
                                    </strong>
                                </p>
                                
                                </Link>
                           </div>


                        </div>


                        
                    ))
                }
            </div>
            <div className="modal fade" data-backdrop="" id="favorite-modal" tabIndex="-1" role="dialog">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title text-success">Not Logged In?</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p><strong>Please Login to continue</strong></p>
                        </div>
                        <div className="modal-footer favorite-modal-footer">
                            <p>Want to login?<Link to="/login"><span className="text-danger">&nbsp;Login</span></Link> </p>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomePageAds
