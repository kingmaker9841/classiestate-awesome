import React, {useEffect, useState} from 'react'
import AliceCarousel from 'react-alice-carousel'
import 'react-alice-carousel/lib/alice-carousel.css';
import hero1 from './hero1.jpg';
import hero2 from './hero2.jpg';
import hero3 from '../../assets/images/hero3.jpg';
import './Carousel.css';
import axios from 'axios';

const imgItems = [
    <img src={hero3} className="slider" alt="heroimage3.jpg"/>,
    <img src={hero1} className="slider" alt="classiHomeImg.jpg" />,
    <img src={hero2} className="slider"alt="heroimage2.jpg"/>
    
]

function Carousel() {

    let [slider, setSlider] = useState([]);

    useEffect(()=>{
        axios.get('/api/v2/slider_images')
        .then(res=>{          
            setSlider(res.data);
            res.data.map(item=>{
                console.log(item.image_name);
            })
        })
        .catch(err=>{
            console.log(err);
        })
    }, [])

    return (
        <div>
            <AliceCarousel 
                autoPlayInterval={4000}
                autoPlayDirection="rtl"
                autoPlay={true}
                autoPlayStrategy={"none"}
                // fadeOutAnimation={true}
                // mouseTrackingEnabled={false}
                // playButtonEnabled={false}
                // disableAutoPlayOnAction={false} 
                // dotsDisabled={true}
                // buttonsDisabled={true}
                disableButtonsControls={true}
                disableDotsControls={true}
                controlsStrategy={"responsive"}

                items={slider?.length > 0 ? (slider.map(item=>(
                    <img src={item.image_name} className="slider" alt={item.id} />
                ))) : (
                    imgItems
                )}
            />

        </div>
    )
}

export default Carousel;
