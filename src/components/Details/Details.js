import React, {useState, useEffect} from 'react';
import Navigation from '../Navigation/Navigation';
import axios from 'axios';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import './Details.css';
import useDocTitle from '../../customHooks/useDocTitle';
import Slider from 'react-slick';
import Footer from '../Footer/Footer';
import Map from '../Map/Map';
import ClassieIcon from '../../assets/images/classibazaar-icon.png';
import ClassiLogo from '../../assets/images/classi-logo.png';
import Modal from '../../library/Modal/Modal';

function Details(props) {

    const {match: {params}} = props;

    let [sharing, setSharing] = useState(false);
    let [saving, setSaving] = useState(false);
    let [lightBox, setLightBox] = useState(false);
    let [adDetails, setAdDetails] = useState({});
    let [docTitle, setDocTitle] = useState('');
    let [numberOfSlides, setNumberOfSlides] = useState(1);
    let [settings, setSetttings] = useState({
        dots: false,
        infinite: true,
        sliderToShow: numberOfSlides,
        sliderToScroll: 1,
        rtl: true,
        autoplay: true,
        autoPlaySpeed: 2000,
        focusOnSelect: true,
        arrows: true
    })
    let [adFeatures, setAdFeatures] = useState([]);

    //Agent Image
    let [agentImg, setAgentImg] = useState('');
    let [agentPhoneClick, setAgentPhoneClick] = useState(false);
    
    //Agent Contact Form
    let [title, setTitle] = useState('');
    let [fname, setFname] = useState('');
    let [lname, setLname] = useState('');
    let [email, setEmail] = useState('');
    let [comment, setComment] = useState('');
    let [postMessageSuccess, setPostMessageSuccess] = useState('');
    let [postMessageError, setPostMessageError] = useState('');

    const onTitleChange = e=> setTitle(e.target.value)
    const onfnameChange = e=> setFname(e.target.value)
    const onlnameChange = e=> setLname(e.target.value)
    const onEmailChange = e=> setEmail(e.target.value);
    const onCommentChange = e=> setComment(e.target.value);

    //Page Title
    useDocTitle(docTitle);

    useEffect(()=>{

        let isMounted = true;

        //Fetching post details

        axios.get(`/api/v2/ads_detail/${params.id}`)
        .then(response=>{
            if (isMounted){
                console.log(response.data);
    
                //If fetch success and set the details
                response.status === 200 &&
                setAdDetails(response.data)
                
                setAdFeatures((response.data.post_features.filter(item=> item.title === 'Ad Features'))[0].value);
                
                setNumberOfSlides(response.data.images.length || 1)
                setDocTitle(response.data.title);
            }
         
        })
        .catch(error =>{
            console.log(error);
            console.log(error.response);
            // setError('Cannot find that Ad');
        })

        //Agent Image
        setAgentImg(
            adDetails.post_owner && adDetails.post_owner.image && adDetails.post_owner.image ? adDetails.post_owner.image :
                    (adDetails.post_owner && adDetails.post_owner.company_logo && adDetails.post_owner.company_logo ?
                        adDetails.post_owner.company_logo:
                        (adDetails.post_owner && adDetails.post_owner.company_logo_thumb && adDetails.post_owner.company_logo_thumb?
                            adDetails.post_owner.company_logo_thumb:
                            ClassieIcon))
        )

        //For sticky agent box
        // window.addEventListener('scroll', agentScrollHandler);

        //For Favorite
        let fav = document.getElementsByClassName('details-photo-icon-save')[0];
        if (localStorage.getItem('classi-favorite')){
            let get = JSON.parse(localStorage.getItem('classi-favorite'));
            if (get.includes(params.id)){
                fav.classList.add('details-icon-save-toggle');
            }
        }
        


    }, [params])

    // const agentScrollHandler = ()=>{
    //     let agentBox = document.getElementsByClassName('details-description-agentbox')[0];
    //     let body = document.body;
    //     let box = agentBox.getBoundingClientRect();
    //     let scrollTop = window.pageYOffset;
    //     let clientTop = body.clientTop;
    //     let top = box.top + scrollTop - clientTop;
    //     console.log(top);
    //     if (window.pageYOffset + 100 > top) {
    //         agentBox.classList.add('agent-sticky');
    //     }else{
    //         agentBox.classList.remove('agent-sticky');
    //     }
    // }

    const phoneToggle = (e)=> {
        setAgentPhoneClick(true);

    }

    const readmoreHandler = e=>{
        let readmore = document.getElementsByClassName('details-readmore')[0];
        let more = document.getElementsByClassName('details-more')[0];
        let org = document.getElementsByClassName('details-description-text')[0];
        if (more.style.display === 'none') {
            org.style.display = 'none';
            more.style.display = 'block';
            readmore.innerHTML = 'Read Less'

        }else {
            more.style.display = 'none';
            org.style.display = 'block'
            readmore.innerHTML = 'Read More'
        }

    }

    const savingHandler = e=>{
        let fav = document.getElementsByClassName('details-photo-icon-save')[0];
        if (localStorage.getItem('classi-favorite')){
            let get = JSON.parse(localStorage.getItem('classi-favorite'));
            if (get.includes(params.id)){
                localStorage.setItem('classi-favorite', JSON.stringify(get).replace(`${params.id}`, ''));
                fav.classList.remove('details-icon-save-toggle');
            }else {
                localStorage.setItem('classi-favorite', JSON.stringify(get + "," + params.id));
                fav.classList.add('details-icon-save-toggle');
            }
        }else {
            localStorage.setItem('classi-favorite', JSON.stringify(params.id));
            fav.classList.add('details-icon-save-toggle');
        }
    }

    const contactSubmitHandler = e=>{
        e.preventDefault();
        console.log(fname,lname,email,comment);
        axios.post('/api/deal/contact', {
             first_name: fname, last_name: lname, email: email,
             msg: comment, contact_from: 'ClassiEstate'
        })
        .then(response=>{
            console.log(response)
            console.log(response.data);
            response.status === 200 && setPostMessageError('');
            response.status === 200 && setPostMessageSuccess(response.data.message);
        })
        .catch(error=>{
            console.log(error);
            console.log(error.response);
            setPostMessageSuccess('');
            if (error.response.status >= 400 ){
                setPostMessageError(error.response.data.message);
            }
        })
    }

    return (
        <>
           <Navigation scroll={true} search={true} />
           <div className="details-container">

                {/* Slick Slider */}
                <div className="details-slider" onClick={()=> setLightBox(!lightBox)}>
                    <Slider {...settings}>
                        {
                           adDetails.images && adDetails.images.map((item,index)=>(
                                <div key={index}>
                                    <img src={item.image_name} alt={index + '.jpg'} className="details-slider-image" />
                                </div>
                            ))
                        }
                    </Slider>
                </div>

                {
                    lightBox && 
                    (<Modal imgArr={adDetails.images && adDetails.images.map(item=> item.image_name)} open={lightBox} />)
                }

                <div className="modal fade" id="shareModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content share-modal-content col-12">
                            <div className="modal-header">
                                <h5 className="modal-title share-modal-title">Share</h5> <button type="button" className="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                            </div>
                            <div className="modal-body share-modal-body">
                                <div className="icon-container1 d-flex">
                                    <div className="smd"> <FontAwesomeIcon icon={["fab", "twitter"]} className="img-thumbnail faba" style={{color:"#4c6ef5",backgroundColor: "aliceblue"}} />
                                        <p>Twitter</p>
                                    </div>
                                    <div className="smd"> <FontAwesomeIcon icon={["fab", "facebook"]} className="img-thumbnail faba " style={{color: "#3b5998",backgroundColor: "#eceff5"}}/>
                                        <p>Facebook</p>
                                    </div>
                                    <div className="smd"> <FontAwesomeIcon icon={["fab", "reddit-alien"]} className="img-thumbnail faba" style={{color: "#FF5700" , backgroundColor: "#fdd9ce"}} />
                                        <p>Reddit</p>
                                    </div>
                                </div>
                                <div className="icon-container2 d-flex">
                                    <div className="smd"> <FontAwesomeIcon icon={["fab", "whatsapp"]} className="img-thumbnail faba " style={{color: "#25D366",backgroundColor: "#cef5dc"}} />
                                        <p>Whatsapp</p>
                                    </div>
                                    <div className="smd"> <FontAwesomeIcon icon={["fab", "facebook-messenger"]} className="img-thumbnail faba " style={{color: "#3b5998", backgroundColor: "#eceff5"}} />
                                        <p>Messenger</p>
                                    </div>
        
                                </div>
                            </div>
                            <div className="modal-footer share-modal-footer"> <label style={{fontWeight: "600"}}>Page Link <span className="share-message"></span></label><br />
                                <div className="row"> <input className="col-10 share-ur ur" type="url" defaultValue={`https://www.classiestate.com.au/posts/${params.id}`} id="myInput" aria-describedby="inputGroup-sizing-default" style={{height: "40px"}} /> 
                                    <button className="cpy share-cpy">
                                        <FontAwesomeIcon icon="clone" className="share-clone-icon"/>
                                    </button> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* Photos Save Share */}
                <div className="details-photo-save-share">
                    <div className="details-photos" onClick={()=> setLightBox(!lightBox)}>
                        <div className="details-fontawesome">
                            <FontAwesomeIcon icon="images" className="details-photo-icon" />
                        </div>
                        <div>&nbsp;&nbsp;Photos&nbsp;&nbsp;</div>
                        <div className="details-images-length"> {adDetails.images && adDetails.images.length} </div>
                    </div>
                    <div className="details-save" onClick={(e)=> savingHandler(e)}>
                        <div className="details-fontawesome">
                            <FontAwesomeIcon icon="star" className="details-photo-icon details-photo-icon-save" />
                        </div>
                        <div>&nbsp;&nbsp;Save</div>
                    </div>
                    <div className="details-share" data-toggle="modal" data-target="#shareModal" >
                        <div className="details-fontawesome">
                            <FontAwesomeIcon icon="share" className="details-photo-icon" />
                        </div>
                        <div>&nbsp;&nbsp;Share</div>
                    </div>
                </div>

                {/* Title and Ad by Open */}
                <div className="details-title-adby">

                    {/* Ad Title */}
                    <div className="details-ad-title">
                        <h3> {adDetails.title} </h3>
                    </div>

                    {/* Ad Category */}
                    {/* <div className="details-categories">
                    {
                        adDetails.categories && 
                        adDetails.categories.map((cat, index)=>(
                            <span key={index} className="details-categories-text">
                                &gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {cat}   
                            </span>
                        ))
                    }
                    </div> */}

                    {/* Ad Post owner Details  */}
                    <div className="details-adby">
                        <span className="details-adby-name">
                            <em> By: {adDetails.post_owner && adDetails.post_owner.first_name} </em>
                            &nbsp;&nbsp;/&nbsp;
                        </span>
                        <span className="details-adby-id">
                            <em> Ad id : {adDetails.post_id} </em>
                            &nbsp;&nbsp;/&nbsp;
                        </span>
                        <span className="details-adby-address">
                            <em> Address: {adDetails.ad_address} </em>
                            &nbsp;&nbsp;/&nbsp;
                        </span>
                        <span className="details-adby-views">
                            <FontAwesomeIcon icon="eye" />
                            &nbsp;&nbsp; {adDetails.no_of_seen ? adDetails.no_of_seen : adDetails.view_count}
                        </span>
                    </div>

                    {/* Bed, Bath Icons */}

                    <div className="details-buyers-guide-icons">
                        <div className="details-buyers-bed">
                            <FontAwesomeIcon icon="bed" className="details-buyers-icon" />
                            &nbsp; {adDetails.no_room? adDetails.no_room: 0}
                        </div>
                        <div className="details-buyers-bathroom">
                            <FontAwesomeIcon icon="sink" className="details-buyers-icon" />
                            &nbsp; {adDetails.no_of_bathroom? adDetails.no_of_bathroom: 0}
                        </div>
                        <div className="details-buyers-parking">
                            <FontAwesomeIcon icon="parking" className="details-buyers-icon" />
                            &nbsp; {adDetails.no_of_car_space? adDetails.no_of_car_space: 0}
                        </div>
                        <div className="details-buyers-area">
                            <FontAwesomeIcon icon="vector-square" className="details-buyers-icon" />
                            &nbsp; {adDetails.area? adDetails.area: 0}
                        </div>
                    </div>
                                    
                </div>
                {/* Title and Adby close */}

            {/* Enclosing Description, Location, Inspection Divs */}
            <div className="details-sub-features">

                {/* Horizontal Divider */}
                <div className="details-divider" ></div>



                    {/* Agent Profile Right Align */}

                    <div className="details-description-agentbox">
                        <div className="details-agentbox-pricename">
                            <h4> $
                                {
                                adDetails.price ? adDetails.price : 0
                                } 
                            </h4>
                            <p><strong>
                                {
                                    adDetails.post_owner ? 
                                    (adDetails.post_owner.first_name + " " + adDetails.post_owner.last_name)
                                    : (<em>Not Specified</em>)
                                }
                            </strong></p>
                        </div>

                        {/* Horizontal line */}
                        <div className="details-agentbox-line"></div>

                        {/*Agent Image */}
                        <div className="details-agentbox-imgalign">
                            <div className="details-agentbox-image">
                                <img src={agentImg} alt={adDetails.name} className="details-agentbox-imagetag" />
                            </div>
                        </div>
                                
                        {/* Agent Name */}
                        <center>
                            <div className="details-agentbox-name mt-2 mb-5">
                                    <h3> {adDetails.name} </h3>
                            </div>
                        </center>

                        {/* Agent Phone number */}
                        <div className="details-agentbox-phone" onClick={(e)=> phoneToggle(e)}>
                                {
                                    agentPhoneClick ?
                                    (<p> {adDetails.phone} </p>) :
                                    (<p>
                                        <FontAwesomeIcon icon="phone" />
                                    <span className="ml-2"> Click to Show Number </span>  
                                    </p>)
                                }
                        </div>

                        {/*Agent Contact */}
                        <div className="details-agentbox-contact">
                            
                            <div className="agent-contact" data-toggle="modal" data-target="#myModal">
                                <p> Contact </p>
                            </div>
                            <div id="myModal" className="modal fade" tabIndex="-1">
                                <div className="modal-dialog">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            {/* <h5 className="modal-title">Contact</h5> */}
                                            <img src={ClassiLogo} width="200" height="100" />
                                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div className="modal-body">

                                            <form className="">
                                                <div className="form-row">
                                                    <div className="form-group col-md-12">
                                                        <label htmlFor="title">Title</label>
                                                        <input type="text" className="form-control" value={title} id="title" onChange={(e)=> onTitleChange(e)} />
                                                    </div>
                                                </div>
                                                <div className="form-row">
                                                    <div className="form-group col-md-12">
                                                        <label htmlFor="fname">First Name</label>
                                                        <input type="text" className="form-control" value={fname} id="fname" onChange={(e)=> onfnameChange(e)} />
                                                    </div>
                                                </div>
                                                <div className="form-row">
                                                    <div className="form-group col-md-12">
                                                        <label htmlFor="lname">Last Name</label>
                                                        <input type="text" className="form-control" value={lname} id="lname" onChange={(e)=> onlnameChange(e)} />
                                                    </div>
                                                </div>
                                                <div className="form-row">
                                                    <div className="form-group col-md-12">
                                                        <label htmlFor="email">Email</label>
                                                        <input type="text" className="form-control" value={email} id="email" onChange={(e)=> onEmailChange(e)} />
                                                    </div>
                                                </div>
                                                <div className="form-row">
                                                    <div className="form-group col-md-12">
                                                        <label htmlFor="comment">Comment</label>
                                                        <textarea type="text" className="form-control" value={comment} id="comment" onChange={(e)=> onCommentChange(e)} ></textarea>
                                                    </div>
                                                </div>
                                            
                                                </form> 
                                        </div>
                                        {
                                            postMessageSuccess 
                                            ? (
                                                <div className="mt-2 mb-2 alert alert-success" role="alert">
                                                    {postMessageSuccess}
                                                </div>
                                            )
                                            :null
                                        }
                                        {
                                            postMessageError
                                            ? (
                                                <div className="mt-2 mb-2 alert alert-danger" role="alert">
                                                    {postMessageError}
                                                </div>
                                            )
                                            :null
                                        }

                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                            <button type="button"  onClick={e=> contactSubmitHandler(e)} className="btn" style={{backgroundColor: 'var(--primary-color)', color: '#f4f4f4'}}>Submit</button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        

                        </div>
                        
                    </div>



                    {/* Agent Closed */}

                 {/* Buyers Guide
                <div className="details-buyers-guide">
                    <h3>Buyers guide ${adDetails.price?adDetails.price:0} </h3>
                    <p> {adDetails.ad_address?adDetails.ad_address: (<em>user did not disclose the address</em>)} </p>
                    <div className="details-buyers-guide-icons">
                        <div className="details-buyers-bed">
                            <FontAwesomeIcon icon="bed" className="details-buyers-icon" />
                            &nbsp; {adDetails.no_room? adDetails.no_room: 0}
                        </div>
                        <div className="details-buyers-bathroom">
                            <FontAwesomeIcon icon="sink" className="details-buyers-icon" />
                            &nbsp; {adDetails.no_of_bathroom? adDetails.no_of_bathroom: 0}
                        </div>
                        <div className="details-buyers-parking">
                            <FontAwesomeIcon icon="parking" className="details-buyers-icon" />
                            &nbsp; {adDetails.no_of_car_space? adDetails.no_of_car_space: 0}
                        </div>
                        <div className="details-buyers-area">
                            <FontAwesomeIcon icon="vector-square" className="details-buyers-icon" />
                            &nbsp; {adDetails.no_room? adDetails.no_room: 0}
                        </div>
                    </div>
                </div> */}
                            
                    <div className="details-description">
                        <h3 className="mb-4">Description</h3>
                        <small> {adDetails.ad_address || (<em>not specified</em>)} </small>
                        <span>&nbsp;,<small> Posted: {adDetails.posted_at}</small></span>
                        <p className="details-description-text text-justify">
                            {adDetails.description && adDetails.description.length >150
                            ?adDetails.description.slice(0,150)
                            :adDetails.description}
                        
                        </p>
                        <p className="details-more text-justify"> {adDetails.description && adDetails.description}</p>
                        <div className="details-readmore" onClick={e=> readmoreHandler(e)}>
                            <strong >Read More</strong> 
                        </div>
                    </div>

                    {/* Horizontal Divider */}
                    <div className="details-divider" ></div>
                
                    <div className="details-property-features">
                        <h3 className="mb-4">Property Features</h3>
                        <div className="details-features-text-icon">
                            {
                                adFeatures && adFeatures.map((item,index)=> (
                                    <div key={index} className="details-features-items">
                                        <div className="features-icon"> 
                                            <img src={item.image_name} alt={index +'.png'} className="features-icon-image" />
                                        </div>
                                        <div className="features-text"> {item.title} </div>
                                    </div>
                                ))
                            }
                        </div>
                    </div>

                    {/* Horizontal Divider */}
                    <div className="details-divider details-divider-features" ></div>

                    <div className="details-location">
                        <h3 className="mb-4">Property Location</h3>
                        <p className="mb-4"> {adDetails.ad_address} </p>
                        <div style={{height: '60vh', width: '80vw'}}>
                            <Map 
                                lat={adDetails.latitude} 
                                lng={adDetails.longitude} 
                                text="Marker"
                                class='details-map-class' 
                            />     
                        </div>
                    </div>

                    {/* Horizontal Divider */}
                    <div className="details-divider details-divider-features" ></div>

                    <div className="details-inspection">
                        <h3 className="mb-4">Inspection</h3>
                        <div className="inspection-grid">
                            {/* <div className="inspection-contact">
                                <h4 className="p-2">Private Inspection</h4>
                                <p className="text-muted p-2 text-justify">
                                    Contact the agent to request a private inspection
                                </p>
                                <p className="p-2 inspection-contact-agent">Contact agent</p>
                            </div> */}
                            <div className="inspection-dates">
                                {
                                    adDetails.inspection_times &&
                                    adDetails.inspection_times.map((item, index)=>(
                                        <div key={index} className="inspection-date-time">
                                            <div className="inspection-date">
                                                <div className="inspection-date-green"><strong>{item.inspection_date}</strong></div>
                                                <div><small>
                                                    {item.start_hr}:{item.start_min} 
                                                    &nbsp;{item.start_time}&nbsp;
                                                    to&nbsp;{item.end_hr}:{item.end_min}
                                                    &nbsp;{item.end_time}
                                                </small></div>
                                            </div>
                                            <div className="inspection-calendar">
                                                <FontAwesomeIcon icon="calendar" className="inspection-date-icon" />
                                            </div>
                                        </div>
                                    ))
                                }
                                <div className="inspection-date-time">
                                    <div className="inspection-date">
                                        <div className="inspection-date-green"><strong>10-12-2020</strong></div>
                                        <div><small>
                                                08:23 am to 10:45 am
                                        </small></div>
                                    </div>
                                    <div className="inspection-calendar">
                                        <FontAwesomeIcon icon="calendar" className="inspection-date-icon" />
                                    </div>
                                </div>   
                                <div className="inspection-date-time">
                                    <div className="inspection-date">
                                        <div className="inspection-date-green"><strong>10-12-2020</strong></div>
                                        <div><small>
                                                08:23 am to 10:45 am
                                        </small></div>
                                    </div>
                                    <div className="inspection-calendar">
                                        <FontAwesomeIcon icon="calendar" className="inspection-date-icon"/>
                                    </div>
                                </div>   
                                <div className="inspection-date-time">
                                    <div className="inspection-date">
                                        <div className="inspection-date-green"><strong>10-12-2020</strong></div>
                                        <div><small>
                                                08:23 am to 10:45 am
                                        </small></div>
                                    </div>
                                    <div className="inspection-calendar">
                                        <FontAwesomeIcon icon="calendar" className="inspection-date-icon"/>
                                    </div>
                                </div>   
                                
                                {
                                    adDetails.inpsection_times &&
                                    !adDetails.inpsection_times.length
                                    ?(<p><em>No inspection time available</em></p>)
                                    :null
                                }
                            </div>

                        </div>
                    </div>
                </div>
            </div> 
            <Footer />
        </>
    )
}

export default Details
