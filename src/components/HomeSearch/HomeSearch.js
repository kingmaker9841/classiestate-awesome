import React, {useEffect, useState} from 'react';
import './HomeSearch.css';
import {useHistory} from 'react-router-dom';
// import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import ClassiLogo from '../../assets/images/classi-logo.png';

function Search(props) {

    let [buyRentSell, setBuyRentSell] = useState('buy');

    let [searchProperty, setSearchProperty] = useState('');
    let [priceMin, setPriceMin] = useState('');
    let [priceMax, setPriceMax] = useState('');
    let [areaMin, setAreaMin] = useState('');
    let [areaMax, setAreaMax] = useState('');

    const history = useHistory();
    
    let buy = document.querySelector('.buy');
    let rent = document.getElementsByClassName('rent')[0];
    let buyTriangle = document.getElementsByClassName('buy-triangle')[0];
    let rentTriangle = document.getElementsByClassName('rent-triangle')[0];
    // if(buy.classList.contains('active-search')){
    //     rentTriangle.classList.toggle('rent-triangle-toggle');
    // }
    // if(rent.classList.contains('active-search')){
    //     buyTriangle.classList.toggle('buy-triangle-toggle');
    // }

    useEffect(()=>{
        let buyRentText = document.getElementsByClassName('search-buyrent-text');
        let triCommon = document.getElementsByClassName('triangle-common');
        for (let i=0; i< buyRentText.length; i++){
            buyRentText[i].addEventListener('click', (e)=>{
                let current = document.getElementsByClassName('active-search');
                let activeTri = document.getElementsByClassName('active-triangle');
                current[0].className = current[0].className.replace(' active-search', '');
                activeTri[0].className = activeTri[0].className.replace(' active-triangle', '');
                buyRentText[i].className += ' active-search';
                triCommon[i].className += ' active-triangle';
            })
        }
    }, []);

    const searchPropertyChange = e=> setSearchProperty(e.target.value);
    const priceMinChange = e=> setPriceMin(e.target.value);
    const priceMaxChange = e=> setPriceMax(e.target.value);
    const areaMinChange = e=> setAreaMin(e.target.value);
    const areaMaxChange = e=> setAreaMax(e.target.value);

    const onSubmitHandler = e=>{
        e.preventDefault();
        history.push(`/listing?property_category=${buyRentSell}&property_type=${searchProperty}&price_min=${priceMin}&price_max=${priceMax}&area_min=${areaMin}&area_max=${areaMax}`);
    }
    return (
        <div className="search-container">
            <div className="search-div-container">
                <div className="search-text-top">
                    <h1>The #1 place for property</h1>
                    <h4>Search Australia's home of property</h4>
                </div>
                <div className="search-group-text">

                    {/* <div className="title-img">
                    <h1 className="search-title mt-4">Welcome to &nbsp;</h1>
                        <img src={ClassiLogo} className="search-logo-img" width="150px" height="80px" alt="classiEstateLogo.png"/>
                    </div> */}

                    <div className="search-buyrent">
                        <div className="search-buyrent-text buy active-search" id="buy-id" onClick={()=> setBuyRentSell('buy')}>
                            Buy
                            <div className="triangle-common buy-triangle active-triangle"></div>
                        </div>
                        <div className="search-buyrent-text rent" onClick={()=> setBuyRentSell('rent')}>
                            Rent
                            <div className="triangle-common rent-triangle"></div>
                        </div>
                    </div>
                </div>
                <section className="search-sec">
                    <div className="container">
                        <form onSubmit={(e)=> onSubmitHandler(e)} noValidate="novalidate">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="row">
                                        <div className="col-lg-9 col-md-9 col-sm-12 p-0 ">
                                            <input type="search" className="form-control search-slt" placeholder="Enter Location" />
                                        </div>
                                        
                                        <div className="col-lg-3 col-md-3 col-sm-12 p-0">
                                            <button type="submit" className="btn btn-success wrn-btn">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row col-lg-16 search-select-form">
                                <div className="form-group col-lg-4 p-1 search-any-property">
                                    <select className="form-control input-search-width" onChange={e=> searchPropertyChange(e)}>
                                        <option value="">Any Property Type</option>
                                        <option value="house">House</option>
                                        <option value="apartment">Apartment</option>
                                        <option value="townhouse">Town House</option>
                                        <option value="land">Land</option>
                                        <option value="rural">Rural</option>
                                        <option value="new-apartment">New Apartment</option>
                                        <option value="commercial">Commercial</option>
                                    </select>
                                </div>
                                <div className="form-group col-lg-2 p-1 search-min-price">
                                    <select className="form-control input-search-width" onChange={e=> priceMinChange(e)}>
                                        <option value="">Price (min)</option>
                                        <option value="20000">$20000</option>
                                        <option value="40000">$40000</option>
                                        <option value="80000">$80000</option>
                                        <option value="100000">$100000</option>
                                        <option value="200000">$200000</option>
                                        <option value="500000">$500000</option>
                                        <option value="1000000">$1000000</option>
                                    </select>
                                </div>
                                <div className="form-group col-lg-2 p-1 search-max-price ">
                                    <select className="form-control input-search-width" onChange={e=> priceMaxChange(e)}>
                                        <option value="">Price (max)</option>
                                        <option value="80000">$80000</option>
                                        <option value="100000">$100000</option>
                                        <option value="200000">$200000</option>
                                        <option value="500000">$500000</option>
                                        <option value="1000000">$1000000</option>
                                        <option value="1000000">$2000000</option>
                                        <option value="1000000">$5000000</option>
                                    </select>
                                </div>
                                <div className="form-group col-lg-2 p-1 search-min-area">
                                    <select className="form-control input-search-width" onChange={e=> areaMinChange(e)}>
                                        <option value="">Area (min)</option>
                                        <option value="10">10 sq. meter</option>
                                        <option value="20">20 sq. meter</option>
                                        <option value="50">50 sq. meter</option>
                                        <option value="100">100 sq. meter</option>
                                        <option value="200">200 sq. meter</option>
                                        <option value="500">500 sq. meter</option>
                                        <option value="1000">1000 sq. meter</option>
                                        <option value="5000">5000 sq. meter</option>
                                    </select>
                                </div>
                                <div className="form-group col-lg-2 p-1 search-max-area">
                                    <select className="form-control input-search-width" onChange={e=> areaMaxChange(e)}>
                                        <option value="">Area (max)</option>
                                        <option value="100">100 sq. meter</option>
                                        <option value="200">200 sq. meter</option>
                                        <option value="500">500 sq. meter</option>
                                        <option value="1000">1000 sq. meter</option>
                                        <option value="5000">5000 sq. meter</option>
                                        <option value="10000">10000 sq. meter</option>
                                        <option value="20000">20000 sq. meter</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    )
}

export default Search
