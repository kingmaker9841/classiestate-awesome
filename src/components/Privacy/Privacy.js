import React from 'react'
import Footer from '../Footer/Footer';
import Navigation from '../Navigation/Navigation';

function Privacy() {
    return (
        <>
        <Navigation line={true} scroll={true} search={true} inactive="inactive"/>
        <div className="terms-container">
            
        <h3><strong>Privacy Policy </strong></h3>
        <p className="mt-3">
          ClassiEstate Terms should be maintaiined.
          <strong>And updated too.</strong>
        </p>
        <p>Terms should be implemented and well .......</p>
        <h6 className="mt-4"><strong>This Policy describes:</strong></h6>
        <ol className="mt-4 mb-3">
          <li>How We Collect & Use Your Personal Data</li>
          <li> Cookies & Similar Technologies</li>
          <li> How We Disclose Personal Data</li>
          <li> How to Access & Control Your Personal Data</li>
          <li> How We Protect Your Personal Data</li>
          <li> How We Process Children’s Personal Data</li>
          <li> Third-Party Providers and Their Services</li>
          <li> International Transfers of Your Personal Data</li>
          <li> Updates to This Policy</li>
          <li> How to Contact Us</li>
        </ol>
        <h3 className="mt-5"><strong>1. How We Collect & Use Your Personal Data</strong></h3>
        <p className="mt-4">
          Commodo viverra maecenas accumsan lacus vel facilisis elit sed
          vulputate. Vitae congue mauris rhoncus aenean vel elit scelerisque.
          Volutpat sed cras ornare arcu dui vivamus arcu felis bibendum. Est
          velit egestas dui id ornare arcu odio ut. Risus feugiat in ante metus.
          Volutpat sed cras ornare arcu dui vivamus.
        </p>
        <p>
          Commodo viverra maecenas accumsan lacus vel facilisis elit sed
          vulputate. Vitae congue mauris rhoncus aenean vel elit scelerisque.
          Volutpat sed cras ornare arcu dui vivamus arcu felis bibendum. Est
          velit egestas dui id ornare arcu odio ut. Risus feugiat in ante metus.
          Volutpat sed cras ornare arcu dui vivamus.
        </p>

        </div>
        <Footer />
        </>
    )
}

export default Privacy
