import React,{useEffect, useState} from 'react';
import {Link, NavLink} from 'react-router-dom'
import './Navigation.css';
import '../Carousel/Carousel';
import axios from 'axios';
import ClassiLogo from '../../assets/images/classi-logo.png';

function Navigation(props) {
    
    //State to check logged in
    let [loggedIn, setLoggedIn] = useState(false);
    let [username, setUsername] = useState('');
    let [userImageUrl, setUserImageUrl] = useState('');

    const scrollHandler = (e)=>{
        let navContainer = document.getElementsByClassName('nav-container')[0];
        if (window.pageYOffset > 10) {
            navContainer.classList.add('navBackground')
            props.fromHome && navContainer.classList.remove('nav-home-background')
        }else {
            props.fromHome && navContainer.classList.add('nav-home-background')
            navContainer.classList.remove('navBackground');
        }
    }

    const hamburgerClick= (e) =>{
        let line1 = document.querySelector('.line1');
        let line2 = document.querySelector('.line2');
        let line3 = document.querySelector('.line3');
        let hamburgerSlide = document.querySelector('.hamburger-slide');
        let navLoggedIn = document.querySelector('.nav-loggedin');

        navLoggedIn && navLoggedIn.classList.toggle('nav-loggedin-open');
        hamburgerSlide && hamburgerSlide.classList.toggle('hamburger-slide-open');
        document && document.body.classList.toggle('body-overflow');
        line1 && line1.classList.toggle('line1-open');
        line2 && line2.classList.toggle('line2-open');
        line3 && line3.classList.toggle('line3-open');
    }

    useEffect(()=>{

        //To display small horizontal border line at bottom of navigation bar
        let navContainer = document.getElementsByClassName('nav-container')[0];
        props.line && navContainer.classList.add('nav-container-border-line');
        
        //Get User Profile
        axios.get('/api/v2/profile')

        .then(response=>{
            console.log(response);
            console.log(response.data);
            if (response.status === 200) {
                setLoggedIn(true);
                setUsername(response.data.first_name);
                response.data.image_url ? setUserImageUrl(response.data.image_url)
                    : setUserImageUrl(response.data.thumbnail_url)
            }
        })
        .catch(error=> {
            console.log(error);
            console.log(error.response)
        })

        //Navigation backgroundColor Toggle (White <> Transparent)
        window.addEventListener('scroll', scrollHandler);                       

        //SideBar
        let navLines = document.querySelector('.nav-lines');
        navLines.addEventListener('click', hamburgerClick);

        //Unmounting 
        return ()=>{
            window.removeEventListener('scroll', scrollHandler);
            navLines.removeEventListener('click', hamburgerClick);
    
        }
    }, [])
    return (
        <div className={props.fromHome ? 'nav-container nav-home-background': 'nav-container'}>
            <div className="nav-logo">
                <Link to="/" className="link">
                    <img src={ClassiLogo} className="ml-5" width="200px" height="100px" alt="classiEstateLogo.png"/>
                </Link>
            </div>
            <div className="nav-links">
                <div className="nav-link nav-home active">
                    <NavLink to="/" exact activeClassName="active-link" className="link link-nav ">
                        <span className="">Home</span>
                    </NavLink>
                </div>
                {/* <div className="nav-link nav-listing">
                    <NavLink to="/listing" exact activeClassName="active-link" className="link link-nav">
                        <span className="">Listing</span>
                    </NavLink>
                </div> */}
                <div className="nav-link nav-buy">
                    <NavLink to="/listing/buy" activeClassName="active-link" className="link link-nav">
                        <span className="">Buy</span>
                    </NavLink>
                </div>
                <div className="nav-link nav-rent">
                    <NavLink to="/listing/rent" activeClassName="active-link" className="link link-nav">
                        <span className="">Rent</span>
                    </NavLink>
                </div>
                <div className="nav-link nav-agents">
                    <NavLink to="/agents" exact activeClassName="active-link" className="link link-nav">
                        <span className="">Find Agents</span>
                    </NavLink>
                </div>
                <div className="nav-link nav-blog">
                    <NavLink to="/blog" exact activeClassName="active-link" className="link link-nav">
                        <span className="">Blog</span>
                    </NavLink>
                </div>
            </div>

            {/* Check if logged In */}
            {
                loggedIn ? (
                    <div className="nav-loggedin">
                        <div className="nav-loggedin-username">
                           <strong> Hi, {username} </strong>
                        </div>
                        <div className="nav-loggedin-userimage dropdown-profile dropdown">
                            <img src={userImageUrl} className="nav-loggedin-profileimage dropdown-profile-toggle dropdown-toggle" id="dropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" alt="username.jpg"/>
                            <div className="dropdown-menu" aria-labelledby="dropdownProfile">
                                <a className="dropdown-item dropdown-item-profile" href="/profile/listing">View Profile</a>
                                <a className="dropdown-item dropdown-item-profile" href="/account-settings/edit-profile">Account Settings</a>
                                <Link className="dropdown-item dropdown-item-profile" to={{
                                    pathname: '/login',
                                    state: {
                                        fromLogout: true
                                    }
                                }} >Logout</Link>
                            </div>
                        </div>
                    </div>
                ) : (
                <div className="nav-authenticate">
                    <div className="nav-link nav-signin">
                        <Link to="/login" className="link link-nav">
                            <span className="">Sign In</span>
                        </Link>
                    </div>
                    <div className="nav-signup">
                        <Link to="/register" className="link">
                            <button className="signup btn">Sign Up</button>
                        </Link>
                    </div>
                </div>
                )
            }
            
            <div className="nav-hamburger">
                <div className="nav-lines">
                    <span className="nav-line line1"></span>
                    <span className="nav-line line2"></span>
                    <span className="nav-line line3"></span>
                </div>
            </div>
            <div className="hamburger-slide">

                {/* Check if Logged In */}
                {
                    loggedIn ? (

                        <div className="ham-authenticate">
                            <h4>Welcome, {username} </h4>
                        </div>
                        
                        ) : (

                    <div className="ham-authenticate">
                        <div className="ham-signin">
                            <Link to="/login" className="link link-ham">
                                <span className="ham-signin-link">Sign In</span>
                            </Link>
                        </div>
                        <div className="ham-signup">
                            <Link to="/register" className="link">
                                <button className="signup btn btn-success">Sign Up</button>
                            </Link>
                        </div>
                    </div>
                    )
                }

                <div className="ham-links">
                    <NavLink to="/" exact activeClassName="active-ham" className="ham link-ham">
                    <div className="ham-link ham-home">
                        
                            <span className="">Home</span>
                        
                    </div>
                    </NavLink>
                    {/* <NavLink to="/listing" exact activeClassName="active-ham" className="ham link-ham">
                    <div className="ham-link ham-listing">
                        
                            <span className="">Listing</span>
                        
                    </div>
                    </NavLink> */}
                    <NavLink to="/listing/buy" exact activeClassName="active-ham" className="ham link-ham">
                    <div className="ham-link ham-listing">
                        
                            <span className="">Buy</span>
                        
                    </div>
                    </NavLink>
                    <NavLink to="/listing/rent" exact activeClassName="active-ham" className="ham link-ham">
                    <div className="ham-link ham-listing">
                        
                            <span className="">Rent</span>
                        
                    </div>
                    </NavLink>
                    <NavLink to="/agents" exact activeClassName="active-ham" className="ham link-ham">
                    <div className="ham-link ham-agents">
                        
                            <span className="">Find Agents</span>
                        
                    </div>
                    </NavLink>
                    {
                    loggedIn ? (
                        <>
                        <NavLink to="/account-settings/edit-profile" activeClassName="active-ham" className="ham link-ham">
                        <div className="ham-link ham-account-settings">
                        
                            <span className="">Account Settings</span>

                        </div>   
                        </NavLink>
                        <NavLink to={{
                            pathname: '/login',
                            state: {
                                fromLogout: true
                            }
                        }} activeClassName="active-ham" className="ham link-ham">
                        <div className="ham-link ham-logout">
                        
                            <span className="">Logout</span>

                        </div>   
                        </NavLink>                        
                        </>
                    ) : null
                    }
                </div>

                {/* Slider ClassiEstate Logo */}
                <div className="ham-logo">
                    <Link to="/" >
                        <img src={ClassiLogo} className="ml-5" width="200px" height="100px" alt="classiEstateLogo.png"/>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default Navigation
