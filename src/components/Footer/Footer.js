import React from 'react';
import './Footer.css';
import {Link} from 'react-router-dom';
import footerImg from '../../assets/images/footer/footer-bg.jpg';
import ClassiIcon from '../../assets/images/classi-logo.png';

function Footer() {
    return (
        <div className="footer-container">
            <div className="footer-top-logo-text">
                <center>
                    <img src={ClassiIcon}  className="footer-classi-icon"/>
                </center>
                <center>
                    <strong>
                        <div className="footer-links">
                            <Link className="footer-link" to="/"><span>Home</span></Link>
                            <Link className="footer-link" to="/"><span>Listing</span></Link>
                            <Link className="footer-link" to="/terms"><span>Terms of Use</span></Link>
                            <Link className="footer-link" to="/privacy"><span>Privacy Policy</span></Link>
                            <Link className="footer-link" to="/"><span>Agent</span></Link>  
                        </div>
                    </strong>
                </center>
            </div>
            <img src={footerImg} alt="Footer" className="footer-svg"/>
        </div>
    )
}

export default Footer
