import React from 'react';
import AccountSettings from '../AccountSettings';
import Navigation from '../../Navigation/Navigation';
import './ChangePassword.css';

function ChangePassword() {
    return (
        <>
        <Navigation />
        <div className="change-password-container">
            <div className="change-password-account-settings">
                <AccountSettings />
            </div>
            <div className="change-password-basic-info">

            </div>
        </div>
        </>
    )
}

export default ChangePassword
