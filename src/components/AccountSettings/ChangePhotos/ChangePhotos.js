import React from 'react';
import AccountSettings from '../AccountSettings';
import Navigation from '../../Navigation/Navigation';
import './ChangePhotos.css';

function ChangePhotos() {
    return (
        <>
        <Navigation />
        <div className="change-photos-container">
            <div className="change-photos-account-settings">
                <AccountSettings />
            </div>
            <div className="change-photos-basic-info">

            </div>
        </div>
        </>
    )
}

export default ChangePhotos
