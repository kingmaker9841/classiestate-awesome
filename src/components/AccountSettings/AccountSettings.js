import React, {useState, useEffect} from 'react';
import './AccountSettings.css';
import axios from 'axios';
import {NavLink} from 'react-router-dom';

function AccountSettings() {
    let [userImage, setUserImage] = useState('');
    let [userName, setUserName] = useState('');
    useEffect(()=>{
        
        axios.get('/api/v2/profile')
        .then(res=>{
            console.log(res.data);
            setUserImage(res.data.image_url);
            setUserName(`${res.data.first_name} ${res.data.last_name}`)
        })
        .catch(err=>{
            console.log(err.response);
        })

        return ()=>{

        }
    }, [])
    return (
        <div className="account-settings-main">
            <div className="account-settins-image-alignment-div">
                <div className="account-settings-image-box">
                    <img src={userImage} alt="userimage.jpg" className="account-settings-image" />
                </div>
                <p className="mt-2"><strong> {userName} </strong></p>
                <p style={{fontSize: '0.8rem'}}>View Profile</p>
            </div>
            
            
            <div className="account-settings-nav">
            
                    <div className="account-settings-edit-profile">
                    <NavLink activeClassName="active-account-settings-nav" to="/account-settings/edit-profile">
                        <span className="account-settings-nav-links">Edit Profile</span>
                    </NavLink>
                    </div>
                    
                
                    <div className="account-settings-change-photos">
                    <NavLink activeClassName="active-account-settings-nav" to="/account-settings/change-photos">
                        <span className="account-settings-nav-links">Change Photos</span>
                    </NavLink>
                    </div>
                
                
                    <div className="account-settings-change-password">
                    <NavLink activeClassName="active-account-settings-nav" to="/account-settings/change-password">
                        <span className="account-settings-nav-links">Change Password</span>
                    </NavLink>
                    </div>
                
                
            </div>
        </div>
    )
}

export default AccountSettings
