import React, {useReducer} from 'react'
import AccountSettings from '../AccountSettings';
import Navigation from '../../Navigation/Navigation';
import useDocTitle from '../../../customHooks/useDocTitle';
import './EditProfile.css';

const initialState = {
    fName: '', lName: '', birth: '', gender: '', language: '', email: '', 
    phoneNumber: '', address: '', yourself: ''
}

const reducer = (state, action)=>{
    switch(action.type){
        case 'fNameChange' : return {...state, fName: action.payload}
        case 'lNameChange' : return {...state, lName: action.payload}
        case 'birthChange' : return {...state, birth: action.payload}
        case 'genderChange' : return {...state, gender: action.payload}
        case 'languageChange' : return {...state, language: action.payload}
        case 'emailChange' : return {...state, email: action.payload}
        case 'phoneNumberChange' : return {...state, phoneNumber: action.payload}
        case 'addressChange' : return {...state, address: action.payload}
        case 'yourselfChange' : return {...state, yourself: action.payload}
    }
}

function EditProfile() {
    const [state, dispatch] = useReducer(reducer, initialState);
    useDocTitle('Edit Profile -Classiestate');
    const onSubmitHandler = e=>{
        e.preventDefault();
        console.log("Fname", state.fName);
        console.log("lname", state.lName);
        console.log("birth", state.birth);
        console.log("gender", state.gender);
        console.log("language", state.language);
        console.log("email", state.email);
        console.log("phoneNumber", state.phoneNumber);
        console.log("address", state.address);
        console.log("yourself", state.yourself);
    }
    return (
        <>
        <Navigation />
        <div className="edit-profile-container">
            <div className="edit-profile-account-settings">
                <AccountSettings />
            </div>
            <div className="edit-profile-basic-info">
                <div className="edit-profile-basic-info-inner">
                    <h3><strong>Basic Information</strong></h3>
                    <form className="edit-profile-basic-info-form" onSubmit={(e)=>onSubmitHandler(e)}>
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label htmlFor="editProfFirstName"><strong>First Name</strong></label>
                                <input type="text" className="form-control" id="editProfFirstName"
                                onChange={e=> dispatch({type: 'fNameChange', payload: e.target.value })} />
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="editProfLastName"><strong>Last Name</strong></label>
                                <input type="text" className="form-control" id="editProfLastName"
                                onChange={(e)=> dispatch({type: 'lNameChange', payload: e.target.value})} />
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label htmlFor="edit-profile-date"><strong>Date of birth</strong></label>
                                <input type="date" placeholder="Select Date" className="form-control" id="edit-profile-date"
                                onChange={(e)=> dispatch({type:'birthChange', payload: e.target.value})} />
                            </div>
                            <div className="form-group col-md-3">
                                <label htmlFor="edit-profile-gender"><strong>I am</strong></label>
                                <select id="edit-profile-gender" className="form-control" 
                                    onChange={(e)=> dispatch({type: 'genderChange', payload: e.target.value})}>
                                    <option value=""></option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                            <div className="form-group col-md-3">
                                <label htmlFor="edit-profile-language"><strong>Prefered Language</strong></label>
                                <select id="edit-profile-language" className="form-control"
                                    onChange={(e)=> dispatch({type: 'languageChange', payload: e.target.value})}>
                                    <option value=""></option>
                                    <option value="english">English</option>
                                    <option value="spanish">Spanish</option>
                                    <option value="french">French</option>
                                    <option value="russian">Russian</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label htmlFor="edit-profile-email"><strong>Email</strong></label>
                                <input type="email" className="form-control" id="edit-profile-email"
                                onChange={(e)=> dispatch({type: 'emailChange', payload: e.target.value})}  />
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="edit-profile-number"><strong>Phone Number</strong></label>
                                <input type="number" className="form-control" id="edit-profile-number" 
                                onChange={(e)=> dispatch({type: 'phoneNumberChange', payload: e.target.value})} />
                            </div>
                        </div>
                        
                        <div className="form-group">
                            <label htmlFor="edit-profile-address"><strong>Where you live?</strong></label>
                            <input type="text" className="form-control" id="edit-profile-address" 
                            onChange={(e)=> dispatch({type: 'addressChange' , payload: e.target.value})} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="edit-profile-descrbe-yourself"><strong>Describe Yourself</strong><span className="text-muted">&nbsp;(Optional)</span></label>
                            <textarea type="text" className="form-control" id="edit-profile-descrbe-yourself" rows="8" 
                            onChange={(e)=> dispatch({type: 'yourselfChange', payload: e.target.value})} />
                        </div>
                        <button type="submit" className="btn btn-success edit-profile-submit-btn">Save Changes</button>
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}

export default EditProfile
