import React from 'react'
import GoogleMapReact from 'google-map-react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

const Any = ({text})=>(
    <div style={{
        color: 'var(--primary-color)', 
        fontSize: '2rem'

    }}> 
        <FontAwesomeIcon icon="map-marker-alt" />
    </div>
)

function Map(props) {
    let latitude = props.lat;
    let longitude = props.lng;
    const defaultData = {
        center: {
            lat: latitude,
            lng: longitude
        },
        zoom: 14
    }
    return (
            <GoogleMapReact
                bootstrapURLKeys={{key: 'AIzaSyAKLUx_rnltQ2u9Xr39DcpX3UdRr293gCU'}}
                center = {defaultData.center}
                defaultZoom = {defaultData.zoom}
            >
                <Any 
                    lat={latitude}
                    lng={longitude}
                    text={props.text}
                />
            </GoogleMapReact>
    )
}

export default Map
