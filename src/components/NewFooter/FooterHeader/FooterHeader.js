import React from 'react';
import './FooterHeader.css';
import ClassiLogo from '../../../assets/images/classi-logo.png';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

function FooterHeader() {
    return (
        <div className="new-footer-header-container">
            <div className="new-footer-header-logo-div">
                ClassiEstate
            </div>
            <div className="new-footer-header-nav-div">
                <div className="new-footer-header-nav-link new-footer-header-nav-link-help">Buy</div>
                <div className="new-footer-header-nav-link new-footer-header-nav-link-contact">Rent</div>
                <div className="new-footer-header-nav-link new-footer-header-nav-link-about">Find Agent</div>
                <div className="new-footer-header-nav-link new-footer-header-nav-link-career">Blog</div>
                <div className="new-footer-header-nav-link new-footer-header-nav-link-mobile">Mobile</div>
                <div className="new-footer-header-nav-link new-footer-header-nav-link-ad">Place an ad</div>
            </div>
            <div className="new-footer-domain-admin">
                <div className="new-footer-domain-admin-text new-footer-domain-for-agent">
                    Domain for Agent
                </div>
                <div className="new-footer-domain-admin-text new-footer-agent-admin">
                    <FontAwesomeIcon icon="lock" className="new-footer-agent-admin-icon" />
                    &nbsp;&nbsp; Agent Admin
                </div>
            </div>
            <div className="new-footer-classiestate-holding">
                <div> &copy; Classiestate Holdings Australia. </div>
            </div>
        </div>
    )
}

export default FooterHeader
