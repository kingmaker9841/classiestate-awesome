import React from 'react';
import './NewFooter.css';
import FooterHeader from './FooterHeader/FooterHeader';
import FooterMain from './FooterMain/FooterMain';
import FooterBottom from './FooterBottom/FooterBottom';

function NewFooter() {
    return (
        <div className="new-footer-container">
            <div className="footer-main-new-header">
                <FooterHeader />
            </div>

            <div className="footer-main-and-bottom">
                <FooterMain />
                <FooterBottom />
            </div>
        </div>
    )
}

export default NewFooter
