import React from 'react';
import './FooterMain.css';

function FooterMain() {
    return (
        <div className="new-footer-main-div">
            <div className="new-footer-all-divs new-footer-main-states-div">
                <span className="new-footer-main-title">States</span>
                <span>NSW</span>
                <span>Victoria</span>
                <span>Queensland</span>
                <span>Western Australia</span>
                <span>South Australia</span>
                <span>Tasmania</span>
                <span>ACT</span>
                <span>Norther Territory</span>
            </div>
            <div className="new-footer-all-divs new-footer-main-capital-cities-div">
                <span className="new-footer-main-title">Capital Cities</span>
                <span>Sydney real estate</span>
                <span>Melbourne real estate</span>
                <span>Brisbane real estate</span>
                <span>Perth real estate</span>
                <span>Adelaide real estate</span>
                <span>Hobart real estate</span>
                <span>Canberra real estate</span>
                <span>Darwin real estate</span>
            </div>
            <div className="new-footer-all-divs new-footer-main-capital-rental-div">
                <span className="new-footer-main-title">Capital Cities - Rentals</span>
                <span>Sydney rental properties</span>
                <span>Melbourne rental properties</span>
                <span>Brisbane rental properties</span>
                <span>Perth rental properties</span>
                <span>Adelaide rental properties</span>
                <span>Hobart rental properties</span>
                <span>Canberra rental properties</span>
                <span>Darwin rental properties</span>
            </div>
            <div className="new-footer-all-divs new-footer-main-popular-areas-div">
                <span className="new-footer-main-title">Popular Areas</span>
                <span>Eastern Suburbs Sydney</span>
                <span>Inner West Sydney</span>
                <span>Lower North Shore</span>
                <span>East Melbourne</span>
                <span>Bayside Melbourne</span>
                <span>Sydney City</span>
                <span>Inner City Melbourne</span>
                <span>Upper North Shore</span>
            </div>
            <div className="new-footer-all-divs new-footer-main-domain-div">
                <span className="new-footer-main-title">Domain</span>
                <span>Home Price Guide</span>
                <span>Auction Results</span>
                <span>Suburb Profiles</span>
                <span>Privacy Policy</span>
                <span>Financial Services Guide</span>
                <span>Conditions of use</span>
                <span>Tracking &amp; targeting policy</span>
                <span>Supplier Code of Conduct</span>
                <span>Oneflare</span>
                <span>UrbanYou</span>
                <span>Word of Mouth</span>
                <span>Domain Group API</span>
            </div>
            <div className="new-footer-all-divs new-footer-main-our-network-div">
                <span className="new-footer-main-title">Our Network</span>
                <span>Sydney Morning Herald</span>
                <span>The Age</span>
                <span>Brisbane Times</span>
                <span>WA Today</span>
                <span>Canberra Times</span>
                <span>Domain Digital Editions</span>
                <span>Drive</span>
                <span>Nine</span>
                <span>9Now</span>
            </div>
        </div>
    )
}

export default FooterMain
