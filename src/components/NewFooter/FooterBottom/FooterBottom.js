import React from 'react';
import './FooterBottom.css';

function FooterBottom() {
    return (
        <div className="new-footer-bottom-div">
            &copy; ClassiEstate Holdings Australia
        </div>
    )
}

export default FooterBottom
