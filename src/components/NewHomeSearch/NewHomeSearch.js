import React, {useState} from 'react';
import './NewHomeSearch.css';
import {useHistory} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import FilterIcon from '../../assets/images/icons/filter.png';
import HomeSearchFilter from './HomeSearchFilter/HomeSearchFilter';
import ReactDom from 'react-dom';
import axios from 'axios';

function NewHomeSearch({propClass, inactiveFilter}) {
    let [filterModal, setFilterModal] = useState(false);

    let [buyRentSell, setBuyRentSell] = useState('buy');
    let history = useHistory();
    const closeFilterModal = val =>{
        console.log(val);
        setFilterModal(!filterModal)
    }
    const onFilterClick = ()=>{
        setFilterModal(!filterModal);
    }
    const activeClassHandler = (searchClass, searchTriangle, val)=>{
        let searchTri = document.getElementsByClassName('new-home-search-triangle');
        let newHomeTab = document.getElementsByClassName('new-home-tab');
        for (let i = 0; i<newHomeTab.length; i++){
            newHomeTab[i].classList.remove('new-home-tab-active');
            searchTri[i].classList.remove('new-home-search-active-triangle');
        }
        let clickedClass = document.getElementsByClassName(searchClass)[0];
        let clickedTri = document.getElementsByClassName(searchTriangle)[0];
        if (clickedClass){
            clickedClass.className += ' new-home-tab-active';
        }
        if (clickedTri){
            clickedTri.className += ' new-home-search-active-triangle';
        }
        setBuyRentSell(val);
    }

    const onSubmitHandler = (e)=>{
        e.preventDefault();
        history.push(`/listing/${buyRentSell}/?&property_category=${buyRentSell}`)
    }
    const onKeyUpHandler = e=>{
        console.log(e.target.value);
        axios.post(`/api/v2/address?address=${e.target.value}`)
        .then(res=>{
            console.log(res.data);
        })
        .catch(err=>{
            console.log(err.response)
        })
    }
    return (
        <div className={propClass? propClass : 'new-home-search-container'}>
            <div className="new-home-search-container-text-p">
                Search Australia's Home of Property
            </div>
            {/* Search Tab Options */}
            <div className="new-home-tab-options">
                <div onClick={()=> activeClassHandler('new-home-tab-buy', 'new-home-search-buy-triangle', 'buy')} className="new-home-tab new-home-tab-buy new-home-tab-active">
                    Buy
                    <div className="new-home-search-triangle new-home-search-buy-triangle new-home-search-active-triangle"></div>
                </div>
                <div onClick={()=> activeClassHandler('new-home-tab-rent', 'new-home-search-rent-triangle', 'rent')} className="new-home-tab new-home-tab-rent home-search-rent">
                    Rent
                    <div className="new-home-search-triangle new-home-search-rent-triangle"></div>
                </div>
                <div onClick={()=> activeClassHandler('new-home-tab-findagent', 'new-home-search-agent-triangle', 'find_agent')} className="new-home-tab new-home-tab-findagent home-search-findagent"> 
                    Find Agent
                    <div className="new-home-search-triangle new-home-search-agent-triangle"></div>
                  </div>
            </div>
            {/* Search Text Box */}
            <form onSubmit={(e)=> onSubmitHandler(e)}  className="new-home-search-box-btn">
                <div className="new-home-search-box-div">
                    <input onKeyUp={e=> onKeyUpHandler(e)} type="text" className="new-home-search-input" placeholder="Enter Location" />
                    
                    {
                    inactiveFilter? null: (
                    <div className="new-home-search-filter-div" onClick={()=> onFilterClick()}>
                        <img src={FilterIcon} alt="filter.png" className="new-home-search-filter-icon" />
                        <span className="new-home-filter-text-span">Filters</span>
                    </div> 
                    )}
                </div>
                <div className="new-home-search-btn-div">
                    <button type="submit" className="new-home-search-btn">Search</button>
                    <div className="new-home-search-btn-icon">
                        <FontAwesomeIcon icon="search" className="new-home-search-icon" />
                    </div>
                </div>
            </form>
            {
             filterModal? (
                 ReactDom.createPortal(<HomeSearchFilter closeFilterModal={closeFilterModal} />,
                    document.getElementById('portal-root'))
             
             ): null   
            }
            
            
        </div>
    )
}

export default NewHomeSearch
