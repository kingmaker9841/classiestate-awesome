import React, {useEffect, useState} from 'react';
import './Bathrooms.css'
import CustomCheckBox from '../../../../library/CustomCheckBox/CustomCheckBox';

function Bathrooms({numberOfBathroom}) {
    let [exact, setExact] = useState(false);
    useEffect(()=>{
        let filterSearch = document.getElementsByClassName('home-new-search-filter-bathroom-all');
        for (let i=0; i<filterSearch.length; i++){
            filterSearch[i].addEventListener('click', ()=>{
                let cur = document.getElementsByClassName('home-new-search-filter-bathroom-active')[0];
                cur.className = cur.className.replace(' home-new-search-filter-bathroom-active', '');
                filterSearch[i].className += ' home-new-search-filter-bathroom-active';
            })
        }
        return ()=>{
            for(let i=0; i<filterSearch.length; i++){
                filterSearch[i].removeEventListener('click', ()=> {})
            }
        }
    }, [])
    const clicked = val=>{
        setExact(val);
    }
    const bathroomClick = val=>{
        numberOfBathroom(val);
    }
    return (
        <div className="home-new-search-filter-bathrooms-div">
            <strong>Bathrooms</strong>
            {
            exact? (
                <div className="home-new-search-filter-bathrooms-options">
                    <div onClick={()=> bathroomClick('0')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-any home-new-search-filter-bathroom-active">0</div>
                    <div onClick={()=> bathroomClick('1')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-1">1</div>
                    <div onClick={()=> bathroomClick('2')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-2">2</div>
                    <div onClick={()=> bathroomClick('3')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-3">3</div>
                    <div onClick={()=> bathroomClick('4')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-4">4</div>
                    <div onClick={()=> bathroomClick('5')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-5">5</div>
            </div>
            ): (
                <div className="home-new-search-filter-bathrooms-options">
                    <div onClick={()=> bathroomClick('any')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-any home-new-search-filter-bathroom-active">Any</div>
                    <div onClick={()=> bathroomClick('1+')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-1">1+</div>
                    <div onClick={()=> bathroomClick('2+')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-2">2+</div>
                    <div onClick={()=> bathroomClick('3+')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-3">3+</div>
                    <div onClick={()=> bathroomClick('4+')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-4">4+</div>
                    <div onClick={()=> bathroomClick('5+')} className="home-new-search-filter-bathroom-all home-new-search-filter-bathroom-5">5+</div>
            </div>
            )
            }

            <div className="home-new-search-filter-bathrooms-select">
                <CustomCheckBox labelName="Use exact values" clicked={clicked} />
            </div>
        </div>
    )
}

export default Bathrooms
