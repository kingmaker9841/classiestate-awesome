import React, {useEffect, useState} from 'react';
import './Parking.css';
import CustomCheckBox from '../../../../library/CustomCheckBox/CustomCheckBox';

function Parking({numberOfParking}) {
    let [exact, setExact] = useState(false);
    useEffect(()=>{
        let filterSearch = document.getElementsByClassName('home-new-search-filter-parking-all');
        for (let i=0; i<filterSearch.length; i++){
            filterSearch[i].addEventListener('click', ()=>{
                let cur = document.getElementsByClassName('home-new-search-filter-parking-active')[0];
                cur.className = cur.className.replace(' home-new-search-filter-parking-active', '');
                filterSearch[i].className += ' home-new-search-filter-parking-active';
            })
        }
        return ()=>{
            for(let i=0; i<filterSearch.length; i++){
                filterSearch[i].removeEventListener('click', ()=> {})
            }
        }
    }, [])
    const clicked = val=>{
        setExact(val);
    }
    const parkingClick = val =>{
        numberOfParking(val);
    }

    return (
        <div className="home-new-search-filter-parkings-div">
            <strong>Parking</strong>
            {
            exact? (
                <div className="home-new-search-filter-parkings-options">
                    <div onClick={()=> parkingClick('0')} className="home-new-search-filter-parking-all home-new-search-filter-parking-any home-new-search-filter-parking-active">0</div>
                    <div onClick={()=> parkingClick('1')} className="home-new-search-filter-parking-all home-new-search-filter-parking-1">1</div>
                    <div onClick={()=> parkingClick('2')} className="home-new-search-filter-parking-all home-new-search-filter-parking-2">2</div>
                    <div onClick={()=> parkingClick('3')} className="home-new-search-filter-parking-all home-new-search-filter-parking-3">3</div>
                    <div onClick={()=> parkingClick('4')} className="home-new-search-filter-parking-all home-new-search-filter-parking-4">4</div>
                    <div onClick={()=> parkingClick('5')} className="home-new-search-filter-parking-all home-new-search-filter-parking-5">5</div>
            </div>
            ): (
                <div className="home-new-search-filter-parkings-options">
                    <div onClick={()=> parkingClick('any')} className="home-new-search-filter-parking-all home-new-search-filter-parking-any home-new-search-filter-parking-active">Any</div>
                    <div onClick={()=> parkingClick('1+')} className="home-new-search-filter-parking-all home-new-search-filter-parking-1">1+</div>
                    <div onClick={()=> parkingClick('2+')} className="home-new-search-filter-parking-all home-new-search-filter-parking-2">2+</div>
                    <div onClick={()=> parkingClick('3+')} className="home-new-search-filter-parking-all home-new-search-filter-parking-3">3+</div>
                    <div onClick={()=> parkingClick('4+')} className="home-new-search-filter-parking-all home-new-search-filter-parking-4">4+</div>
                    <div onClick={()=> parkingClick('5+')} className="home-new-search-filter-parking-all home-new-search-filter-parking-5">5+</div>
            </div>
            )
            }

            <div className="home-new-search-filter-parkings-select">
                <CustomCheckBox labelName="Use exact values" clicked={clicked} />
            </div>
        </div>
    )
}

export default Parking
