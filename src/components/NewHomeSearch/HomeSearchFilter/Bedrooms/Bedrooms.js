import React, {useEffect, useState} from 'react';
import './Bedrooms.css'
import CustomCheckBox from '../../../../library/CustomCheckBox/CustomCheckBox';

function Bedrooms({numberOfBedrooms}) {
    let [exact, setExact] = useState(false);

    useEffect(()=>{
        let filterSearch = document.getElementsByClassName('home-new-search-filter-bedroom-all');
        for (let i=0; i<filterSearch.length; i++){
            filterSearch[i].addEventListener('click', ()=>{
                let cur = document.getElementsByClassName('home-new-search-filter-bedroom-active')[0];
                cur.className = cur.className.replace(' home-new-search-filter-bedroom-active', '');
                filterSearch[i].className += ' home-new-search-filter-bedroom-active';
            })
        }
        return ()=>{
            for(let i=0; i<filterSearch.length; i++){
                filterSearch[i].removeEventListener('click', ()=> {})
            }
        }
    }, [])
    const clicked = (val)=>{
        setExact(val);
    }
    const bedroomClick = val =>{
        numberOfBedrooms(val);
    }
    return (
        <div className="home-new-search-filter-bedrooms-div">
            <strong>Bedrooms</strong>
            {
            exact? (
                <div className="home-new-search-filter-bedrooms-options">
                    <div onClick={()=> bedroomClick('Studio')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-any home-new-search-filter-bedroom-active">Studio</div>
                    <div onClick={()=> bedroomClick('1')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-1">1</div>
                    <div onClick={()=> bedroomClick('2')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-2">2</div>
                    <div onClick={()=> bedroomClick('3')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-3">3</div>
                    <div onClick={()=> bedroomClick('4')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-4">4</div>
                    <div onClick={()=> bedroomClick('5')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-5">5</div>
            </div>
            ): (
                <div className="home-new-search-filter-bedrooms-options">
                    <div onClick={()=> bedroomClick('any')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-any home-new-search-filter-bedroom-active">Any</div>
                    <div onClick={()=> bedroomClick('1+')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-1">1+</div>
                    <div onClick={()=> bedroomClick('2+')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-2">2+</div>
                    <div onClick={()=> bedroomClick('3+')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-3">3+</div>
                    <div onClick={()=> bedroomClick('4+')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-4">4+</div>
                    <div onClick={()=> bedroomClick('5+')} className="home-new-search-filter-bedroom-all home-new-search-filter-bedroom-5">5+</div>
            </div>
            )
            }

            <div className="home-new-search-filter-bedrooms-select">
                <CustomCheckBox labelName="Use exact values" clicked={clicked} />
            </div>
        </div>
    )
}

export default Bedrooms
