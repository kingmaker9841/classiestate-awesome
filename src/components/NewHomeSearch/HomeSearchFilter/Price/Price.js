import React, {useState} from 'react';
import './Price.css';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

function Price({minPrice, maxPrice}) {
    let [priceRange, setPriceRange] = useState('Any');
    const sliderChangeHandler = (val)=>{
        setPriceRange(`$${val[0]} - $${val[1]}`);
        minPrice(val[0]); maxPrice(val[1]);
    }
    return (
        <div className="home-new-search-filter-price-div">
            <div className="home-new-search-filter-price-text"><strong>Price</strong></div>
            <div className="home-new-search-filter-price-range-text"> {priceRange} </div>
            <div className="home-new-search-filter-price-slider">
                <Range 
                    min ={0}
                    max={10000000}
                    defaultValue={[0, 10000000]}
                    minDistance={10000}
                    onChange={(val)=> sliderChangeHandler(val)}
                    className="new-search-filter-range"
                    allowCross={false}
                    count={10}
                    handleStyle={[{
                        fontSize: '3rem',
                        width: '2rem', 
                        height: '2rem',
                        border: '1px solid rgba(0,0,0,0.2)'
                    }]}
                    trackStyle={[{
                        height: '3px',
                        backgroundColor: 'var(--primary-color)',
                        marginTop: '0.5rem'
                        
                    }]}
                    railStyle={{marginTop: '0.5rem'}}
                    step={500000}
                    tipProps={{
                        placement: 'top',
                        prefixCls: 'rc-slider-tooltip'
                    }}
                />
            </div>
        </div>
    )
}

export default Price
