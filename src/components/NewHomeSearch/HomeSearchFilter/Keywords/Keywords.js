import React from 'react';
import './Keywords.css';

function Keywords({searchKeywords}) {
    const onChangeHandler = e=>{
        searchKeywords(e.target.value);
    }
    return (
        <div className="home-new-search-filter-keywords">
            <div className="home-new-search-filter-keywords"><strong>Keywords</strong></div>
            <input onChange={(e)=> onChangeHandler(e)} type="text" placeholder="e.g. waterfront, views, street name" className="home-new-search-filter-keywords-input" />
        </div>
    )
}

export default Keywords
