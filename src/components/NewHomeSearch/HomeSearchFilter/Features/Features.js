import React from 'react';
import './Features.css';

function Features({searchFeatures}) {
    const onChangeHandler = (e)=>{
        searchFeatures(e.target.checked, e.target.value);
    }
    return (
        <div className="home-new-search-features-container">
            <div className="home-new-search-features-text"><strong>Features</strong></div>
            <div className="home-new-search-features-div">
                <div className="home-new-search-features-row home-new-search-features-row1">
                    <label htmlFor="features-pet" className="home-new-search-features-row-group home-new-search-features-row1-group1">
                        Pets Allowed
                        <input type="checkbox" value="Pets Allowed" onChange={(e)=> onChangeHandler(e)} id="features-pet" className="home-new-search-features-checkbox" />
                        <span className="home-new-features-checkmark"></span>
                        {/* <label htmlFor="features-pet">&nbsp;&nbsp;Pets Allowed</label> */}
                    </label>
                    <label htmlFor="features-wardrobes" className="home-new-search-features-row-group home-new-search-features-row1-group2">
                        Built in Wardrobes
                        <input type="checkbox" value="Built in Wardrobes" onChange={(e)=> onChangeHandler(e)} id="features-wardrobes" className="home-new-search-features-checkbox" />
                        <span className="home-new-features-checkmark"></span>
                        {/* <label htmlFor="features-wardrobes">&nbsp;&nbsp;Built in Wardrobes</label> */}
                    </label>
                </div>
                <div className="home-new-search-features-row home-new-search-features-row2">
                    <label htmlFor="features-gas" className="home-new-search-features-row-group home-new-search-features-row2-group1">
                        Gas
                        <input type="checkbox" value="Gas" onChange={(e)=> onChangeHandler(e)} id="features-gas" className="home-new-search-features-checkbox" />
                        <span className="home-new-features-checkmark"></span>
                        {/* <label htmlFor="features-gas">&nbsp;&nbsp;Gas</label> */}
                    </label>
                    <label htmlFor="features-garden" className="home-new-search-features-row-group home-new-search-features-row2-group2">
                        Garden/courtyard
                        <input value="Garden/courtyard" onChange={(e)=> onChangeHandler(e)} type="checkbox" id="features-garden" className="home-new-search-features-checkbox" />
                        <span className="home-new-features-checkmark"></span>
                        {/* <label htmlFor="features-gardenn">&nbsp;&nbsp;Garden/courtyard</label> */}
                    </label>
                </div>
                <div className="home-new-search-features-row home-new-search-features-row3">
                    <label htmlFor="features-balcony" className="home-new-search-features-row-group home-new-search-features-row3-group1">
                        Balcony/deck
                        <input value="Balcony/deck" onChange={(e)=> onChangeHandler(e)} type="checkbox" id="features-balcony" className="home-new-search-features-checkbox" />
                        <span className="home-new-features-checkmark"></span>
                        {/* <label htmlFor="features-balcony">&nbsp;&nbsp;Balcony/deck</label> */}
                    </label>
                    <label htmlFor="features-internal-laundry" className="home-new-search-features-row-group home-new-search-features-row3-group2">
                        Internal-laundry
                        <input value="Internal-laundry" onChange={(e)=> onChangeHandler(e)} type="checkbox" id="features-internal-laundry" className="home-new-search-features-checkbox" />
                        <span className="home-new-features-checkmark"></span>
                        {/* <label htmlFor="features-internal-laundry">&nbsp;&nbsp;Internal-laundry</label> */}
                    </label>
                </div>
                <div className="home-new-search-features-row home-new-search-features-row4">
                    <label htmlFor="features-Study" className="home-new-search-features-row-group home-new-search-features-row4-group1">
                        Study
                        <input value="Study" onChange={(e)=> onChangeHandler(e)} type="checkbox" id="features-Study" className="home-new-search-features-checkbox" />
                        <span className="home-new-features-checkmark"></span>
                        {/* <label htmlFor="features-Study">&nbsp;&nbsp;Study</label> */}
                    </label>
                    <label className="home-new-search-features-row-group home-new-search-features-row4-group2">
                        Swimming pool
                        <input value="Swimming pool" onChange={(e)=> onChangeHandler(e)} type="checkbox" id="features-Swimming-pool" className="home-new-search-features-checkbox" />
                        <span className="home-new-features-checkmark"></span>
                        {/* <label htmlFor="features-Swimming-pool">&nbsp;&nbsp;Swimming pool</label> */}
                    </label>
                </div>
                <div htmlFor="features-air" className="home-new-search-features-row home-new-search-features-row5">
                    <label className="home-new-search-features-row-group home-new-search-features-row5-group1">
                        Air Conditioning
                        <input value="Air Conditioning" onChange={(e)=> onChangeHandler(e)} type="checkbox" id="features-air" className="home-new-search-features-checkbox" />
                        <span className="home-new-features-checkmark"></span>
                        {/* <label htmlFor="features-air">&nbsp;&nbsp;Air Conditioning</label> */}
                    </label>
                </div>
            </div>
    </div>
    )
}

export default Features
