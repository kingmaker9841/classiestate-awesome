import React, {useEffect, useState, useContext} from 'react';
import {useHistory} from 'react-router-dom';
import './HomeSearchFilter.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import PropertyTypes from './PropertyTypes/PropertyTypes';
import Price from './Price/Price';
import Bedrooms from './Bedrooms/Bedrooms';
import Bathrooms from './Bathrooms/Bathrooms';
import Parking from './Parking/Parking';
import LandSize from './LandSize/LandSize';
import Features from './Features/Features';
import NewEstablished from './NewEstablished/NewEstablished';
import CustomCheckBox from '../../../library/CustomCheckBox/CustomCheckBox';
import Keywords from './Keywords/Keywords';

function HomeSearchFilter({closeFilterModal}) {
    let [moreFilter, setMoreFilter] = useState(false);
    let [propType, setPropType] = useState('');
    let [priceMin, setPriceMin] = useState('');
    let [priceMax, setPriceMax] = useState('');
    let [numberBedroom, setNumberBedroom] = useState('');
    let [numberBathroom, setNumberBathroom] = useState('');
    let [numberParking, setNumberParking] = useState('');
    let [minLandSize, setMinLandSize] = useState('');
    let [maxLandSize, setMaxLandSize] = useState('');
    let [homeSearchFeatures, setHomeSearchFeatures] = useState([]);
    let [establishedText, setEstablishedText] = useState('');
    let [keywordsText , setKeywordsText] = useState('');
    let [buyRentSell, setBuyRentSell] = useState('buy');

    let history = useHistory();
    useEffect(()=>{ 
        let filterContainer = document.getElementsByClassName('home-search-filter-container')[0];
        document.body.style.overflow = 'hidden';
        filterContainer.addEventListener('scroll', (e)=>{
            let bottomSearch = document.getElementsByClassName('home-search-filter-bottom-fixed-search')[0]; 
            if (filterContainer.scrollTop > 170){
                bottomSearch.classList.add('home-search-filter-bottom-fixed-search-toggle');
            } 
            if (filterContainer.scrollTop < 170){
                bottomSearch.classList.remove('home-search-filter-bottom-fixed-search-toggle');
            }
        });
        return ()=>{
            filterContainer.removeEventListener('scroll', ()=> {})
            document.body.style.overflow = 'auto';
        }
    }, []);
    const clicked =()=> {}
    const closeHandler = ()=>{
        console.log("close clicked");
        closeFilterModal(true);
    }
    const activeClassHandler = (searchClass, searchTriangle, val)=>{
        let searchTri = document.getElementsByClassName('new-home-search-triangle-portal');
        let newHomeTab = document.getElementsByClassName('new-home-tab-portal');
        for (let i = 0; i<newHomeTab.length; i++){
            newHomeTab[i].classList.remove('new-home-tab-active-portal');
            searchTri[i].classList.remove('new-home-search-active-triangle-portal');
        }
        let clickedClass = document.getElementsByClassName(searchClass)[0];
        let clickedTri = document.getElementsByClassName(searchTriangle)[0];
        if (clickedClass){
            clickedClass.className += ' new-home-tab-active-portal';
        }
        if (clickedTri){
            clickedTri.className += ' new-home-search-active-triangle-portal';
        }
        setBuyRentSell(val);
    }

    // Getting Values for api
    const propertyTypes = (val)=>{
        if (val === 'all'){
            propType = 'all';
        }else{
            if (propType.includes('all')){
                console.log("true");
                propType = propType.replace('all', '');
            }
            if (propType.includes(val)){
                propType = propType.replace(val, '');
                return;
            }
            propType += " " + val;
        }
        console.log(propType);
    }
    const minPrice = (val)=>{
        setPriceMin(val);
    }
    const maxPrice = val =>{
        setPriceMax(val);
    }
    const numberOfBedrooms = val =>{

        setNumberBedroom(val);
    }
    const numberOfBathroom = val =>{
        setNumberBathroom(val);
    }
    const numberOfParking= val =>{
        setNumberParking(val);
    }
    const minLand = val =>{
        setMinLandSize(val);
    }
    const maxLand = val =>{
        setMaxLandSize(val);
    }
    const searchFeatures = (check, val)=>{
        if (check){
            homeSearchFeatures.push(val);
        }else {
            homeSearchFeatures.splice(homeSearchFeatures.indexOf(val), 1);
        }
    }

    const searchEstablished = val=>{
        setEstablishedText(val);
        console.log(val);
    }
    const searchKeywords = val =>{
        console.log(val);
        setKeywordsText(val);
    }
    const onSubmitHandler = e=>{
        e.preventDefault();
        let price = '';
        let area = '';
        if (priceMin && !priceMax){
            price = priceMin + '__';
        }
        if (priceMax && !priceMin){
            price = '__' + priceMax;
        }
        if (priceMin && priceMax){
            price = priceMin + "__" + priceMax;
        }
        if (minLandSize && !maxLandSize){
            area = minLandSize + '__'
        }
        if (maxLandSize && !minLandSize){
            area = '__' + maxLandSize;
        }
        if (minLandSize && maxLandSize){
            area = minLandSize + '__' + maxLandSize;
        }
        history.push(`/listing/${buyRentSell}/?property_category=${buyRentSell}${propType &&`&property_type=${propType}`}${price && `&price=${price}`}${area && `&area=${area}`}${numberBedroom && `&no_of_room=${numberBedroom}`}${numberBathroom && `&no_of_bathroom=${numberBathroom}`}${numberParking && `&no_of_car_space=${numberParking}`}`);
    
    }
    return (
        <div className="home-search-filter-container">
            <section>
                <div className="home-search-filter-close" onClick={()=> closeHandler()} >
                    <div className="home-search-filter-close-text">Close</div>
                    <div className="home-search-filter-close-icon">&times;</div>
                </div>
                <div className="home-search-filter-search-box">
                    <div className='home-search-filter-search-container'>
                        {/* Search Tab Options */}
                        <div className="new-home-tab-options-portal">
                            <div onClick={()=> activeClassHandler('new-home-tab-buy-portal', 'new-home-search-buy-triangle-portal', 'buy')} className="new-home-tab-portal new-home-tab-buy-portal new-home-tab-active-portal">
                                Buy
                                <div className="new-home-search-triangle-portal new-home-search-buy-triangle-portal new-home-search-active-triangle-portal"></div>
                            </div>
                            <div onClick={()=> activeClassHandler('new-home-tab-rent-portal', 'new-home-search-rent-triangle-portal', 'rent')} className="new-home-tab-portal new-home-tab-rent-portal">
                                Rent
                                <div className="new-home-search-triangle-portal new-home-search-rent-triangle-portal"></div>
                            </div>
                            <div onClick={()=> activeClassHandler('new-home-tab-findagent-portal', 'new-home-search-agent-triangle-portal', 'find_agent')} className="new-home-tab-portal new-home-tab-findagent-portal"> 
                                Find Agent
                                <div className="new-home-search-triangle-portal new-home-search-agent-triangle-portal"></div>
                            </div>
                        </div>
                        {/* Search Text Box */}
                        <form onSubmit={(e)=> onSubmitHandler(e)} className="new-home-search-box-btn">
                            <div className="new-home-search-box-div">
                                <input type="text" className="new-home-search-input" placeholder="Enter Location" />
                            </div>
                            <div className="new-home-search-btn-div">
                                <button type="submit" className="new-home-search-btn">Search</button>
                                <div className="new-home-search-btn-icon">
                                    <FontAwesomeIcon icon="search" className="new-home-search-icon" />
                                </div>
                            </div>
                        </form>                       
                    </div>
                </div>
                <div className="home-search-filter-suburb">
                   <CustomCheckBox labelName='Search by Suburb' clicked={clicked} checked={false}/>
                </div>
            </section>
            <main className="home-search-filter-main">
                <div className="home-search-filter-property-types-clear" >
                    <div><strong>Property Types</strong></div>
                    <div style={{color: 'var(--primary-color)'}}>Clear All</div>
                </div>
                <PropertyTypes propertyTypes={propertyTypes} />
                <div className="home-search-filter-main-divider"></div>
                <Price minPrice={minPrice} maxPrice={maxPrice} />
                <div className="home-search-filter-main-divider"></div>
                <Bedrooms numberOfBedrooms={numberOfBedrooms} />
                <div className="home-search-filter-main-divider"></div>
                <Bathrooms numberOfBathroom={numberOfBathroom} />
                <div className="home-search-filter-main-divider"></div>
                <Parking numberOfParking={numberOfParking} />
                <div className="home-search-filter-main-divider"></div>

                {
                !moreFilter? (
                <div className="home-search-filter-more-filters" onClick={(e)=> setMoreFilter(!moreFilter)}>
                    <span className="home-search-filter-more-text">More filters &nbsp;&nbsp;</span>
                    <FontAwesomeIcon icon="chevron-down" className="home-search-filter-more-chevron" />
                </div>
                    ): null
                }
                
                {
                    moreFilter && (
                <>
                <LandSize minLand={minLand} maxLand={maxLand} />
                <Features searchFeatures={searchFeatures} />
                <div className="home-search-filter-main-divider"></div>
                <NewEstablished searchEstablished={searchEstablished} />
                <div className="home-search-filter-main-divider"></div>
                <Keywords searchKeywords={searchKeywords} />
                <div className="home-search-filter-main-divider"></div>
                <div className="home-search-filter-exclude-offer">
                   <CustomCheckBox labelName='Exclude under offer' clicked={clicked} checked={true} />
                </div>
                <div className="home-search-filter-more-filters" onClick={(e)=> setMoreFilter(!moreFilter)}>
                    <span className="home-search-filter-more-text">Less filters &nbsp;&nbsp;</span>
                    <FontAwesomeIcon icon="chevron-up" className="home-search-filter-more-chevron" />
                </div>
                </>
                )}
            </main>
            <div className="home-search-filter-back-background"></div>
            <div className="home-search-filter-bottom-fixed-search">
                <div 
                className="home-search-filter-bottom-search-button"
                onClick={(e)=> onSubmitHandler(e)}
                >
                    <FontAwesomeIcon icon="search" className="home-search-filter-bottom-search-icon" />
                    <span className="home-search-filter-bottom-search-text">Search</span>
                </div>
            </div>
        </div>
    )
}

export default HomeSearchFilter
