import React, {useState} from 'react';
import './PropertyTypes.css';

function PropertyTypes({propertyTypes}) {
    let [propTypes, setPropTypes] = useState('');
    const proptypesClickHandler = e =>{
        let exceptAll = document.getElementsByClassName('home-search-property-types-except-all');
        for (let i =0; i<exceptAll.length; i++){
            if (exceptAll[i].classList.contains('home-search-property-types-active')){
                exceptAll[i].className = exceptAll[i].className.replace(' home-search-property-types-active', ''); 
            }
        }
        e.target.className += ' home-search-property-types-active';
        propertyTypes('all');
    }
    const otherClickHandler = e=>{
        let all = document.getElementsByClassName('home-search-property-types-all')[0];
        if (all.classList.contains('home-search-property-types-active')){
            all.className = all.className.replace(' home-search-property-types-active', '');
        }
        e.target.classList.toggle('home-search-property-types-active');        
        propertyTypes(e.target.innerText);
    }
    return (
        <div className="home-search-property-types-div">
            <div onClick={e=> proptypesClickHandler(e)} className="home-search-property-types-single home-search-property-types-all home-search-property-types-active">All</div>
            <div onClick={e=> otherClickHandler(e)} className="home-search-property-types-single home-search-property-types-except-all home-search-property-types-house">House</div>
            <div onClick={e=> otherClickHandler(e)} className="home-search-property-types-single home-search-property-types-except-all home-search-property-types-apartment">Apartment</div>
            <div onClick={e=> otherClickHandler(e)} className="home-search-property-types-single home-search-property-types-except-all home-search-property-types-townhouse">TownHouse</div>
            <div onClick={e=> otherClickHandler(e)} className="home-search-property-types-single home-search-property-types-except-all home-search-property-types-land">Land</div>
            <div onClick={e=> otherClickHandler(e)} className="home-search-property-types-single home-search-property-types-except-all home-search-property-types-rural">Rural</div>
            <div onClick={e=> otherClickHandler(e)} className="home-search-property-types-single home-search-property-types-except-all home-search-property-types-new-apartment">New Apartment</div>
            <div onClick={e=> otherClickHandler(e)} className="home-search-property-types-single home-search-property-types-except-all home-search-property-types-new-house-land">New House and Land</div>
            <div onClick={e=> otherClickHandler(e)} className="home-search-property-types-single home-search-property-types-except-all home-search-property-types-commercial">Commercial</div>
            <div onClick={e=> otherClickHandler(e)} className="home-search-property-types-single home-search-property-types-except-all home-search-property-types-garage-storage">Garage & Storage</div>
            <div onClick={e=> otherClickHandler(e)} className="home-search-property-types-single home-search-property-types-except-all home-search-property-types-other">Other</div>
        </div>
    )
}

export default PropertyTypes
