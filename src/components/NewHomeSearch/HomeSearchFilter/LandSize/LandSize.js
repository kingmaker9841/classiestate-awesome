import React, {useState} from 'react';
import './LandSize.css';

function LandSize({minLand, maxLand}) {
    let [minLandSize , setMinLandSize] = useState('');
    let [maxLandSize , setMaxLandSize] = useState('');
    const changeMin = (e)=>{
        setMinLandSize(e.target.value);
        minLand(e.target.value);
    }
    const changeMax = (e)=>{
        setMaxLandSize(e.target.value);
        maxLand(e.target.value);
    }
    return (
        <div className="home-new-search-land-size-container">
            <div className="home-new-search-land-size-text"><strong>Land Size</strong></div>
            <div className="home-new-search-land-size-div">
                <div className="home-new-search-land-size-min">
                    <div className="home-new-search-land-size-meter text-muted">m<sup>2</sup></div>
                    <input onChange={(e)=> changeMin(e)} type="text" className="home-new-search-land-size-input home-new-search-land-size-min-input" placeholder="e.g. 100" />
                </div>
                <span className="home-new-search-land-size-span">-</span>
                <div className="home-new-search-land-size-max">
                    <div className="home-new-search-land-size-meter text-muted">m<sup>2</sup></div>
                    <input onChange={(e)=> changeMax(e)} type="text" className="home-new-search-land-size-input home-new-search-land-size-max-input" placeholder="e.g. 500" />
                </div>
            </div>
        </div>
    )
}

export default LandSize
