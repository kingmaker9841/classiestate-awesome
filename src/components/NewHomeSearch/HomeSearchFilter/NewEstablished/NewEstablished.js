import React, {useState, useEffect} from 'react';
import './NewEstablished.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

function NewEstablished({searchEstablished}) {
    let [established, setEstablished] = useState('Any');
    useEffect(()=>{

        document.body.addEventListener('click', (e)=>{
            let dropdown = document.getElementsByClassName('home-custom-select-dropdown')[0];
            let chevron = document.getElementsByClassName('home-custom-select-chevron-icon')[0];
            if (dropdown && dropdown.classList.contains('home-custom-select-dropdown-toggle')){
                dropdown.className = dropdown.className.replace(' home-custom-select-dropdown-toggle', '');
            }
        })
        return ()=>{
            document.body.removeEventListener('click', ()=> {})
        }
    }, [])
    const onClickHandler = ()=>{
        let dropdown = document.getElementsByClassName('home-custom-select-dropdown')[0];
        let selectText = document.getElementsByClassName('home-custom-select-text')[0];
        selectText.style.backgroundColor = 'rgba(0,0,0,0.1)';
        dropdown.classList.toggle('home-custom-select-dropdown-toggle');
    }
    const onEstablishClickHandler = (e)=>{
        let dropdown = document.getElementsByClassName('home-custom-select-dropdown-toggle')[0];
        setEstablished(e.target.innerText);
        searchEstablished(e.target.innerText);
        dropdown && dropdown.classList.toggle('home-custom-select-dropdown');

    }
    return (
        <div className="home-new-search-filter-established-container">
            <div className="home-new-search-filter-established-text"><strong>New/Established</strong></div>
            <div className="home-new-search-filter-established-select">
                <div className="home-custom-select-text" onClick={()=> onClickHandler()}>
                    {established}
                    <FontAwesomeIcon icon="chevron-right" className="home-custom-select-chevron-icon" />
                </div>
                <div className="home-custom-select-dropdown">
                    <div aria-valuetext="Any" className="home-custom-select-dropdown-elem home-custom-select-dropdown-elem-any" onClick={(e)=> onEstablishClickHandler(e)} >Any</div>
                    <div aria-valuetext="New Construction" className="home-custom-select-dropdown-elem home-custom-select-dropdown-elem-new" onClick={(e)=> onEstablishClickHandler(e)} >New Constructions</div>
                    <div aria-valuetext="Established Properties" className="home-custom-select-dropdown-elem home-custom-select-dropdown-elem-established" onClick={(e)=> onEstablishClickHandler(e)} >Established Properties</div>
                </div>
            </div>
        </div>
    )
}

export default NewEstablished
