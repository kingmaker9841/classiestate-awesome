import React, {useState, useEffect, useReducer} from 'react';
import Navigation from '../Navigation/Navigation';
import './Listing.css';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import SyncLoader from 'react-spinners/SyncLoader';
import Footer from '../Footer/Footer';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import ReactPaginate from 'react-paginate';
import queryString from 'query-string';
import Map from '../Map/Map';
// import dayjs from 'dayjs';
import moment from 'moment';
import Manual1 from '../../assets/images/manual1.jpg'
import Manual2 from '../../assets/images/manual2.jpg'
import Manual3 from '../../assets/images/manual3.jpg'
import Manual4 from '../../assets/images/manual4.jpg'


const initialState = {
    filterTitle: '',
    filterType: '',
    filterMinPrice: '',
    filterMaxPrice: '',
    filterOfferType: '',
    filterStartDate: new Date(),
    filterEndDate: new Date(),
    filterMinArea: '',
    filterMaxArea: '',
    filterRooms: '',
    filterBathrooms: '',
    filterCarSpace: '',
    filterParkingType: '',
    filterInspectionStartDate: new Date(),
    filterInspectionEndDate: new Date(),
    filterPropertyType: '',
    filterPriceType: ''
}

const reducer = (state, action)=>{
    switch(action.type){
        case 'filterTitleChange' : return {...state, filterTitle: action.payload}
        case 'filterTypeChange' : return {...state, filterType: action.payload}
        case 'filterMinPriceChange' : return {...state, filterMinPrice: action.payload}
        case 'filterMaxPriceChange' : return  {...state, filterMaxPrice: action.payload}
        case 'filterOfferTypeChange' : return {...state, filterOfferType: action.payload}
        case 'filterStartDateChange' :  return {...state, filterStartDate: action.payload}
        case 'filterEndDateChange' : return {...state, filterEndDate: action.payload}
        case 'filterMinAreaChange' : return {...state, filterMinArea: action.payload}
        case 'filterMaxAreaChange' : return {...state, filterMaxArea: action.payload}
        case 'filterRoomsChange' : return {...state, filterRooms: action.payload}
        case 'filterBathroomsChange' : return {...state, filterBathrooms: action.payload}
        case 'filterCarSpaceChange' : return {...state, filterCarSpace: action.payload}
        case 'filterParkingTypeChange' : return {...state, filterParkingType: action.payload}
        case 'filterInspectionStartDateChange' : return {...state, filterInspectionStartDate: action.payload}
        case 'filterInspectionEndDateChange' : return {...state, filterInspectionEndDate: action.payload}
        case 'filterPropertyTypeChange' : return {...state, filterPropertyType: action.payload}
        case 'filterPriceTypeChange' : return {...state, filterPriceType: action.payload}
        default : return {...state}
    }
}

function Listing(props) {

    //For Searching Filter
    const [state, dispatch] = useReducer(reducer, initialState);

    let [posts, setPosts] = useState([]);
    let [loading, setLoading] = useState(false);
    let [queryParam, setQueryParam] = useState('buy');

    // For Showing Map

    let [showMap, setShowMap] = useState(false);

    //Url
    let [search, setSearch] = useState('?');

    //For Map Latitude and Longitude
    let [mapLat, setMapLat] = useState(-37.840935);
    let [mapLng, setMapLng] = useState(144.946457);

    //For Displaying Search Results 
    let [searchResults, setSearchResults] = useState('');
    let [searchTags, setSearchTags] = useState([]);

    //Saved Favorite Ads in Local Storage
    let [isLoggedIn, setIsLoggedIn] = useState(false);
    let [savedFav, setSavedFav] = useState('');

    const scrollHandler = (e)=>{
        let listingPropertyMap = document.getElementsByClassName('listing-property-map')[0];
        if (window.pageYOffset > 0) {
            listingPropertyMap && listingPropertyMap.classList.add('listing-property-map-sticky')
        }else {
            listingPropertyMap && listingPropertyMap.classList.remove('listing-property-map-sticky');
        }
    }
    //For Listing View Active
    let listingView = document.getElementsByClassName('listing-list-views');
    for (let i =0; i<listingView.length; i++){
        listingView[i].addEventListener('click', ()=>{
            let cur = document.getElementsByClassName('listing-list-view-active')[0];
            cur.className = cur.className.replace(' listing-list-view-active', '');
            listingView[i].className += ' listing-list-view-active';
        })
    }

    useEffect(()=>{

        let isActive = true;
        let listingGrid = document.getElementsByClassName('listing-grid')[0];
        let listingGridMap = document.getElementsByClassName('listing-gridmap')[0];
        // let searchTags = document.getElementsByClassName('search-tags-box')[0];
        

        //For sticky Property Map Nav
        window.addEventListener('scroll', scrollHandler);

        if (showMap) {
            listingGrid &&
            listingGrid.classList.add('listing-grid-toggle');
            listingGridMap &&
            listingGridMap.classList.add('listing-gridmap-toggle');
            // searchTags.classList.add('listing-search-tags-toggle');
        }else {
            listingGrid &&
            listingGrid.classList.remove('listing-grid-toggle');
            listingGridMap &&
            listingGridMap.classList.remove('listing-gridmap-toggle');
            // searchTags.classList.remove('listing-search-tags-toggle')
        }

        //Query String Parse
        let params = queryString.parse(props.location.search);
        setQueryParam(params.property_category ? params.property_category : 'buy');

        let homeSearchUrl = '/api/v2/ads_list?slug=real-estate&api_from=classiEstate';

        //For Price from Home Search
        if (params.price_min && !params.price_max) homeSearchUrl += `&price=${params.price_min}__`
        if (params.price_max && !params.price_min) homeSearchUrl += `&price=__${params.price_max}`
        if (params.price_min && params.price_max) homeSearchUrl += `&price=${params.price_min}__${params.price_max}`

        //For Area from Home Search
        if (params.area_min && !params.area_max) homeSearchUrl +=  `&area=${params.area_min}__`
        if (params.area_max && !params.area_min) homeSearchUrl +=  `&area=__${params.area_max}`
        if (params.area_min && params.area_max) homeSearchUrl += `&area=${params.area_min}__${params.area_max}`

        //For Property Category
        if (params.property_category) homeSearchUrl += `&property_category=${params.property_category}`;

        setLoading(true)
    
        // axios.get(`/api/v2/ads_list?slug=real-estate&api_from=classiEstate`);
        console.log(homeSearchUrl);
        axios.get(homeSearchUrl)
        .then(response=>{
            if (isActive){
                console.log(response);
                console.log(response.data);
                setSearchResults(response.data.posts.length);
                setSearchTags(response.data.search_tags);
                setPosts(response.data.posts);
                setLoading(false);
            }

        })
        .catch(err=>{
            if (isActive){
                console.log(err);
                console.log(err.response);
                setLoading(false);
            }

        })

        if (localStorage.getItem('token')){
            setIsLoggedIn(true);
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            setSavedFav(cF);
        }
        return ()=>{
            window.removeEventListener('scroll', scrollHandler);
            isActive = false;
        }
    }, [showMap, queryParam])

    const favoriteClickHandler = (e, id)=>{
        if (isLoggedIn){
            if (savedFav){
                if (savedFav.includes(id)){
                    e.target.classList.toggle('homead-icon-saveshare');
                    //Now Update savedFav
                    setSavedFav(prev=> prev.replace(id, ''));
                }else {
                    e.target.classList.toggle('homead-icon-saveshare-active');
                }
            }else {
                e.target.classList.toggle('homead-icon-saveshare-active');
            }
            
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            if (cF){
                if (String(cF).includes(id)){
                    cF = String(cF).replace(id, '')
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }else {
                    cF = String(cF) + ',' + id;
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }  
            }else {
                localStorage.setItem('classi_favorite', JSON.stringify(id));
            }
            
        }
    }


    const filterSubmitHandler = e =>{
        setLoading(true);

        let url = "/api/v2/ads_list?slug=real-estate&api_from=classiEstate";

        //Filter Type = 'buy/rent'
        if (state.filterType) url += `&property_category=${state.filterType}`
        //Filter Title not available

        //For Price
        if (state.filterMinPrice && !state.filterMaxPrice) url += `&price=${state.filterMinPrice}__`
        if (state.filterMaxPrice && !state.filterMinPrice) url += `&price=__${state.filterMaxPrice}`
        if (state.filterMinPrice && state.filterMaxPrice) url += `&price=${state.filterMinPrice}__${state.filterMaxPrice}`
        //For Offer Type
        if (state.filterOfferType)  url+= `&type=${state.filterOfferType}`


        //For Start and End Date
        console.log("Starting Date",moment(state.filterStartDate).format('YYYY-MM-DD'));

        let momentStartDate = moment(state.filterStartDate).format('YYYY-MM-DD');
        let momentEndDate = moment(state.filterEndDate).format('YYYY-MM-DD');
        if ((state.filterStartDate !== initialState.filterStartDate) && (state.filterEndDate != initialState.filterEndDate)){
            url += `&date=${momentStartDate}__${momentEndDate}`
        }
        if ((state.filterStartDate != initialState.filterStartDate) && (state.filterEndDate === initialState.filterEndDate)) url += `&date=${momentStartDate}__`
        if ((state.filterEndDate != initialState.filterEndDate) && (state.filterStartDate === initialState.filterStartDate)) url += `&date=__${momentEndDate}`

        //For Area
        if (state.filterMinArea && !state.filterMaxArea) url +=  `&area=${state.filterMinArea}__`
        if (state.filterMaxArea && !state.filterMinArea) url +=  `&area=__${state.filterMaxArea}`
        if (state.filterMinArea && state.filterMaxArea) url += `&area=${state.filterMinArea}__${state.filterMaxArea}`
        
        //For Rooms, Bathrooms, car space
        if (state.filterRooms) url += `&no_of_rooms=${state.filterRooms}`
        if (state.filterBathrooms) url += `&no_of_bathroom=${state.filterBathrooms}`
        if (state.filterCarSpace) url += `&no_of_carspace=${state.filterCarSpace}`

        //For Inspection Dates 
        let inspectionStartDate = moment(state.filterInspectionStartDate).format('YYYY-MM-DD');
        let inspectionEndDate = moment(state.filterInspectionEndDate).format('YYYY-MM-DD');
        if (state.filterInspectionStartDate !== initialState.filterInspectionStartDate) url += `&inspection_start_date=${inspectionStartDate}`
        if (state.filterInspectionEndDate !== initialState.filterInspectionEndDate) url += `&inspection_end_date=${inspectionEndDate}`

        //For Parking Type
        if (state.filterParkingType) url += `&parking_type=${state.filterParkingType}`

        //For Property Type not given

        //For Price Type
        if (state.filterPriceType) url += `&price_type=${state.filterPriceType}`
        console.log(url);
        axios.get(url)
        .then(res=>{
            console.log(res);
            console.log(res.data);
            if (res.status === 200){
                setPosts(res.data.posts);
                setSearchResults(res.data.posts.length);
                setSearchTags(res.data.search_tags)
            }
            setLoading(false);
            document.getElementById('filter-form-id').reset();
        })
        .catch(err=>{
            console.log(err);
            console.log(err.response);
            setLoading(false);
            document.getElementById('filter-form-id').reset();
        })
        
    }

    const listingImageHandler = (e,lat,lng)=>{
        setMapLat(lat); setMapLng(lng);
    }
    const handlePageClick = ()=>{

    }
    const mapViewHandler = e=>{
        let listinglist = document.getElementsByClassName('listing-list-map-inspection')[0];
        listinglist.classList.toggle('listing-list-map-inspection-toggle');
        setShowMap(!showMap);
    }

    return ( 
        <>
        <Navigation fromHome={false} line={true} scroll={true} search={true}  />
         {/* Main Container */}
        <div className="listing-container">
            <div className="listing-home-adlist">
                <span className="text-success">Home&nbsp;&nbsp;&gt;&nbsp;&nbsp;</span><span className="text-muted">Ad List</span>
            </div>
            {/* SyncLoader */}
            {
                loading && (
                    <div className="homead-syncloader d-flex justify-content-center mt-2">
                        <SyncLoader size={10} color={'var(--primary-color)'} loading={loading} />
                    </div>
                )
            }
            

            {/* <div className="search-tags-box mt-3 mb-1 ml-3">
            {
                searchTags.length > 0 ? (
                        searchTags.map((item,index)=>(
                            <span key={index} className="listing-search-tags-box border border-secondary mr-2 px-1 rounded text-secondary bg-light"><small> {item.parameter} &nbsp;:&nbsp; {item.label} </small></span>
                        ))
                    
                ): null
            }
            </div> */}
            
            {
                parseInt(searchResults) > 0 ? (
                    <div className="listing-search-results mt-4 mb-4">
                        <h3>Found {searchResults + " "+ "Results" } </h3>
                    </div>
                ) : null
            }
            <div className="listing-list-map-inspection">
                <div className="listing-list-views listing-list-view listing-list-view-active">
                    List
                </div>
                <div className="listing-list-views listing-map-view" onClick={e=> mapViewHandler(e)}>
                    Map View
                </div>
                <div className="listing-list-views listing-inpsection-view">
                    Inspection
                </div>
                <div className="listing-list-views listing-sort-view">
                    Sort
                </div>
                <div className="listing-list-views listing-relevant-view listing-list-view-active">
                    Most Relevant
                </div>
            </div>  

            {/* New Grid Design */}
            <div className="listing-new-grid">
                {
                    posts &&
                    posts.map((item,index)=>(
                        <>
                        <div key={index} className="listing-new-grid-wrapper">
                            <div className="listing-new-grid-image-wrapper">
                                <div className="homead-img-hover">
                                    <FontAwesomeIcon icon="heart" data-toggle={isLoggedIn ? null : 'modal'} data-target="#favorite-modal" onClick={(e)=> favoriteClickHandler(e, item.post_id)} 
                                    className={
                                            savedFav 
                                            ? (savedFav.includes("1020") 
                                            ? "homead-icon-saveshare-active"
                                            : "homead-icon-saveshare") 
                                            : "homead-icon-saveshare"
                                        }  /> 
                                </div>
                                <img src={item.default_image} alt={`${index}.jpg`} className="listing-new-grid-image" />
                                <div className="listing-new-grid-company-image-abs"
                                    style={{backgroundColor: `${item.company.company_color}`}} >
                                    <div className="listing-company-image-name">
                                        <div className="listing-new-grid-company-thumb">
                                            <img src={item.company.company_logo_thumb} alt={`${index}.jpg`} className="lising-new-grid-company-thumb-image" />
                                        </div>
                                        <div className="listing-new-grid-company-name">
                                            <p><strong> {item.company.first_name}</strong> </p>
                                            <p> {item.company.company_name} </p>
                                        </div>
                                    </div>
                                    <div className="listing-new-grid-company-main-name">
                                        <h3> 
                                             
                                            {
                                                item.company.company_name.length > 19 
                                                ? (
                                                    `${item.company.company_name.slice(0,19)}...`
                                                )
                                                : `${item.company.company_name}`
                                            }
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div className="listing-new-grid-other-contents">
                                <div className="listing-new-grid-other-title">
                                    <h2><em>{item.title}</em>  </h2>
                                </div>
                                <div className="listing-new-grid-other-address">
                                    <h4><em>{item.address}</em>  </h4>
                                </div>
                                <div className="listing-new-grid-other-price">
                                    <h4> ${item.price} - ${item.to_price} </h4>
                                </div>
                                <div className="listing-new-grid-other-icons">
                                    <div className="listing-new-grid-other-icons-bed listing-new-grid-other-icons-all">
                                        <FontAwesomeIcon icon="bed" className="listing-new-grid-icon" />
                                        <span className="listing-new-grid-icons-inspection-numbers"> {item.no_room} </span>
                                    </div>
                                    <div className="listing-new-grid-other-icons-bath listing-new-grid-other-icons-all">
                                        <FontAwesomeIcon icon="bath" className="listing-new-grid-icon" />
                                        <span className="listing-new-grid-icons-inspection-numbers"> {item.no_of_bathroom} </span>
                                    </div>
                                    <div className="listing-new-grid-other-icons-park listing-new-grid-other-icons-all">
                                        <FontAwesomeIcon icon="parking" className="listing-new-grid-icon" />
                                        <span className="listing-new-grid-icons-inspection-numbers"> {item.no_of_car_space} </span>
                                    </div>
                                    <div className="listing-new-grid-other-icons-area listing-new-grid-other-icons-all">
                                        <FontAwesomeIcon icon="vector-square" className="listing-new-grid-icon" />
                                        <span className="listing-new-grid-icons-inspection-numbers"> {item.area} sq.ft</span>
                                    </div>
                                </div>
                                <div className="listing-new-grid-other-inspection">
                                    <h4>Inspection</h4>
                                    {
                                        item.inspection_times.map((ins,index)=>(
                                            <div key={index} className="listing-other-inspection-individual">
                                                <span>
                                                    <FontAwesomeIcon icon="calendar" className="listing-other-inspection-icon" />
                                                </span>
                                                <span>&nbsp; {ins.inspection_date} </span>
                                                <span>&nbsp;&nbsp;<FontAwesomeIcon icon="clock" className="listing-other-inspection-icon" /></span>
                                                <span>&nbsp; {`${ins.start_hr}:${ins.start_min} ${ins.start_time}`} </span>
                                            </div>
                                        ))
                                    }
                                </div>
                                <div className="listing-new-grid-company-contact">
                                    <div className="listing-new-grid-company-contact-image-name">
                                        <div className="listing-new-grid-company-logo-wrapper">
                                            <img src={item.company.company_logo} className="listing-new-grid-company-logo" />
                                        </div>
                                        <div className="listing-new-grid-company-contact-name">
                                            <p><strong> {item.company.company_name} </strong></p>
                                        </div>
                                    </div>
                                    <div className="listing-new-grid-company-contact-icon"
                                        style={{backgroundColor: `${item.company.company_color}`, color: 'whitesmoke'}}>
                                        Contact us
                                    </div>
                                </div>
                            </div>

                            {/* 100% Grid container width */}
                            <div className="listing-grid">
                                <div className="listing-gridmap">
                                <Map lat={mapLat} lng={mapLng} />
                                </div>

                            </div>

                        </div>
                        </>
                    ))
                }
            </div> {/* New Grid Design CLoses ******************** */}

            {/* New Grid Map View Design */}
            {/* New Grid Map View Design CLose */}
        

        </div> {/* Listing Container Ends Here */}


        <div className="mt-5 "></div>
        <ReactPaginate 
            breakClassName={'page-item'}
            breakLinkClassName={'page-link'}
            containerClassName={'pagination listing-pagination'}
            pageClassName={'page-item'}
            pageLinkClassName={'page-link'}
            previousClassName={'page-item'}
            previousLinkClassName={'page-link'}
            nextClassName={'page-item'}
            nextLinkClassName={'page-link'}
            activeClassName={'active'}
            previousLabel={<FontAwesomeIcon icon="chevron-left" />}
            nextLabel={<FontAwesomeIcon icon="chevron-right" />}
            pageCount={2}
            pageRangeDisplayed={10}
            onPageChange = {handlePageClick}
        />
 
        <Footer />

        {/* ********************************** Filter Modal Starts Here************************************* */}
        <div id="filterModal" className="modal fade" tabIndex="-1" role="dialog">
            <div className="modal-dialog modal-lg" role="document">

                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title">Narrow your Search</h4>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        
                    </div>

                    <div className="modal-body">
                        <form className="ml-5 mr-5 mt-3" id="filter-form-id">
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="filtertitle"><strong>By Title/Keyword</strong></label>
                                    <input type="search" onChange={(e)=> dispatch({type: 'filterTitleChange', payload: e.target.value})} className="form-control" id="filtertitle" placeholder="Search Title/Keyword" />
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="filter-type"><strong>By Type</strong></label>
                                    <select onChange={(e)=> dispatch({type: 'filterTypeChange', payload: e.target.value})} id="filter-type" className="form-control">
                                        <option value="buy">Buy</option>
                                        <option value="rent">Rent/Lease</option>
                                    </select>
                                </div>
                            </div>
                            <p><strong>By Price Range</strong></p>
                            <div className="form-row">  
                                <div className="form-group col-md-6">
                                    <label htmlFor="filterMinPrice">Minimum Price:</label>
                                    <input type="number" placeholder="$" onChange={(e)=> dispatch({type: 'filterMinPriceChange', payload: e.target.value})} id="filterMinPrice" className="form-control" />
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="filterMaxPrice">Maximum Price</label>
                                    <input type="Number" placeholder="$" onChange={(e)=> dispatch({type: 'filterMaxPriceChange', payload: e.target.value})} id="filterMaxPrice" className="form-control" />
                                </div>
                            </div>
                            <p><strong>By Date Range</strong></p>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="filterStartDate">Start Date &nbsp;&nbsp;</label>
                                    <DatePicker id="filterStartDate" isClearable={true} selected={state.filterStartDate ? state.filterStartDate : new Date()} className="form-control"  onChange={date=> dispatch({type: 'filterStartDateChange', payload: date })}  />
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="filterEndDate">End Date &nbsp;&nbsp;</label>
                                    <DatePicker id="filterEndDate" isClearable={true} selected={state.filterEndDate ? state.filterEndDate : new Date()} className="form-control"  onChange={date=> dispatch({type: 'filterEndDateChange', payload: date})} />
                                </div>
                            </div>
                            <p><strong>By Area</strong></p>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="filterMinArea">Minimum Area</label>
                                    <input type="number" id="filterMinArea" placeholder="in sq. meter" className="form-control" onChange={(e)=> dispatch({type: 'filterMinAreaChange', payload: e.target.value})} />
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="filterMaxArea">Maximum Area</label>
                                    <input type="number" id="filterMaxArea" placeholder="in sq. meter" className="form-control" onChange={(e)=> dispatch({type: 'filterMaxAreaChange', payload: e.target.value})} />
                                </div>
                            </div>
                            <p><strong>By Property Features</strong></p>
                            <div className="form-row">
                                <div className="form-group col-md-3">
                                    <label htmlFor="filterRooms">Rooms</label>
                                    <select className="form-control" id="filterRooms" onChange={(e)=> dispatch({type: 'filterRoomsChange', payload: e.target.value})}>
                                        <option value="Studio">Studio</option><option value="1">1</option><option value="2">2</option><option value="3">3</option>
                                        <option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option>
                                        <option value="8">8</option><option value="9">9</option>
                                        <option value="11">10+</option>
                                    </select>
                                </div>
                                <div className="form-group col-md-3">
                                    <label htmlFor="filterBathrooms">Bathroom</label>
                                    <select className="form-control" id="filterBathrooms" onChange={(e)=> dispatch({type: 'filterBathroomsChange', payload: e.target.value})}>
                                        <option value="1">1</option><option value="2">2</option><option value="3">3</option>
                                        <option value="4">4</option><option value="5">5</option><option value="6">6</option>
                                    </select>
                                </div>
                                <div className="form-group col-md-3">
                                    <label htmlFor="filterCarSpace">Car Space</label>
                                    <select className="form-control" id="filterCarSpace" onChange={(e)=> dispatch({type: 'filterCarSpaceChange', payload: e.target.value})}>
                                        <option value="1">1</option><option value="2">2</option><option value="3">3</option>
                                        <option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option>
                                        <option value="8">8</option><option value="9">9</option>
                                        <option value="11">10+</option>
                                    </select>
                                </div>
                                <div className="form-group col-md-3">
                                    <label htmlFor="filterParkingType">P. Type</label>
                                    <select id="filterParkingType" className="form-control" onChange={(e)=> dispatch({type: 'filterParkingTypeChange', payload: e.target.value})}>
                                        <option value="Garage">Garage</option>
                                        <option value="Covered">Covered</option>
                                        <option value="Street">Street</option>
                                        <option value="None">None</option>
                                    </select>
                                </div>
                            </div>
                            <p><strong>By Inspection Time</strong></p>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="filterInspectionStartDate">Start Date: &nbsp;&nbsp;</label>
                                    <DatePicker  className="form-control" selected={state.filterInspectionStartDate} onChange={(date)=> dispatch({type: 'filterInspectionStartDateChange', payload: date})} />
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="filterInspectionEndDate">End Date: &nbsp;&nbsp;</label>
                                    <DatePicker  className="form-control" selected={state.filterInspectionEndDate} onChange={(date)=> dispatch({type: 'filterInspectionEndDateChange', payload: date})} />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="filterOfferType"><strong>Offer Type</strong></label>
                                    <select id="filterOfferType" className="form-control" onChange={(e)=> dispatch({type: 'filterOfferTypeChange', payload: e.target.value})}>
                                        <option value="offering">Offering</option>
                                        <option value="wanted">Wanted</option>
                                    </select>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="filterPriceType"><strong>Price Type</strong></label>
                                    <select id="filterPriceType" className="form-control" onChange={(e)=> dispatch({type: 'filterPriceTypeChange', payload: e.target.value})}>
                                        <option value="fixed">Fixed</option>
                                        <option value="negotiable">Negotiable</option>
                                        <option value="free">Free</option>
                                        <option value="swaptrade">Swap/Trade</option>
                                    </select>
                                </div>
                            </div>
                            <p><strong>By Property Type</strong></p>
                            <div className="form-check">
                                <input className="form-check-input"  type="checkbox"  value="house" id="filterPropertyHouse" />
                                <label className="form-check-label" htmlFor="filterPropertyHouse">House</label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input"  type="checkbox"  value="apartment" id="filterPropertyApartment" />
                                <label className="form-check-label" htmlFor="filterPropertyApartment">Apartment</label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input"  type="checkbox"  value="townhouse" id="filterPropertyTownHouse" />
                                <label className="form-check-label" htmlFor="filterPropertyTownHouse">Town House</label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input"  type="checkbox"  value="land" id="filterPropertyLand" />
                                <label className="form-check-label" htmlFor="filterPropertyLand">Land</label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input"  type="checkbox"  value="rural" id="filterPropertyRural" />
                                <label className="form-check-label" htmlFor="filterPropertyRural">Rural</label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input"  type="checkbox"  value="newapartment" id="filterPropertyNewApartment" />
                                <label className="form-check-label" htmlFor="filterPropertyNewApartment">New Apartment</label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input"  type="checkbox"  value="newhouseandland" id="filterPropertyNewHouseAndLand" />
                                <label className="form-check-label" htmlFor="filterPropertyNewHouseAndLand">New House And Land</label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input"  type="checkbox"  value="officespace" id="filterPropertyOfficeSpace" />
                                <label className="form-check-label" htmlFor="filterPropertyOfficeSpace">Office Space</label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input"  type="checkbox"  value="commerical" id="filterPropertyCommerical" />
                                <label className="form-check-label" htmlFor="filterPropertyCommerical">Commerical</label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input"  type="checkbox"  value="garageandstorage" id="filterPropertyGarageAndStorage" />
                                <label className="form-check-label" htmlFor="filterPropertyGarageAndStorage">GarageAndStorage</label>
                            </div>
                        </form>
                    </div>

                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={(e)=> filterSubmitHandler(e)}>Apply Filter</button>
                    </div>
                </div>
            </div>
        </div>

        {/* ********************************** Filter Modal Ends Here ************************************** */}
        {/* Favorite Modal Starts Here */}
            <div className="modal fade" data-backdrop="" id="favorite-modal" tabIndex="-1" role="dialog">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title text-success">Not Logged In?</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p><strong>Please Login to continue</strong></p>
                        </div>
                        <div className="modal-footer favorite-modal-footer">
                            <p>Want to login?<Link to="/login"><span className="text-danger">&nbsp;Login</span></Link> </p>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                           
                        </div>
                    </div>
                </div>
            </div>
        </>

    )
}

export default Listing
