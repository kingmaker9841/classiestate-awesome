import React, {useState, useEffect} from 'react';
import './LatestNews.css';
import NewsImg from '../../../assets/images/news/1.jpg';
import axios from 'axios';

function LatestNews() {
    const [newsPosts, setNewsPosts] = useState([]);
    useEffect(()=> {
        axios.get('https://classibazaar.com.au/api/v2/news')
        .then(result => {
            console.log("News Data", result.data);
            setNewsPosts(result.data);
        })
        .catch(err => {
            console.log("News Fetch Error", err.response)
        })
    }, []);
    return (
        <div className="latest-news-container">
            <div className="latest-news-heading">
                <strong>Latest News</strong>
            </div>
            <div className="latest-news-grid-container">
            {
            newsPosts && newsPosts.map((news,idx)=> {
            return (
            <div key={idx} className="latest-news-single">
                <div className="latest-news-single-image-div">
                    <img src={news.news_thumb} alt="news1.jpg" className="latest-news-single-image" />
                </div>
                <div className="latest-news-single-text-div">
                    <div className="latest-news-single-text-title">
                        {/* Cause for Optimism despite failing prices,
                        trade headwinds */}
                        {news.news_title}
                    </div>
                </div>
            </div>
            )
            })
            }
            </div>
            {/* <div className="latest-news-grid-container">
                <div className="latest-news-single">
                    <div className="latest-news-single-image-div">
                        <img src={NewsImg} alt="news1.jpg" className="latest-news-single-image" />
                    </div>
                    <div className="latest-news-single-text-div">
                        <div className="latest-news-single-text-title">
                            Cause for Optimism despite failing prices,
                            trade headwinds
                        </div>
                    </div>
                </div>
                <div className="latest-news-single">
                    <div className="latest-news-single-image-div">
                        <img src={NewsImg} alt="news1.jpg" className="latest-news-single-image" />
                    </div>
                    <div className="latest-news-single-text-div">
                        <div className="latest-news-single-text-title">
                            Cause for Optimism despite failing prices,
                            trade headwinds
                        </div>
                    </div>
                </div>
                <div className="latest-news-single">
                    <div className="latest-news-single-image-div">
                        <img src={NewsImg} alt="news1.jpg" className="latest-news-single-image" />
                    </div>
                    <div className="latest-news-single-text-div">
                        <div className="latest-news-single-text-title">
                            Cause for Optimism despite failing prices,
                            trade headwinds
                        </div>
                    </div>
                </div>
                <div className="latest-news-single">
                    <div className="latest-news-single-image-div">
                        <img src={NewsImg} alt="news1.jpg" className="latest-news-single-image" />
                    </div>
                    <div className="latest-news-single-text-div">
                        <div className="latest-news-single-text-title">
                            Cause for Optimism despite failing prices,
                            trade headwinds
                        </div>
                    </div>
                </div>
            </div> */}
        </div>
    )
}

export default LatestNews
