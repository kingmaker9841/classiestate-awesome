import React, {useEffect, useState} from 'react';
import './Ads.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {useHistory, Link} from 'react-router-dom';

function Ads({data}) {
    let history = useHistory();
    let [isLoggedIn, setIsLoggedIn] = useState(false);
    let [savedFav, setSavedFav] = useState('')
    useEffect(()=>{
        if (localStorage.getItem('token')){
            setIsLoggedIn(true);
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            setSavedFav(cF);
        }
        return ()=>{

        }
    }, [])
    const onClickHandler = (e,id)=>{
        
        history.push('/posts/' + id);
        // let favIcon = document.getElementsByClassName('homead-icon-saveshare-star')[0];
        // let loggedFavIcon = document.getElementsByClassName('homead-icon-saveshare-star-active')[0]
        // let singleDiv = document.getElementsByClassName('ads-single-div')[0];
        // if (e.target === favIcon && e.target === singleDiv){
        //     console.log("FavIcon Clicked");
        // }else {
        //     console.log("Clicked");
        // }
        
    }
    const favoriteClickHandler = (e, id)=>{
        e.stopPropagation();
        if (isLoggedIn){
            if (savedFav){
                if (savedFav.includes(id)){
                    e.target.classList.toggle('homead-icon-saveshare-star');
                    //Now Update savedFav
                    setSavedFav(prev=> prev.replace(id, ''));
                }else {
                    e.target.classList.toggle('homead-icon-saveshare-star-active');
                }
            }else {
                e.target.classList.toggle('homead-icon-saveshare-star-active');
            }
            
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            if (cF){
                if (String(cF).includes(id)){
                    cF = String(cF).replace(id, '')
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }else {
                    cF = String(cF) + ',' + id;
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }  
            }else {
                localStorage.setItem('classi_favorite', JSON.stringify(id));
            }
            
        }
    }
    return (
        <div className="ads-container">
        {
         data && data.map((item,index)=>(
             <div key={index} className="ads-single-div" onClick={e=> onClickHandler(e,item.post_id)}>
                 <div className="ads-single-image-div">
                     <img src={item.default_image} alt={`${index}.jpg`} className="ads-single-image" />
                     <FontAwesomeIcon icon="star"
                     data-toggle={isLoggedIn?null:'modal'}
                     data-target="#favorite-modal"
                     onClick={(e)=> favoriteClickHandler(e, item.post_id)} 
                     className={
                        savedFav 
                        ? (savedFav.includes(item.post_id) 
                        ? "homead-icon-saveshare-star-active"
                        : "homead-icon-saveshare-star") 
                        : "homead-icon-saveshare-star"
                    }/>
                 </div>
                 <div className="ads-single-other-div">
                    <div className="ads-single-other-address">
                        {item.ad_address? (item.ad_address.length>25?item.ad_address.slice(0,25)+' ...': item.ad_address): "Unknown"}
                    </div>
                    <div className="ads-single-other-title">
                        {item.title && item.title.length>30?item.title.slice(0,25)+' ...': item.title}
                    </div>
                    <div className="ads-single-other-price">
                        {'$ ' + item.price}
                    </div>
                    <div className="ads-single-other-icons">
                        <div className="ads-single-other-icons-all ads-single-other-icons-bed">
                            <FontAwesomeIcon icon="bed" className="ads-single-other-font-icon" />
                            <span> {item.no_room} </span>
                        </div>
                        <div className="ads-single-other-icons-all ads-single-other-icons-bath">
                            <FontAwesomeIcon icon="bath" className="ads-single-other-font-icon" />
                            <span> {item.no_of_bathroom} </span>
                        </div>
                        <div className="ads-single-other-icons-all ads-single-other-icons-parking">
                            <FontAwesomeIcon icon="parking" className="ads-single-other-font-icon" />
                            <span> {item.no_of_car_space} </span>
                        </div>
                    </div>
                 </div>
             </div>
         ))   
        }
         {/* Favorite Modal Starts Here */}
         <div className="modal fade"  id="favorite-modal" tabIndex="-1" role="dialog">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Not Logged In?</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p><strong>Please Login to continue</strong></p>
                        </div>
                        <div className="modal-footer favorite-modal-footer">
                            <p>Want to login?<Link to="/login"><span className="text-danger">&nbsp;Login</span></Link> </p>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Ads
