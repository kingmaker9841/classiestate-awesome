import React, {useState, useEffect} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import Commercial from '../../../assets/images/home-icons/commercial.svg';
import Development from '../../../assets/images/home-icons/development.svg';
import Hotel from '../../../assets/images/home-icons/hotel.svg';
import Medical from '../../../assets/images/home-icons/medical.svg';
import Offices from '../../../assets/images/home-icons/offices.svg';
import Other from '../../../assets/images/home-icons/other.svg';
import ShopRetail from '../../../assets/images/home-icons/shop-retial.svg';
import Showroom from '../../../assets/images/home-icons/showroom.svg';
import Warehouse from '../../../assets/images/home-icons/warehouse.svg';
import './ExploreByProperty.css';
import axios from 'axios';

function ExploreByProperty() {
    const [exploreProperty, setExploreProperty] = useState({});
    useEffect(() => {
        console.log("Inside ExploreProperty")
        axios.get('https://classibazaar.com.au/api/v2/property_type')
        .then(result=> {
            console.log("ExploreProperty",result.data);
            setExploreProperty(result.data);
        })
        .catch(err=> {
            console.log("Getting ExploreByProperty Err", err.response)
        })
        return () => {
        }
    }, [])
    return (
        <div className="explore-property-div">
            <span className="explore-property-div-text"><strong>Explore by property type</strong></span>
            <div className="explore-property-div-grid">
                <div className="explore-property-div-grid-single explore-property-div-grid-warehouse">
                    <img src={Warehouse} alt="warehouse.svg" className="explore-property-div-grid-icon explore-property-div-grid-warehouse" />
                    <div className="explore-property-div-grid-text explore-property-div-grid-warehouse">
                        {/* Warehouse, Factory &amp; Industrial */}
                        {
                            exploreProperty && 
                            Object.values(exploreProperty)[0]
                        }
                    </div>
                </div>
                <div className="explore-property-div-grid-single explore-property-div-grid-shop">
                    <img src={ShopRetail} alt="shop.svg" className="explore-property-div-grid-icon explore-property-div-grid-shop" />
                    <div className="explore-property-div-grid-text explore-property-div-grid-shop">
                        {/* Shop &amp; Retail */}
                        {
                            exploreProperty && 
                            Object.values(exploreProperty)[1]
                        }
                    </div>
                </div>
                <div className="explore-property-div-grid-single explore-property-div-grid-offices">
                    <img src={Offices} alt="offices.svg" className="explore-property-div-grid-icon explore-property-div-grid-offices" />
                    <div className="explore-property-div-grid-text explore-property-div-grid-offices">
                        {/* Offices */}
                        {
                            exploreProperty && 
                            Object.values(exploreProperty)[2]
                        }
                    </div>
                </div>
                <div className="explore-property-div-grid-single explore-property-div-grid-development">
                    <img src={Development} alt="development.svg" className="explore-property-div-grid-icon explore-property-div-grid-development" />
                    <div className="explore-property-div-grid-text explore-property-div-grid-development">
                        {/* Development Sites &amp; Land */}
                        {
                            exploreProperty && 
                            Object.values(exploreProperty)[3]
                        }
                    </div>
                </div>
                <div className="explore-property-div-grid-single explore-property-div-grid-showroom">
                    <img src={Showroom} alt="showroom.svg" className="explore-property-div-grid-icon explore-property-div-grid-showroom" />
                    <div className="explore-property-div-grid-text explore-property-div-grid-showroom">
                        {/* Showroom &amp; Bulky Goods */}
                        {
                            exploreProperty && 
                            Object.values(exploreProperty)[4]
                        }
                    </div>
                </div>
                <div className="explore-property-div-grid-single explore-property-div-grid-hotel">
                    <img src={Hotel} alt="hotel.svg" className="explore-property-div-grid-icon explore-property-div-grid-hotel" />
                    <div className="explore-property-div-grid-text explore-property-div-grid-hotel">
                        {/* Hotel &amp; Leisure */}
                        {
                            exploreProperty && 
                            Object.values(exploreProperty)[5]
                        }
                    </div>
                </div>
                <div className="explore-property-div-grid-single explore-property-div-grid-medical">
                    <img src={Medical} alt="medical.svg" className="explore-property-div-grid-icon explore-property-div-grid-medical" />
                    <div className="explore-property-div-grid-text explore-property-div-grid-medical">
                        {/* Medical &amp; Consulting */}
                        {
                            exploreProperty && 
                            Object.values(exploreProperty)[6]
                        }
                    </div>
                </div>
                <div className="explore-property-div-grid-single explore-property-div-grid-commercial">
                    <img src={Commercial} alt="commercial.svg" className="explore-property-div-grid-icon explore-property-div-grid-commercial" />
                    <div className="explore-property-div-grid-text explore-property-div-grid-commercial">
                        {/* Commercial Farming &amp; Rural */}
                        {
                            exploreProperty && 
                            Object.values(exploreProperty)[7]
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ExploreByProperty
