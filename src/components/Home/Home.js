import React, {useEffect, useState} from 'react'
import useDocTitle from '../../customHooks/useDocTitle';
import Navigation from '../Navigation/Navigation';
import './Home.css';
import HomePageAds from '../HomePageAds/HomePageAds';
import FeaturedAds from '../HomePageAds/FeaturedAds';
import Footer from '../Footer/Footer';
import GlideCarousel from '../../library/Glide/GlideCarousel';
import NewHomeSearch from '../NewHomeSearch/NewHomeSearch';
import ExploreByProperty from './ExploreByProperty/ExploreByProperty';
import CarouselExploreByProperty from './CarouselExploreByProperty/CarouselExploreByProperty'
import ExploreByArea from './ExploreByArea/ExploreByArea';
import LatestNews from './LatestNews/LatestNews';
import LatestAds from './LatestAds/LatestAds';
import NewFooter from '../NewFooter/NewFooter';

function Home() {
    let [winWidth, setWinWidth] = useState(window.innerWidth);
    useDocTitle("ClassiEstate -Homepage")
    useEffect(()=>{
        window.addEventListener('resize', (e)=>{
            setWinWidth(window.innerWidth);
        })
    })
    return (
        <div>
            <Navigation fromHome={true} />
            {/* <Search /> */}
            <NewHomeSearch />
            <GlideCarousel />
            {winWidth>1030?(
                <ExploreByProperty />
            ): (<CarouselExploreByProperty />)}
            
            <ExploreByArea />
            <LatestAds />
            <LatestNews />
            <NewFooter />
            {/* <FeaturedAds />
            <div className="home-classi">
                <div className="home-classi-p">
                    <strong>Latest Posted Ads</strong>
                </div>
                <div className="home-classi-show-all-div">Show All</div>
            </div>

            <HomePageAds /> */}
            {/* <Footer /> */}
            
        </div>
    )
}

export default Home
