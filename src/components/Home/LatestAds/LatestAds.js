import React, {useState,useEffect} from 'react';
import './LatestAds.css';
import axios from 'axios';
import Ads from '../Ads/Ads';

function LatestAds() {
    let [posts, setPosts] = useState([]);
    useEffect(()=>{
        axios.get('/api/v2/ads_list?slug=real-estate&api_from=classiEstate')
        .then(res=>{
            console.log(res.data);
            setPosts(res.data.posts.filter((item,index)=> index<5))
        })
        .catch(err=>{
            console.log(err.response);
        })
    }, [])
    return (
        <div className="latest-ads-container">
            <div className="latest-ads-title-see-div">
                <div className="latest-ads-title">
                    <strong>Latest Posted Ads</strong>
                </div>
                <div className="latest-ads-see-all">
                    See All
                </div>
            </div>
            <Ads data={posts} />
        </div>
    )
}

export default LatestAds
