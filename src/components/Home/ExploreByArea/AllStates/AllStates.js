import React from 'react';
import './AllStates.css';
import Carousel from 'react-grid-carousel';
import Vic from '../../../../assets/images/explore-area/all-states/vic.jpg'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

function AllStates() {
    return (
        <div className="all-states-container">
            <Carousel rows={1} cols={5} gap={10} containerClassName="all-states-carousel-container"
             responsiveLayout={[
                 {
                     breakpoint: 1125,
                     cols: 4,
                     gap: 10
                 },
                 {
                     breakpoint: 945,
                     cols: 3,
                     gap: 10
                 }, 
                //  {
                //      breakpoint: 595,

                //  }
             ]} mobileBreakpoint={250}>
                <Carousel.Item>
                    <div className="all-states-carousel-item">
                        <img src={Vic} alt="vic.jpg" className="all-states-image all-states-vic-image" />
                        <div className="all-states-carousel-text-div">
                            <div className="all-states-carousel-text-title">
                                Victoria
                            </div>
                            <div className="all-states-carousel-text-info">
                                Explore commercial real estate in Australia's fastest growing state
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="all-states-carousel-item">
                        <img src={Vic} alt="vic.jpg" className="all-states-image all-states-vic-image" />
                        <div className="all-states-carousel-text-div">
                            <div className="all-states-carousel-text-title">
                                Victoria
                            </div>
                            <div className="all-states-carousel-text-info">
                                Explore commercial real estate in Australia's fastest growing state
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="all-states-carousel-item">
                        <img src={Vic} alt="vic.jpg" className="all-states-image all-states-vic-image" />
                        <div className="all-states-carousel-text-div">
                            <div className="all-states-carousel-text-title">
                                Victoria
                            </div>
                            <div className="all-states-carousel-text-info">
                                Explore commercial real estate in Australia's fastest growing state
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="all-states-carousel-item">
                        <img src={Vic} alt="vic.jpg" className="all-states-image all-states-vic-image" />
                        <div className="all-states-carousel-text-div">
                            <div className="all-states-carousel-text-title">
                                Victoria
                            </div>
                            <div className="all-states-carousel-text-info">
                                Explore commercial real estate in Australia's fastest growing state
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="all-states-carousel-item">
                        <img src={Vic} alt="vic.jpg" className="all-states-image all-states-vic-image" />
                        <div className="all-states-carousel-text-div">
                            <div className="all-states-carousel-text-title">
                                Victoria
                            </div>
                            <div className="all-states-carousel-text-info">
                                Explore commercial real estate in Australia's fastest growing state
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="all-states-carousel-item">
                        <img src={Vic} alt="vic.jpg" className="all-states-image all-states-vic-image" />
                        <div className="all-states-carousel-text-div">
                            <div className="all-states-carousel-text-title">
                                Victoria
                            </div>
                            <div className="all-states-carousel-text-info">
                                Explore commercial real estate in Australia's fastest growing state
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="all-states-carousel-item">
                        <img src={Vic} alt="vic.jpg" className="all-states-image all-states-vic-image" />
                        <div className="all-states-carousel-text-div">
                            <div className="all-states-carousel-text-title">
                                Victoria
                            </div>
                            <div className="all-states-carousel-text-info">
                                Explore commercial real estate in Australia's fastest growing state
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="all-states-carousel-item">
                        <img src={Vic} alt="vic.jpg" className="all-states-image all-states-vic-image" />
                        <div className="all-states-carousel-text-div">
                            <div className="all-states-carousel-text-title">
                                Victoria
                            </div>
                            <div className="all-states-carousel-text-info">
                                Explore commercial real estate in Australia's fastest growing state
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
            </Carousel>
        </div>
    )
}

export default AllStates
