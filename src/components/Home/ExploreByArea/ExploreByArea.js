import React, {useEffect, useState} from 'react';
import './ExploreByArea.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import AllStates from './AllStates/AllStates';
import PopularAreas from './PopularAreas/PopularAreas';
import Act from './Act/Act';
import Nsw from './Nsw/Nsw';
import Nt from './Nt/Nt';
import Qld from './Qld/Qld';
import Sa from './Sa/Sa';
import Tas from './Tas/Tas';
import Vic from './Vic/Vic';
import Wa from './Wa/Wa';

function ExploreByArea() {
    let [show, setShow] = useState('all-states');
    useEffect(()=>{
        let navExplore = document.getElementsByClassName('home-explore-area-text-single');
        for(let i = 0; i<navExplore.length; i++){
            navExplore[i].addEventListener('click', (e)=>{
                let current = document.getElementsByClassName('home-explore-area-text-active')[0];
                current.className = current.className.replace(' home-explore-area-text-active', '');
                navExplore[i].className += ' home-explore-area-text-active';
            })
        }
        //Custom Select Box
        let selectBox = document.getElementsByClassName('area-custom-selected')[0];
        selectBox.addEventListener('click', (e)=>{
            let optionBox = document.getElementsByClassName('area-custom-select-options')[0];
            optionBox.classList.toggle('area-custom-select-options-toggle');
        })
        let options = document.getElementsByClassName('area-option');
        for (let i = 0; i<options.length; i++){
            options[i].addEventListener('click', (e)=>{
                let cur = document.getElementsByClassName('area-option-selected')[0];
                cur.className = cur.className.replace('area-option-selected', '');
                options[i].className += ' area-option-selected';
                let optionBox = document.getElementsByClassName('area-custom-select-options')[0];
                if (optionBox.classList.contains('area-custom-select-options-toggle')){
                    optionBox.className = optionBox.className.replace('area-custom-select-options-toggle', '');
                }
                let selectText = document.getElementsByClassName('area-custom-selected-text')[0];
                selectText.innerText = options[i].innerText;
                setShow(options[i].innerText);

                //CHange select box 
                let selectBox = document.getElementsByClassName('area-custom-selected')[0];
                selectBox.classList.add('area-custom-selected-toggle');
            })
        }
        //When Clicked on body, hide select options
        document.body.addEventListener('click',(e)=>{
            let selectBox = document.getElementsByClassName('area-custom-selected')[0];
            let optionBox = document.getElementsByClassName('area-custom-select-options')[0];
            if ((e.target !== selectBox) && optionBox && optionBox.classList.contains('area-custom-select-options-toggle')){
                console.log("True");
                optionBox && optionBox.classList.remove('area-custom-select-options-toggle');
            }
            //For Dashed Border < 792px
            let singleEl = document.getElementsByClassName('home-explore-area-text-single');
            for (let i = 0; i<singleEl.length; i++){
                if (e.target != singleEl[i]){
                    singleEl[i].style.border = '1px solid transparent';
                }
            }

        })
        return ()=>{
            // for (let i = 0; i<navExplore.length; i++){
            //     navExplore[i].removeEventListener('click', ()=> {})
            // }
            document.body.removeEventListener('click', ()=> {})

        }
    }, [])
    const onClickHandler = (txt,cl) =>{
        setShow(txt);
        let all = document.getElementsByClassName('home-explore-area-text-single');
        for (let i = 0; i<all.length; i++){
            all[i].style.border = '1px solid transparent';
        }
        let cur = document.getElementsByClassName(cl)[0];
        cur.style.border = '1px dashed rgba(0,0,0,0.8)';
    }
    return (
        <div className="home-explore-by-area-div">
            <div className="home-explore-by-area-text">
                <strong>
                    Explore by Area
                </strong>
                <div className="home-explore-by-area-nav-text">
                    <div onClick={()=> onClickHandler('all-states', 'home-explore-area-text-all')} className="home-explore-area-text-single home-explore-area-text-all home-explore-area-text-active">
                        All States
                    </div>
                    <div onClick={()=> onClickHandler('popular-areas', 'home-explore-area-text-popular')} className="home-explore-area-text-single home-explore-area-text-popular">
                        Popular Areas
                    </div>
                    <div onClick={()=> onClickHandler('vic', 'home-explore-area-text-vic')} className="home-explore-area-text-single home-explore-area-text-vic">
                        VIC
                    </div>
                    <div onClick={()=> onClickHandler('nsw', 'home-explore-area-text-nsw')} className="home-explore-area-text-single home-explore-area-text-nsw">
                        NSW
                    </div>
                    <div onClick={()=> onClickHandler('qld', 'home-explore-area-text-qld')} className="home-explore-area-text-single home-explore-area-text-qld">
                        QLD
                    </div>
                    <div onClick={()=> onClickHandler('wa', 'home-explore-area-text-wa')} className="home-explore-area-text-single home-explore-area-text-wa">
                        WA
                    </div>
                    <div onClick={()=> onClickHandler('sa', 'home-explore-area-text-sa')} className="home-explore-area-text-single home-explore-area-text-sa">
                        SA
                    </div>
                    <div onClick={()=> onClickHandler('tas', 'home-explore-area-text-tas')} className="home-explore-area-text-single home-explore-area-text-tas">
                        TAS
                    </div>
                    <div onClick={()=> onClickHandler('act', 'home-explore-area-text-act')} className="home-explore-area-text-single home-explore-area-text-act">
                        ACT
                    </div>
                    <div onClick={()=> onClickHandler('nt', 'home-explore-area-text-nt')} className="home-explore-area-text-single home-explore-area-text-nt">
                        NT
                    </div>
                </div>
                <div className="home-explore-area-custom-select-wrapper">
                    <div className="area-custom-selected">
                        <span className="area-custom-selected-text">All States</span>
                        <div className="area-custom-arrow">
                            <FontAwesomeIcon icon="chevron-down" className="area-custom-arrow-icon" />
                        </div>
                    </div>
                    <div className="area-custom-select-options">
                        <div className="area-option area-option-selected">All States</div>
                        <div className="area-option">Popular Areas</div>
                        <div className="area-option">VIC</div>
                        <div className="area-option">NSW</div>
                        <div className="area-option">QLD</div>
                        <div className="area-option">WA</div>
                        <div className="area-option">SA</div>
                        <div className="area-option">TAS</div>
                        <div className="area-option">ACT</div>
                        <div className="area-option">NT</div>
                    </div>
                </div>
            </div>
            <div className="home-explore-by-area-grid-div">
                {(show == 'all-states' || show === 'All States') && (<AllStates />)}
                {(show == 'popular-areas' || show === 'Popular Areas') && (<PopularAreas />)}
                {(show == 'vic' || show === 'VIC') && (<Vic />)}
                {(show == 'nsw' || show === 'NSW') && (<Nsw />)}
                {(show == 'qld' || show === 'QLD') && (<Qld />)}
                {(show == 'wa' || show === 'WA') && (<Wa />)}
                {(show == 'sa' || show === 'SA') && (<Sa />)}
                {(show == 'tas' || show === 'TAS') && (<Tas />)}
                {(show == 'act' || show === 'ACT') && (<Act />)}
                {(show == 'nt' || show === 'NT') && (<Nt />)}
            </div>
        </div>
    )
}

export default ExploreByArea
