import React, {useState, useEffect} from 'react';
import './CarouselExploreByProperty.css';
import Carousel from 'react-grid-carousel';
import Commercial from '../../../assets/images/home-icons/commercial.svg';
import Development from '../../../assets/images/home-icons/development.svg';
import Hotel from '../../../assets/images/home-icons/hotel.svg';
import Medical from '../../../assets/images/home-icons/medical.svg';
import Offices from '../../../assets/images/home-icons/offices.svg';
import ShopRetail from '../../../assets/images/home-icons/shop-retial.svg';
import Showroom from '../../../assets/images/home-icons/showroom.svg';
import Warehouse from '../../../assets/images/home-icons/warehouse.svg';
import { faFileMedicalAlt } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';

function CarouselExploreByProperty() {
    const [exploreProperty, setExploreProperty] = useState({});
    useEffect(() => {
        console.log("Inside ExploreProperty")
        axios.get('https://classibazaar.com.au/api/v2/property_type')
        .then(result=> {
            console.log("ExploreProperty",result.data);
            setExploreProperty(result.data);
        })
        .catch(err=> {
            console.log("Getting ExploreByProperty Err", err.response)
        })
        return () => {
        }
    }, [])
    return (
        <div className="carousel-explore-property-div">
            <span className="explore-property-div-text"><strong>Explore by property type</strong></span>
            <div className="carousel-explore-property-grid-div">
            <Carousel cols={3} rows={1} gap={10} mobileBreakpoint={300}
            responsiveLayout={[{
                breakpoint: 692,
                cols: 2,
                rows: 1
            }]}>
                <Carousel.Item>
                    <div className="carousel-explore-property-single">
                        <img src={Warehouse} className="carousel-explore-property-icon carousel-explore-property-icon-warehouse" />
                        <p className="carousel-explore-property-text">
                            {/* Warehouse, Factory &amp; Industrial
                            */}
                            {
                                exploreProperty && 
                                Object.values(exploreProperty)[0]
                            }
                        </p>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="carousel-explore-property-single">
                        <img src={ShopRetail} className="carousel-explore-property-icon" />
                        <div className="carousel-explore-property-text">
                            {/* Shop &amp; Retail */}
                            {
                                exploreProperty && 
                                Object.values(exploreProperty)[1]
                            }
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="carousel-explore-property-single">
                        <img src={Offices} className="carousel-explore-property-icon" />
                        <div className="carousel-explore-property-text">
                            {/* Offices */}
                            {
                                exploreProperty && 
                                Object.values(exploreProperty)[2]
                            }
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="carousel-explore-property-single">
                        <img src={Development} className="carousel-explore-property-icon" />
                        <div className="carousel-explore-property-text">
                            {/* Development Sites &amp; Land */}
                            {
                                exploreProperty && 
                                Object.values(exploreProperty)[3]
                            }
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="carousel-explore-property-single">
                        <img src={Showroom} className="carousel-explore-property-icon" />
                        <div className="carousel-explore-property-text">
                            {/* Showroom &amp; Bulky Goods */}
                            {
                                exploreProperty && 
                                Object.values(exploreProperty)[4]
                            }
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="carousel-explore-property-single">
                        <img src={Hotel} className="carousel-explore-property-icon" />
                        <div className="carousel-explore-property-text">
                            {/* Hotel &amp; Leisure */}
                            {
                                exploreProperty && 
                                Object.values(exploreProperty)[5]
                            }
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="carousel-explore-property-single">
                        <img src={Medical} className="carousel-explore-property-icon" />
                        <div className="carousel-explore-property-text">
                            {/* Medical &amp; Consulting */}
                            {
                                exploreProperty && 
                                Object.values(exploreProperty)[6]
                            }
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="carousel-explore-property-single">
                        <img src={Commercial} className="carousel-explore-property-icon" />
                        <div className="carousel-explore-property-text">
                            {/* Commercial, Farming &amp; Rural */}
                            {
                                exploreProperty && 
                                Object.values(exploreProperty)[7]
                            }
                        </div>
                    </div>
                </Carousel.Item>
            </Carousel>
            </div>
        </div>
    )
}

export default CarouselExploreByProperty
