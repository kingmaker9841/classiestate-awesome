import React,{useReducer, useState, useEffect} from 'react'
import {Redirect, Link} from 'react-router-dom';
import useDocTitle from '../../customHooks/useDocTitle';
import '../Register/Register.css';
import './Login.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import axios from 'axios';
import ClipLoader from 'react-spinners/ClipLoader';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import ClassiLogo from '../../assets/images/classi-logo.png';
import LoginPageBg from '../../assets/images/authentication/login-page-bg.jpg';

const initialState = {
    email: '',
    password: '',
    error: '',
    loading: false,
    success: false,
}
const reducer = (state, action)=>{
    switch(action.type){
        case 'emailChange': return {...state, email: action.payload}
        case 'passwordChange': return {...state, password: action.payload}
        case 'postError' : return {...state, error: action.payload}
        case 'loadingTrue' : return {...state, loading: true}
        case 'loadingFalse' : return {...state, loading: false}
        case 'success' : return {...state, success: true}
        default:  return {...state}
    }
}

function Login(props) {
    //Document Title
    useDocTitle("Login -ClassiEstate");

    //Reducer
    const [state, dispatch] = useReducer(reducer, initialState);
    let [fromRegister, setFromRegister] = useState(false);

    //SuccessSocial
    let [successSocial, setSuccessSocial] = useState(false);
    useEffect(()=>{
        if (props.location && props.location.state && props.location.state.fromLogout){
            localStorage.removeItem('token');
        }
        //Registration Successful
        if(props.location && props.location.state && props.location.state.fromRegister){ 
            setFromRegister(true); 
        }   
        
        return ()=> {

        }
    }, [props.location])
    

    // Handling Submit
    const onSubmitHandler = async (e)=>{
        e.preventDefault();

        let isActive = true;

        if (isActive){

            //Loading Starts
            dispatch({type: 'loadingTrue'})

            const {email, password} = state;

            //Post data with credentials
            axios.post('/api/v2/login_user', {
                email, password
            })

            .then(response=>{
                console.log(response.data);
                console.log(response);

                response.data.message === 'Validation Failed' && 
                dispatch({type: 'postError', payload: 'The password field is required.'});

                if (response.status === 200) {
                    localStorage.setItem('token', JSON.stringify({
                        access_token: response.data.access_token,
                        issued_at: response.data.issued_at,
                        refresh_token: response.data.refresh_token
                    }));
                    dispatch({type: 'success'});
                }
                
            })
            .catch((error)=>{
                window.scrollTo(0,0);
                dispatch({type: 'loadingFalse'});
                console.log("Inside error",error);
                let e = error.response.data;

                e && e.message === 'email or password do not match' &&
                dispatch({type: 'postError', payload: 'Email or Password do not match. Please check and Try Again'})
            
                
                e && e.message === 'Your account is not activated yet. Please enter the code sent in email to verify your email.' &&
                dispatch({type: 'postError', payload: 'Your account is not activated yet. Please enter the code sent in email to verify your email.'});

                
            })

        }

        return isActive = false;

    }

    const responseFacebook = (response)=>{

        console.log(response);

        axios.post('/api/v2/social_authenticate', {
            first_name: response.first_name,
            last_name: response.last_name,
            email: response.email,
            token: response.accessToken,
            is_apple: 1
        })
        .then(res=>{
            console.log(res);
            console.log(res.data);

            if (res.data.message === 'Validation Failed'){
                window.scrollTo(0,0);
                console.log("Login inside then facebook");
                dispatch({type: 'postError', payload: 'Error from Facebook. Please clear the cache/history/images and try again.'});

            }
            if (res.data.access_token) {
                localStorage.setItem('token', JSON.stringify({
                    access_token: res.data.access_token,
                    issued_at: res.data.issued_at,
                    refresh_token: res.data.refresh_token
                }));
                setSuccessSocial(true);
            }
        })
        .catch(error=>{
            console.log(error);
            console.log(error.response)

            window.scrollTo(0,0);
            let e = error.response.data;

            e && e.message === 'email or password do not match' &&
            dispatch({type: 'postError', payload: 'Email or Password do not match. Please check and Try Again'})
        
            
            e && e.message === 'Your account is not activated yet. Please enter the code sent in email to verify your email.' &&
            dispatch({type: 'postError', payload: 'Your account is not activated yet. Please enter the code sent in email to verify your email.'});

            dispatch({type: 'loadingFalse'});
        })
    }

    const responseGoogle = async (response)=>{
        console.log(response);

        axios.post('/api/v2/social_authenticate', {
            first_name: response.profileObj.givenName,
            last_name: response.profileObj.familyName,
            email: response.profileObj.email,
            token: response.accessToken,
            is_apple: 1
        })
        .then(res=>{

            console.log(res);
            console.log(res.data);

            if (res.data.message === 'Validation Failed'){
                window.scrollTo(0,0);
                console.log("Login inside then Google");
                dispatch({type: 'postError', payload: 'Error from Google. Please clear the cache/history/images and try again.'});

            }

            if (res.data.access_token) {
                localStorage.setItem('token', JSON.stringify({
                    access_token: res.data.access_token,
                    issued_at: res.data.issued_at,
                    refresh_token: res.data.refresh_token
                }));
                setSuccessSocial(true);
            }
        })
        .catch(error=>{
            console.log(error);
            console.log(error.response);

            window.scrollTo(0,0);
            let e = error.response.data;

            e && e.message === 'email or password do not match' &&
            dispatch({type: 'postError', payload: 'Email or Password do not match. Please check and Try Again'})
        
            
            e && e.message === 'Your account is not activated yet. Please enter the code sent in email to verify your email.' &&
            dispatch({type: 'postError', payload: 'Your account is not activated yet. Please enter the code sent in email to verify your email.'});

            dispatch({type: 'loadingFalse'});
        })
    
    }

    return (
        <div className="container-register">

            {/* ******************Left Container Start****************** */}
            <div className="left-container">
                <Link to="/">
                    <img src={ClassiLogo} className="register-login-logo" width="200px" height="100px" alt="classiEstateLogo.png"/>
                </Link>
                <div className="register-text">
                    <h3> Welcome Back</h3>
                    <p><strong className="text-muted">Please log into your account</strong></p>
                </div>


                {/* Handle Error */}
                {
                    state.error && 
                    <p className="alert alert-danger mb-3" role="alert"> {state.error} </p>
                }
                {/* Redirection from Register */}
                {
                    fromRegister && 
                    <p className="alert alert-warning mb-3" role="alert">Registration Successful. Please check your email and follow the instruction to activate your account.</p> 
                }


                {/* Input Credentials Start--------------- */}
                <form className="register-form" onSubmit={(e)=> onSubmitHandler(e)}>
                    <p className="text-muted marked-fields">All the fields marked with <span className="text-danger">*</span> are required</p>

                    <div className="form-group col-md-10">
                        <label htmlFor="email" ><strong>Email<span className="text-danger">*</span></strong></label>
                        <input type="email" onChange={(e)=> dispatch({type: 'emailChange', payload: e.target.value})} className="form-control form-control-lg" id="email" required/>
                    </div>

                    <div className="form-group col-md-10">
                        <label htmlFor="newPassword" ><strong>Password<span className="text-danger">*</span></strong></label>
                        <input type="password" onChange={(e)=> dispatch({type: 'passwordChange', payload: e.target.value})} className="form-control form-control-lg" id="newPassword" maxLength="50" required/>  
                    </div>
                    {/* Input Credentials End--------------- */}


                    {/* Submit Button */}
                    <div className="form-group col-md-10 mt-5">
                        {
                            state.loading ? (
                                <button className="form-control btn btn-success btn-lg btn-submit">
                                    <ClipLoader loading={state.loading} color={"#f4f4f4"} />
                                </button>
                            ) : (
                                <button type="submit" className="form-control btn btn-success btn-lg btn-submit">
                                    <FontAwesomeIcon icon="unlock-alt" className="lock-register"/>
                                    &nbsp;Login
                                </button>
                            )
                        }
                    </div>
                    <div className="register-social-media">
                        <div className="mt-5">
                            <strong>Or Connect with</strong>
                        </div>

                        {/* Social Media Button */}
                        <div className="social-btn mt-5">
                            
                            <GoogleLogin 
                                clientId ="751639415076-18g0bsmpkcaj6hnbub861qr0df4p33ar.apps.googleusercontent.com"
                                buttonText="Google+"
                                theme="dark"
                                className="google-btn btn btn-large"
                                onSuccess={responseGoogle}
                                onFailure={responseGoogle}
                            >
                            </GoogleLogin>
                            <FacebookLogin 
                                    appId="351080506307919"
                                    fields ="first_name,last_name,email"
                                    
                                    callback = {responseFacebook}
                                    cssClass = "facebook-btn btn btn-large p-2 pl-4 pr-5"
                                    
                                    icon="fa-facebook"
                                    textButton="&nbsp;&nbsp;Facebook"
                            />
                        </div>
                    </div>
                    {/* Social Media Button Testing */}

                    <div className="social-btn-test mt-5 mb-5" >
                        <div className="test-facebook-btn">
                            
                        </div>
                    </div>

                </form>
            </div>
            {/* ****************************Left Container End********************* */}

            {/* Right Container Image Div */}
            <div className="right-container right-container-login">
                <img src={LoginPageBg} className="img-classiBuilding" alt="classEstateBuilding.jpeg"/>
            </div>


            {/* Successful Login ? */}

            {
              state.success && <Redirect to={{
                    pathname: "/",
                    state: {
                        fromLogin: true
                    }
                }} />
            }

            {/* Social Success Login */}
            {
                successSocial &&
                <Redirect to={{
                    pathname: "/",
                    state: {
                        fromLogin: true
                    }
                }} />
            }

        </div>
    )
}

export default Login
