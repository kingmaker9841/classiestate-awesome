import React from 'react';
import './NewListingPageTopTitle.css';

function NewListingPageTopTitle({topTitle}) {
    return (
        <div className="new-listing-top-title-container">
            <strong>
                {topTitle}
            </strong>
        </div>
    )
}

export default NewListingPageTopTitle
