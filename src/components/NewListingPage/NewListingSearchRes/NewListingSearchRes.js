import React from 'react';
import './NewListingSearchRes.css';

function NewListingSearchRes({buyRent}) {
    return (
        <div className="new-listing-search-res-container">
            <div className="new-listing-search-res-items">
            {buyRent && buyRent.map((item,index)=>(
                <div key={index} className="new-listing-search-res-buyrent">
                    {item}{buyRent.length !== index +1 && (<span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;</span>)}
                </div>
            ))}
            </div>
        </div>
    )
}

export default NewListingSearchRes
