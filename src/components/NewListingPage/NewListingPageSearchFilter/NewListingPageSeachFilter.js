import React from 'react';
import './NewListingPageSearchFilter.css';

function NewListingPageSeachFilter({toParentFilter}) {
    let allItem = document.getElementsByClassName('new-listing-search-filter-all-item');
    for(let i=0; i<allItem.length; i++){
        allItem[i].addEventListener('click', (e)=>{
            let current = document.getElementsByClassName('new-listing-search-filter-active-item')[0];
            current.className = current.className.replace('new-listing-search-filter-active-item', '');
            allItem[i].className += ' new-listing-search-filter-active-item';
            toParentFilter(allItem[i].getAttribute('value'));
        })
    }
    return (
        <div className="new-listing-search-filter-container">
            <div value="all" className="new-listing-search-filter-all-item new-listing-search-filter-home new-listing-search-filter-active-item">
                Homes for you
            </div>
            <div value="new" className="new-listing-search-filter-all-item new-listing-search-filter-newest">
                Newest
            </div>
            <div value="old" className="new-listing-search-filter-all-item new-listing-search-filter-oldest">
                Oldest
            </div>
            <div value="expensive" className="new-listing-search-filter-all-item new-listing-search-filter-expensive">
                Expensive
            </div>
            <div value="cheap" className="new-listing-search-filter-all-item new-listing-search-filter-cheapest">
                Cheapest
            </div>
            <div value="inspection" className="new-listing-search-filter-all-item new-listing-search-filter-inspection">
                Inspection
            </div>
        </div>
    )
}

export default NewListingPageSeachFilter
