import React from 'react';
import './NewListingSearchLength.css';

function NewListingSearchLength({searchLength}) {
    return (
        <div className="new-listing-search-length-container">
            Displaying {searchLength}+ Australian property
        </div>
    )
}

export default NewListingSearchLength
