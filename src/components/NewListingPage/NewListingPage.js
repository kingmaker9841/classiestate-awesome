import React, {useEffect,useState, useContext} from 'react';
import './NewListingPage.css'
import Navigation from '../Navigation/Navigation';
import NewListingFixedNav from './NewListingFixedNav/NewListingFixedNav';
import NewListingSearchRes from './NewListingSearchRes/NewListingSearchRes';
import NewListingPageTopTitle from './NewListingPageTopTitle/NewListingPageTopTitle'
import NewListingSearchLength from './NewListingSearchLength/NewListingSearchLength';
import NewListingPageSearchFilter from './NewListingPageSearchFilter/NewListingPageSeachFilter';
import Map from '../Map/Map';
import axios from 'axios';
import queryString from 'query-string';
import NewListingAd from './NewListingAd/NewListingAd';
import ReactPaginate from 'react-paginate';
import NewListingFixedPriceNav from './NewListingFixedNav/NewListingFixedNavPrice/NewListingFixedNavPrice'
import SyncLoader from 'react-spinners/SyncLoader';
import NewListingFixedNavBed from './NewListingFixedNav/NewListingFixedNavBed/NewListingFixedNavBed';
import NewListingFixedNavBath from './NewListingFixedNav/NewListingFixedNavBath/NewListingFixedNavBath';
import NewListingFIxedNavPropType from './NewListingFixedNav/NewListingFixedNavPropType/NewListingFixedNavPropType';
import NewListingFixedNavMore from './NewListingFixedNav/NewListingFixedNavMore/NewListingFixedNavMore';
import NewFooter from '../NewFooter/NewFooter';

function NewListingPage(props) {

    let [posts, setPosts] = useState([]);
    let [searchResults, setSearchResults] = useState('');
    let [loading, setLoading] = useState(false);
    let [propertyCategory, setPropertyCategory] = useState('')

    //For Filter Data
    let [minPrice, setMinPrice] = useState('');
    let [maxPrice, setMaxPrice] = useState('');
    
    //For ReactPagination
    let [currentPage, setCurrentPage] = useState(0);
    const perPage = 5;
    const offset = currentPage * perPage;
    const currentPageData = posts &&  posts.slice(offset, offset + perPage);
    const pageCount = posts &&  Math.ceil(posts.length / perPage);
    const handlePageClick = ({selected: selectedPage})=> {
        setCurrentPage(selectedPage);
    }

    //For Map Latitude and Longitude
    let [mapLat, setMapLat] = useState(-37.840935);
    let [mapLng, setMapLng] = useState(144.946457);

    //FOr changing Price,Bed, PropertyType, CarSpace, landSize data
    let [minName, setMinName] = useState('');
    let [maxName, setMaxName] = useState('');
    let [bedName, setBedName] = useState('');
    let [bathName, setBathName] = useState('');
    let [propertyType, setPropertyType] = useState([]);
    let [propertyTypeLen, setPropertyTypeLen] = useState(0);
    let [carSpace, setCarSpace] = useState('');
    let [minSize, setMinSize] = useState('');
    let [maxSize, setMaxSize] = useState('');

    //For new,old,expensive,cheap,inspection,all 
    let [filterSearch, setFilterSearch] = useState('');

    useEffect(()=>{
        setLoading(true);
        let isActive = true;
        //Query String Parse
        let params = queryString.parse(props.location.search);
        let homeSearchUrl = '/api/v2/ads_list?slug=real-estate&api_from=classiEstate';

        //For Price from Home Search
        if (params.price_min && !params.price_max) homeSearchUrl += `&price=${params.price_min}__`
        if (params.price_max && !params.price_min) homeSearchUrl += `&price=__${params.price_max}`
        if (params.price_min && params.price_max) homeSearchUrl += `&price=${params.price_min}__${params.price_max}`

        //For Area from Home Search
        if (params.area_min && !params.area_max) homeSearchUrl +=  `&area=${params.area_min}__`
        if (params.area_max && !params.area_min) homeSearchUrl +=  `&area=__${params.area_max}`
        if (params.area_min && params.area_max) homeSearchUrl += `&area=${params.area_min}__${params.area_max}`

        let path = props.location.pathname;
        console.log(path.slice(9));
        console.log("Props.cat", props.cat);
        //For Property Category
        if (params.property_category){
             setPropertyCategory(params.property_category.charAt().toUpperCase() + params.property_category.slice(1));
             homeSearchUrl += `&property_category=${params.property_category}`
        }
        else if (props.cat){
            setPropertyCategory(props.cat.charAt().toUpperCase() + props.cat.slice(1));
            homeSearchUrl += `&property_category=${props.cat}`;
        }
        else if (path.slice(8).trim() === 'rent'){
            setPropertyCategory('Rent');
            homeSearchUrl += `&property_category=rent`;
        }
        else {
            setPropertyCategory('Buy');
            homeSearchUrl += '&property_category=buy'
        }
        
        // axios.get(`/api/v2/ads_list?slug=real-estate&api_from=classiEstate`);
        console.log("HomeSearchUrl on first render",homeSearchUrl);
        axios.get(homeSearchUrl)
        .then(response=>{
            if (isActive){
                console.log(response);
                console.log(response.data);
                setSearchResults(response.data.posts.length);
                setPosts(response.data.posts);
                setLoading(false);
            }

        })
        .catch(err=>{
            if (isActive){
                console.log(err);
                console.log(err.response);
                setLoading(false);
            }

        })
        let paginateEl = document.getElementsByClassName('new-listing-react-paginate')[0];
        let mapEl = document.getElementsByClassName('new-listing-map-container')[0];
        if (paginateEl && (window.pageYOffset >= (paginateEl.scrollTop() - 300))){
            mapEl.classList.toggle('new-listing-map-container-toggle');
        }
        paginateEl && console.log(paginateEl.offsetTop);

        return ()=>{
            setLoading(false);
            isActive = false;
        }
    }, []);
    const minPriceHandler = val =>{
        console.log(val);
        setMinPrice(val);
    }
    const maxPriceHandler = val =>{
        console.log(val);
        setMaxPrice(val);
    }
    const onApplyFilterHandler = (val)=>{
        setPosts([]);
        setLoading(true);
        console.log("true");
        let homeSearchUrl = '/api/v2/ads_list?slug=real-estate&api_from=classiEstate';

        //For Price from Home Search
        if (minPrice && !maxPrice) homeSearchUrl += `&price=${minPrice}__`
        if (maxPrice && !minPrice) homeSearchUrl += `&price=__${maxPrice}`
        if (minPrice && maxPrice) homeSearchUrl += `&price=${minPrice}__${maxPrice}`

        //For Bed
        if (bedName) homeSearchUrl += `&no_of_rooms=${bedName}`
        //For Bath
        if (bathName) homeSearchUrl += `&no_of_bathroom=${bathName}`
        //For PropertyType
        if (propertyType) homeSearchUrl += `&property_type=${encodeURIComponent(propertyType)}`
        //For Car Space
        if (carSpace) homeSearchUrl += `&no_of_carspace=${carSpace}`
        //For Area
        if (minSize && !maxSize) homeSearchUrl +=  `&area=${minSize}__`
        if (maxSize && !minSize) homeSearchUrl +=  `&area=__${maxSize}`
        if (minSize && maxSize) homeSearchUrl += `&area=${minSize}__${maxSize}`

        //For Lower Half Search Filter
        if (val === 'cheap') homeSearchUrl += `&sort=cheapest`;
        if (val === 'expensive') homeSearchUrl += `&sort=highest`;
        if (val === 'new') homeSearchUrl += `&sort=recent`;
        if (val === 'old') homeSearchUrl += `&sort=`;

        console.log("HOMESEARCHURL",homeSearchUrl);
        axios.get(homeSearchUrl)
        .then(response=>{
            console.log(response);
            console.log(response.data);
            setSearchResults(response.data.posts.length);
            setPosts(response.data.posts);
            setLoading(false);
        })
        .catch(err=>{
            console.log(err);
            console.log(err.response);
            setLoading(false);
        })

    }
    const toParentMinName = val =>{
        setMinName(val);
    }
    const toParentMaxName = val =>{
        setMaxName(val);
    }
    const toParentBed =val =>{
        setBedName(val);
    }
    const toParentBath = val =>{
        setBathName(val);
    }
    const toParentPropertyType = val =>{
        if (val === 'all'){
            propertyType = [];
            propertyType.push = ['all'];
            setPropertyTypeLen(propertyType.length);
        }else {
            if (propertyType.includes('all')){
                propertyType.splice(propertyType.indexOf('all'), 1);
                setPropertyTypeLen(propertyType.length);
            }
            if (propertyType.includes(val)){
                propertyType.splice(propertyType.indexOf(val), 1);
                setPropertyTypeLen(propertyType.length);
            }else {
                propertyType.push(val);
                setPropertyTypeLen(propertyType.length);
            }
        }
        console.log(propertyType);
        console.log(propertyTypeLen);
    }
    const toParentCarHandler = val =>{
        setCarSpace(val);
    }
    const toParentMinSizeHandler = val =>{
        setMinSize(val);
    }
    const toParentMaxSizeHandler = val =>{
        setMaxSize(val);
    }
    const toParentFilterHandler = val =>{
        setFilterSearch(val);
        onApplyFilterHandler(val);
    }
    const clearSearchFromListingFixedNav = val => {
        setMinName('');
        setMaxName('');
        setBedName('');
        setBathName('');
        setCarSpace('');
        setMinSize('');
        setMaxSize('');
        setPropertyTypeLen(0);
    }
    return (
        <>
        {/* <div className="absolute-line-left-home"></div>
        <div className="absolute-line-right-home"></div> */}
        <Navigation fromHome={false} line={true} />
        <NewListingFixedNav 
        minimumName={minName} 
        maximumName={maxName} 
        bedName = {bedName}
        bathName = {bathName}
        propertyType = {propertyTypeLen}
        minimumSize = {minSize}
        maximumSize = {maxSize}
        parkingSpace = {carSpace}
        clearSearchFromListingFixedNav = {clearSearchFromListingFixedNav}
        />
        <div className="new-listing-container">
            <NewListingSearchRes buyRent={['For Sale', propertyCategory]} />
            <NewListingPageTopTitle 
            topTitle=
            {propertyCategory === 'Buy'?
            "Property and real estate For Sale in Australia"
            : "Rent Property and Real Estate in Australia"
            } />
            <NewListingSearchLength searchLength={searchResults} />
            <NewListingPageSearchFilter toParentFilter={toParentFilterHandler} />
            <NewListingAd posts={currentPageData} />            
        </div>
        <div className="new-listing-map-container">
            <Map lat={mapLat} lng={mapLng} />
        </div>
        <div className="new-listing-clip-loader">
            <SyncLoader loading={loading} size={10} color={'var(--primary-color)'} />
        </div>
        {searchResults > 0 && !loading? (
        <div className="new-listing-react-paginate">
            <ReactPaginate 
            previousLabel = {'<<'}
            nextLabel={'>>'}
            pageCount={pageCount}
            onPageChange={handlePageClick}
            containerClassName={'react-pagination-container'}
            previousLinkClassName={'react-link-class'}
            nextLinkClassName={'react-link-class'}
            disabledClassName={'react-link-class--active'}
            activeClassName={'react-link-class--disabled'}
            />
        </div> 
        ): null}
        <NewListingFixedPriceNav 
        toParentMinName={toParentMinName} 
        toParentMaxName={toParentMaxName} 
        onApplyFilter={onApplyFilterHandler} 
        toParentMinPrice={minPriceHandler} 
        toParentMaxPrice={maxPriceHandler} 
        />
        <NewListingFixedNavBed 
        toParentBed={toParentBed}
        applyFilter={onApplyFilterHandler}
        />
        <NewListingFixedNavBath 
        toParentBath={toParentBath}
        applyFilter={onApplyFilterHandler}
        />
        <NewListingFIxedNavPropType 
        toParentPropertyType={toParentPropertyType}
        applyFilter={onApplyFilterHandler}
        />
        <NewListingFixedNavMore 
        toParentCar={toParentCarHandler}
        toParentMinSize={toParentMinSizeHandler}
        toParentMaxSize={toParentMaxSizeHandler}
        applyFilter={onApplyFilterHandler}
        />
        {!loading? (
        <NewFooter />
        ):null}
        </>
    )
}

export default NewListingPage
