import React, {useState} from 'react';
import './NewListingFixedNav.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

function NewListingFixedNav({minimumName, maximumName, bedName, 
    bathName, propertyType, minimumSize, maximumSize, parkingSpace, clearSearchFromListingFixedNav}) {

    let [clearSearch, setClearSearch] = useState(true);
    let item = document.getElementsByClassName('new-listing-fixed-filter-item');
    let morefilter = 0;
    if ((minimumSize || maximumSize) && !parkingSpace){
        morefilter = 1
    }else if (parkingSpace && !(minimumSize || maximumSize)){
        morefilter = 1;
    }else if ((minimumSize || maximumSize) && parkingSpace){
        morefilter = 2;
    }

    // useEffect(() => {
    //     clearSearchClickHandler();
    //     return () => {
            
    //     }
    // }, [])

    const clearSearchClickHandler = ()=> {
        clearSearchFromListingFixedNav(true)
    }

    return (
        <div className="new-listing-fixed-nav-container">
            <div className="new-listing-fixed-search">
                <FontAwesomeIcon icon="search" className="new-listing-fixed-search-icon" />
                <input className="new-listing-fixed-search-input" placeholder="Enter Location" />
                <div className="new-listing-fixed-search-search-text">
                    <FontAwesomeIcon icon="search" className="new-listing-fixed-search-search-icon" />
                    Search
                </div>
            </div>
            <div className="new-listing-fixed-filters">
                <div data-toggle="modal" data-target="#priceModal" 
                className={minimumName? "new-listing-fixed-filter-item new-listing-fixed-price-div new-listing-fixed-filter-item-active" : "new-listing-fixed-filter-item new-listing-fixed-price-div"}>
                    {minimumName ? `${minimumName} - ${maximumName}` : 'Price'}
                </div>
                <div data-toggle="modal" data-target="#bedModal" 
                className={bedName? "new-listing-fixed-filter-item new-listing-fixed-bed-div new-listing-fixed-filter-item-active": "new-listing-fixed-filter-item new-listing-fixed-bed-div"}>
                    {bedName && clearSearch ? `${bedName} Bed` : 'Bed'}
                </div>
                <div data-toggle="modal" data-target="#bathModal" 
                className={bathName ? "new-listing-fixed-filter-item new-listing-fixed-bath-div new-listing-fixed-filter-item-active" :"new-listing-fixed-filter-item new-listing-fixed-bath-div"}>
                    {bathName ? `${bathName} Bath` : 'Bath'}
                </div>
                <div data-toggle="modal" data-target="#propertyTypeModal" 
                className={propertyType?"new-listing-fixed-filter-item new-listing-fixed-property-type-div new-listing-fixed-filter-item-active": "new-listing-fixed-filter-item new-listing-fixed-property-type-div"}>
                    {propertyType?`${propertyType} Property Types` : 'Property Type'}
                </div>
                <div data-toggle="modal" data-target="#moreFilterModal" 
                className={morefilter?"new-listing-fixed-filter-item new-listing-fixed-more-filter-div new-listing-fixed-filter-item-active":"new-listing-fixed-filter-item new-listing-fixed-more-filter-div"}>
                     {morefilter?morefilter:''} More Filter
                </div>
                <div onClick={()=> clearSearchClickHandler()} className="new-listing-fixed-filter-item new-listing-fixed-clear-all-div">
                    Clear Search
                </div>
            </div>
        </div>
    )
}

export default NewListingFixedNav
