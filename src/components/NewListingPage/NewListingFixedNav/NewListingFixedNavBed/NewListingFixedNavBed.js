import React, {useState, useEffect} from 'react';
import './NewListingFixedNavBed.css';

function NewListingFixedNavBed({toParentBed, applyFilter}) {

    const onClearAllHandler = e =>{
        toParentBed('');
    }
    const applyFilterHandler = e =>{
        applyFilter(true);
    }
    let modalBedSingle = document.getElementsByClassName('modal-bed-single');
    for(let i =0; i<modalBedSingle.length; i++){
        modalBedSingle[i].addEventListener('click', (e)=>{
            let current = document.getElementsByClassName('modal-bed-active')[0];
            current.className = current.className.replace('modal-bed-active', '');
            modalBedSingle[i].className += ' modal-bed-active';
            toParentBed(modalBedSingle[i].getAttribute('value'));
        })
    }
    return (
        <>
        <div className="modal fade" id="bedModal" tabIndex="-1" role="dialog" aria-labelledby="bedFilterModal" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content col-12">
                    <div className="modal-header">
                        <h5 className="modal-title">Bedrooms</h5> 
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> 
                            <span aria-hidden="true">&times;</span> 
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="modal-bed-all">
                            <div value='' className="modal-bed-single modal-bed-any modal-bed-active">Any</div>
                            <div value='Studio' className="modal-bed-single modal-bed-studio">Studio</div>
                            <div value='1' className="modal-bed-single modal-bed-one">1</div>
                            <div value='2' className="modal-bed-single modal-bed-two">2</div>
                            <div value='3' className="modal-bed-single modal-bed-three">3</div>
                            <div value='4' className="modal-bed-single modal-bed-four">4</div>
                            <div value='5+' className="modal-bed-single modal-bed-five">5+</div>
                        </div>
                    </div>
                    <div className="modal-footer modal-price-footer">
                        <div onClick={(e)=> onClearAllHandler(e)} className="modal-clear-filter" >
                            Clear All
                        </div>
                        <div onClick={(e)=> applyFilterHandler(e)} className="modal-apply-filter" data-dismiss="modal" aria-label="Close">
                            Apply Filter
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default NewListingFixedNavBed
