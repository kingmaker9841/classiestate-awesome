import React, {useState, useEffect} from 'react';
import './NewListingFixedNavBath.css';

function NewListingFixedNavBath({toParentBath, applyFilter}) {

    const onClearAllHandler = e =>{
        toParentBath('');
    }
    const applyFilterHandler = e =>{
        applyFilter(true);
    }
    let modalBathSingle = document.getElementsByClassName('modal-bath-single');
    for(let i =0; i<modalBathSingle.length; i++){
        modalBathSingle[i].addEventListener('click', (e)=>{
            let current = document.getElementsByClassName('modal-bath-active')[0];
            current.className = current.className.replace('modal-bath-active', '');
            modalBathSingle[i].className += ' modal-bath-active';
            toParentBath(modalBathSingle[i].getAttribute('value'));
        })
    }
    return (
        <>
        <div className="modal fade" id="bathModal" tabIndex="-1" role="dialog" aria-labelledby="bathFilterModal" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content col-12">
                    <div className="modal-header">
                        <h5 className="modal-title">Bathroom</h5> 
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> 
                            <span aria-hidden="true">&times;</span> 
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="modal-bath-all">
                            <div value='' className="modal-bath-single modal-bath-any modal-bath-active">Any</div>
                            <div value='1' className="modal-bath-single modal-bath-one">1</div>
                            <div value='2' className="modal-bath-single modal-bath-two">2</div>
                            <div value='3' className="modal-bath-single modal-bath-three">3</div>
                            <div value='4' className="modal-bath-single modal-bath-four">4</div>
                            <div value='5+' className="modal-bath-single modal-bath-five">5+</div>
                        </div>
                    </div>
                    <div className="modal-footer modal-price-footer">
                        <div onClick={(e)=> onClearAllHandler(e)} className="modal-clear-filter" >
                            Clear All
                        </div>
                        <div onClick={(e)=> applyFilterHandler(e)} className="modal-apply-filter" data-dismiss="modal" aria-label="Close">
                            Apply Filter
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default NewListingFixedNavBath
