import React, {useRef, useState} from 'react';
import './NewListingFixedNavMore.css';

function NewListingFixedNavMore({toParentCar, applyFilter, toParentMinSize, toParentMaxSize}) {
    const inputElOne = useRef(null);
    const inputElTwo = useRef(null);
    const onClearAllHandler = (e)=>{
        toParentCar('');
        toParentMinSize('');
        toParentMaxSize('');
        let modalCar = document.getElementsByClassName('modal-filter-car-single');
        for (let i =0; i<modalCar.length; i++){
            modalCar[i].className = modalCar[i].className.replace(' modal-filter-car-active', '');
        }
        modalCar[0].className += ' modal-filter-car-active';
        // minChangeHandler('');
        // maxChangeHandler('');
        inputElOne.current.value = '';
        inputElTwo.current.value = '';
    }
    const applyFilterHandler = e =>{
        applyFilter(true);
    }
    let modalCar = document.getElementsByClassName('modal-filter-car-single');
    for (let i =0; i<modalCar.length; i++){
        modalCar[i].addEventListener('click', (e)=>{
            let current = document.getElementsByClassName('modal-filter-car-active')[0];
            current.className = current.className.replace(' modal-filter-car-active', '');
            modalCar[i].className += ' modal-filter-car-active';
            toParentCar(modalCar[i].getAttribute('value'));
        })
    }
    const minChangeHandler = val =>{
        toParentMinSize(val);
    }
    const maxChangeHandler = val =>{
        toParentMaxSize(val);
    }
    return (
        <>
        <div className="modal fade" id="moreFilterModal" tabIndex="-1" role="dialog" aria-labelledby="moreFilterModal" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content col-12">
                    <div className="modal-header">
                        <h5 className="modal-title">More Filters</h5> 
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> 
                            <span aria-hidden="true">&times;</span> 
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="modal-more-filter-car-space-text">Car Space</div>
                        <div className="modal-more-filter-car-space">
                            <div value='' className="modal-filter-car-single modal-filter-car-any modal-filter-car-active">Any</div>
                            <div value='1' className="modal-filter-car-single modal-filter-car-one">1</div>
                            <div value='2' className="modal-filter-car-single modal-filter-car-two">2</div>
                            <div value='3' className="modal-filter-car-single modal-filter-car-three">3</div>
                            <div value='4' className="modal-filter-car-single modal-filter-car-four">4</div>
                            <div value='5+' className="modal-filter-car-single modal-filter-car-five">5+</div>
                        </div>
                        <div className="modal-more-filter-land-text">Land Size</div>
                        <div className="modal-more-filter-land">
                            <div className="modal-more-filter-land-min-div">
                                <div className="modal-more-filter-land-min-area-text">
                                    Minimum Area (m<sup>2</sup>)
                                </div>
                                <input ref={inputElOne} onChange={e=> minChangeHandler(e.target.value)} type="text" className="modal-more-filter-land-input modal-more-filter-land-min-input" />
                                <div className="modal-more-filter-land-meter-abs-min">
                                    m<sup>2</sup>
                                </div>
                            </div>
                            <div className="modal-more-filter-land-max-div">
                                <div className="modal-more-filter-land-max-area-text">
                                    Maximum Area (m<sup>2</sup>)
                                </div>
                                <input ref={inputElTwo} onChange={e=> maxChangeHandler(e.target.value)} type="text" className="modal-more-filter-land-input modal-more-filter-land-max-input" />
                                <div className="modal-more-filter-land-meter-abs-max">
                                    m<sup>2</sup>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="modal-footer modal-price-footer">
                        <div onClick={(e)=> onClearAllHandler(e)} className="modal-clear-filter" >
                            Clear All
                        </div>
                        <div onClick={(e)=> applyFilterHandler(e)} className="modal-apply-filter" data-dismiss="modal" aria-label="Close">
                            Apply Filter
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default NewListingFixedNavMore
