import React, {useState, useRef} from 'react';
import './NewListingFixedNavPrice.css';
import {Multiselect} from 'multiselect-react-dropdown';

function NewListingFixedNavPrice({toParentMinPrice, toParentMaxPrice, onApplyFilter, toParentMinName, toParentMaxName}) {
    let [minPrice, setMinPrice] = useState('');
    let [maxPrice, setMaxPrice] = useState('');
    const multiselectMinRef = useRef(null);
    const multiselectMaxRef = useRef(null);

    const onMinSelectHandler = (list, item) =>{
        setMinPrice(item.value);
        toParentMinPrice(item.value);
        toParentMinName(item.name);
    }
    const onMaxSelectHandler = (list, item) =>{
        setMaxPrice(item.value);
        toParentMaxPrice(item.value);
        toParentMaxName(item.name);
    }
    const onClearAllHandler = (e)=>{
        setMinPrice('');
        setMaxPrice('');
        multiselectMinRef.current.resetSelectedValues();
        multiselectMaxRef.current.resetSelectedValues();
        toParentMinPrice('');
        toParentMaxPrice('');
        toParentMinName('');
        toParentMaxName('');
    }
    const applyFilterHandler = e =>{
        onApplyFilter(true);
    }
    const style = {
        searchBox : {
            border : '1px solid rgba(0,0,0,.2)',
            'border-radius': '6px',
            'font-size' : '0.8rem'
        },
        chips: {
            background: 'var(--primary-color)',
            color: '#fff'
        },
        option: {
            'font-size' : '0.8rem'
        }
    }
    const optionPrice = [
        {name: '$10k', value: '10000'},
        {name: '$50k', value: '50000'},
        {name: '$100k', value: '100000'},
        {name: '$150k', value: '150000'},
        {name: '$200k', value: '200000'},
        {name: '$250k', value: '250000'},
        {name: '$300k', value: '300000'},
        {name: '$500k', value: '500000'},
        {name: '$1M', value: '1000000'},
        {name: '$1.5M', value: '1500000'},
        {name: '$3M', value: '3000000'},
        {name: '$5M', value: '5000000'},
        {name: '$10M', value: '10000000'},
        {name: '$10M+', value: '11000000'},
    ]
    return (
        <>
        <div className="modal fade" id="priceModal" tabIndex="-1" role="dialog" aria-labelledby="priceFilterModal" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content col-12">
                    <div className="modal-header">
                        <h5 className="modal-title">Price</h5> 
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> 
                            <span aria-hidden="true">&times;</span> 
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="modal-price-min">
                            Minimum $Price
                        </div>
                        <Multiselect 
                        options={optionPrice}
                        displayValue = 'name'
                        isObject={true}
                        style = {style}
                        singleSelect = {true}
                        disablePreSelectedValues = {true}
                        avoidHighlightFirstOption = {true}
                        placeholder = "Select Minimum Price"
                        onSelect = {onMinSelectHandler}
                        ref={multiselectMinRef}
                        />
                        <div className="modal-price-max">
                            Maximum $Price
                        </div>
                        <Multiselect 
                        options={optionPrice}
                        displayValue = 'name'
                        isObject={true}
                        style = {style}
                        singleSelect = {true}
                        disablePreSelectedValues = {true}
                        avoidHighlightFirstOption = {true}
                        placeholder = "Select Minimum Price"
                        onSelect = {onMaxSelectHandler}
                        ref={multiselectMaxRef}
                        />
                    </div>
                    <div className="modal-footer modal-price-footer">
                        <div onClick={(e)=> onClearAllHandler(e)} className="modal-clear-filter" >
                            Clear All
                        </div>
                        <div onClick={(e)=> applyFilterHandler(e)} className="modal-apply-filter" data-dismiss="modal" aria-label="Close">
                            Apply Filter
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default NewListingFixedNavPrice
