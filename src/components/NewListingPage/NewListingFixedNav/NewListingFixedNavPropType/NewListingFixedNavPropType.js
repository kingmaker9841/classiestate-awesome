import React from 'react';
import './NewListingFixedNavPropType.css';

function NewListingFixedNavPropType({toParentPropertyType, applyFilter}) {
    const onClearAllHandler = (e)=>{
        let exceptAll = document.getElementsByClassName('modal-property-type-common');
        for (let i =0; i<exceptAll.length; i++){
            if (exceptAll[i].classList.contains('modal-property-type-active')){
                exceptAll[i].className = exceptAll[i].className.replace(' modal-property-type-active', ''); 
            }
        }
        let any = document.getElementsByClassName('modal-property-type-any')[0];
        any.classList.toggle('modal-property-type-active');
        toParentPropertyType('all');
    }
    const applyFilterHandler = (e)=>{
        applyFilter(true);
        
    }
    const proptypesClickHandler = e =>{
        let exceptAll = document.getElementsByClassName('modal-property-type-common');
        for (let i =0; i<exceptAll.length; i++){
            if (exceptAll[i].classList.contains('modal-property-type-active')){
                exceptAll[i].className = exceptAll[i].className.replace(' modal-property-type-active', ''); 
            }
        }
        e.target.className += ' modal-property-type-active';
        toParentPropertyType('all');
    }
    const otherClickHandler = e=>{
        let all = document.getElementsByClassName('modal-property-type-any')[0];
        if (all.classList.contains('modal-property-type-active')){
            all.className = all.className.replace(' modal-property-type-active', '');
        }
        e.target.classList.toggle('modal-property-type-active');   
        toParentPropertyType(e.target.innerText);
    }
    return (
        <>
        <div className="modal fade" id="propertyTypeModal" tabIndex="-1" role="dialog" aria-labelledby="propertyTypeFilterModal" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content col-12">
                    <div className="modal-header">
                        <h5 className="modal-title">Property Type</h5> 
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"> 
                            <span aria-hidden="true">&times;</span> 
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="modal-property-type-all">
                            <div onClick={e=> proptypesClickHandler(e)} className="modal-property-type-single modal-property-type-any modal-property-type-active">Any</div>
                            <div onClick={e=> otherClickHandler(e)} className="modal-property-type-single modal-property-type-common modal-property-type-house">House</div>
                            <div onClick={e=> otherClickHandler(e)} className="modal-property-type-single modal-property-type-common modal-property-type-apartment">Apartment</div>
                            <div onClick={e=> otherClickHandler(e)} className="modal-property-type-single modal-property-type-common modal-property-type-townhouse">Townhouse</div>
                            <div onClick={e=> otherClickHandler(e)} className="modal-property-type-single modal-property-type-common modal-property-type-land">Land</div>
                            <div onClick={e=> otherClickHandler(e)} className="modal-property-type-single modal-property-type-common modal-property-type-rural">Rural</div>
                            <div onClick={e=> otherClickHandler(e)} className="modal-property-type-single modal-property-type-common modal-property-type-new-apartment">New Apartment</div>
                            <div onClick={e=> otherClickHandler(e)} className="modal-property-type-single modal-property-type-common modal-property-type-new-house-and-land">New House and Land</div>
                            <div onClick={e=> otherClickHandler(e)} className="modal-property-type-single modal-property-type-common modal-property-type-commerical">Commerical</div>
                            <div onClick={e=> otherClickHandler(e)} className="modal-property-type-single modal-property-type-common modal-property-type-garage-and-storage">Garage and Storage</div>
                            <div onClick={e=> otherClickHandler(e)} className="modal-property-type-single modal-property-type-common modal-property-type-other">Other</div>
                        </div>
                    </div>
                    <div className="modal-footer modal-price-footer">
                        <div onClick={(e)=> onClearAllHandler(e)} className="modal-clear-filter" >
                            Clear All
                        </div>
                        <div onClick={(e)=> applyFilterHandler(e)} className="modal-apply-filter" data-dismiss="modal" aria-label="Close">
                            Apply Filter
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default NewListingFixedNavPropType
