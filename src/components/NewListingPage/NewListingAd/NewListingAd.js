import React,{useState, useEffect} from 'react';
import './NewListingAd.css';
import ClassiBazaar from '../../../assets/images/classibazaar-icon.png';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {changePrice} from '../../../library/CustomPrice/CustomPrice';

export function padZero(str, len) {
    len = len || 2;
    var zeros = new Array(len).join('0');
    return (zeros + str).slice(-len);
}
export function invertColor(hex, bw) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if (hex.length !== 6) {
        throw new Error('Invalid HEX color.');
    }
    var r = parseInt(hex.slice(0, 2), 16),
        g = parseInt(hex.slice(2, 4), 16),
        b = parseInt(hex.slice(4, 6), 16);
    if (bw) {
        return (r * 0.299 + g * 0.587 + b * 0.114) > 186
            ? '#000000'
            : '#FFFFFF';
    }
    // invert color components
    r = (255 - r).toString(16);
    g = (255 - g).toString(16);
    b = (255 - b).toString(16);
    // pad each with zeros and return
    return "#" + padZero(r) + padZero(g) + padZero(b);
}

function NewListingAd({posts}) {
    let [isLoggedIn, setIsLoggedIn] = useState(false);
    let [savedFav, setSavedFav] = useState('')
    useEffect(()=>{
        if (localStorage.getItem('token')){
            setIsLoggedIn(true);
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            setSavedFav(cF);
        }
        return ()=>{

        }
    }, []);
    const favoriteClickHandler = (e, id)=>{
        e.stopPropagation();
        if (isLoggedIn){
            if (savedFav){
                if (savedFav.includes(id)){
                    e.target.classList.toggle('new-listing-ad-fav-icon');
                    //Now Update savedFav
                    setSavedFav(prev=> prev.replace(id, ''));
                }else {
                    e.target.classList.toggle('new-listing-ad-fav-icon-active');
                }
            }else {
                e.target.classList.toggle('new-listing-ad-fav-icon-active');
            }
            
            let cF = JSON.parse(localStorage.getItem('classi_favorite'));
            if (cF){
                if (String(cF).includes(id)){
                    cF = String(cF).replace(id, '')
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }else {
                    cF = String(cF) + ',' + id;
                    localStorage.setItem('classi_favorite', JSON.stringify(cF));
                }  
            }else {
                localStorage.setItem('classi_favorite', JSON.stringify(id));
            }
            
        }
    }
    return (
        <div className="new-listing-ad-container">
            {posts && posts.map((item,index)=>(
            <div key={index} className="new-listing-ad-single">
                <div className="new-listing-ad-image-div">
                    <img src={item.default_image} alt={index} className="new-listing-ad-image-single" />
                    <div style={{backgroundColor: `${item.company?(item.company.company_color): 'var(--primary-color)'}`}} className="new-listing-ad-image-abs-div">
                        <div className="new-listing-ad-image-agent-div">
                            <img 
                            src={item.agents?(item.agents[0]?(item.agents[0].image_url?item.agents[0].image_url : ClassiBazaar): ClassiBazaar):ClassiBazaar}
                             alt={index} className="new-listing-ad-image-agent-image" />
                        </div>
                        <div className="new-listing-ad-image-agent-name-div">
                            <div style={{color: `${invertColor(item.company.company_color, 1)}`}} className="new-listing-ad-image-agent-name-text">
                                {item.agents?(item.agents[0]?(item.agents[0].first_name + " " + item.agents[0].last_name) : 'ClassiEstate'): 'ClassiEstate'}
                            </div>
                            <div style={{color: `${invertColor(item.company.company_color, 1)}`}} className="new-listing-ad-image-agent-company-name-div">
                                {item.company?(item.company.first_name + " " + item.company.last_name): 'ClassiEstate Property'}
                            </div>
                        </div>
                        <div className="new-listing-ad-image-company-logo-div">
                            <img src={item.company.company_logo?item.company.company_logo: ClassiBazaar} alt="company.jpg" className="new-listing-ad-image-company-logo" />
                        </div>
                    </div>
                    <div className="new-listing-ad-fav-icon-div">
                        <FontAwesomeIcon onClick={e=> favoriteClickHandler(e,item.post_id)} icon="star" className={
                                                    savedFav 
                                                    ? (savedFav.includes(item.post_id) 
                                                    ? "new-listing-ad-fav-icon-active "
                                                    : "new-listing-ad-fav-icon") 
                                                    : "new-listing-ad-fav-icon"
                        } />
                    </div>
                </div>
                <div className="new-listing-ad-other-div">
                    <div className="new-listing-ad-price-div">
                        ${changePrice(item.price)}
                    </div>
                    <div className="new-listing-ad-address-div">
                        {item.ad_address?item.ad_address + ",": (item.address ? (item.address) + "," : 'Australian Property,')}
                    </div>
                    <div className="new-listing-ad-second-address-div">
                        {item.state_name?item.state_name:(item.addres2?item.address2:'Australia')}
                    </div>
                    <div className="new-listing-ad-misc">
                        <div className="new-listing-ad-misc-icons">
                            <FontAwesomeIcon icon="bed" className="new-listing-ad-misc-icon" />
                            <span> {item.no_room}&nbsp;&nbsp;&nbsp;&nbsp; </span>
                        </div>
                        <div className="new-listing-ad-misc-icons">
                            <FontAwesomeIcon icon="bath" className="new-listing-ad-misc-icon" />
                            <span> {item.no_of_bathroom}&nbsp;&nbsp;&nbsp;&nbsp;  </span>
                        </div>
                        <div className="new-listing-ad-misc-icons">
                            <FontAwesomeIcon icon="parking" className="new-listing-ad-misc-icon" />
                            <span> {item.no_of_car_space}&nbsp;&nbsp;&nbsp;&nbsp; </span>
                        </div>
                        <div className="new-listing-ad-misc-icons">
                            <FontAwesomeIcon icon="vector-square" className="new-listing-ad-misc-icon" />
                            <span> {item.area + " " + 'm'}<sup>2</sup> </span>
                        </div>
                    </div>
                </div>
            </div>
            ))}
        </div>
    )
}

export default NewListingAd;
